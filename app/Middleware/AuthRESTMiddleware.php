<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 22/01/2019
 * Time: 14:04
 */

namespace App\Middleware;


class AuthRESTMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {

        //revisar si el usuario tiene sesión
        /*if(!$this->container->auth->check()){
            return $response->withJson([
                "success" => false,
                "data" => [],
                "message" => 'Sin sesión'
            ], 200,
                JSON_PRETTY_PRINT);
        }*/
        $response = $next($request, $response);

        return $response;
    }

}