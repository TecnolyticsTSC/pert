<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 22/01/2019
 * Time: 14:04
 */

namespace App\Middleware;


class AuthAdminMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {

        //revisar si el usuario tiene sesión
        if(!$this->container->auth->check()){
            $this->container->flash->addMessage('info','Primero debe iniciar sesión');
            return $response->withRedirect($this->container->router->pathFor('login'));
        }else{
            if (!(int)$this->container->auth->user()->admin == 1){
                $this->container->flash->addMessage('info','No tiene permisos para esta acción');
                return $response->withRedirect($this->container->router->pathFor('home'));
            }
            if (!(int)$this->container->auth->user()->activo == 1){
                $this->container->auth->logout();
                $this->container->flash->addMessage('info','Cuenta inactiva');
                return $response->withRedirect($this->container->router->pathFor('landing'));
            }
        }
        $response = $next($request, $response);

        return $response;
    }

}