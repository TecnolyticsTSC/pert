<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 22/01/2019
 * Time: 14:14
 */

namespace App\Middleware;


class GuestMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {

        //revisar si el usuario es un invitado
        if($this->container->auth->check()){
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $response = $next($request, $response);
        return $response;
    }

}