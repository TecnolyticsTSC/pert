<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 18/01/2019
 * Time: 17:09
 */

namespace App\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CsrfViewMiddleware extends Middleware
{

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response,callable $next)
    {

        $tokenNameKey = $this->container->csrf->getTokenNameKey();
        $tokenName = $this->container->csrf->getTokenName();

        $tokenValeKey = $this->container->csrf->getTokenValueKey();
        $tokenValeName = $this->container->csrf->getTokenValue();

        $this->container->view->getEnvironment()->addGlobal('csrf',[
            'field'=> '
            <input type="hidden" name="' . $tokenNameKey . '" value="' . $tokenName . '">
            <input type="hidden" name="' . $tokenValeKey . '" value="' . $tokenValeName . '">
            '
        ]);
        $this->container->view->getEnvironment()->addGlobal('ajax_request',[
            'field'=>'
            <meta name="' . $tokenNameKey . '" content="' . $tokenName . '">
            <meta name="' . $tokenValeKey . '" content="' . $tokenValeName . '">
            '
        ]);
        /*$request = $request->withAttribute($tokenNameKey, $tokenName);
        $request = $request->withAttribute($tokenValeKey, $tokenValeName);*/
       /* $requestbody = $request->getBody();
        $requestobject = json_decode($requestbody);
        $requestobject[$tokenNameKey]=$tokenName;
        $requestobject[$tokenValeKey]=$tokenValeName;
        echo json_encode($requestobject); die();*/

       /* $request =$request->withParsedBody($requestobject);*/
       // var_dump($request->getBody()->__toString());exit();
        if (false === $request->getAttribute('csrf_status')) {
            // display suitable error here
           /*$dats = array("CODE"=>403,"MESSAGE"=>"Acceso denegado");
            return $response->withJson($dats,403);*/
            return $response->write("
                <!DOCTYPE html>
                <html>
                <head><title>CSRF test</title></head>
                <body>
                    <h1>Error</h1>
                    <p>An error occurred with your form submission.
                       Please start again.</p>
                </body>
                </html>
                ");
        }
        $response = $next($request, $response);
        return $response;
    }

}