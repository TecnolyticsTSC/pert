<?php

$app->get('/tb', App\Controllers\HomeController::class . ':tables');
$app->get('/zonasmp', App\Controllers\HomeController::class . ':zonasmp');
//$this->get('/', App\Controllers\HomeController::class . ':index')->setName('home');
$app->get('/procesos', App\Controllers\ProcesosController::class . ':procesos')->setName('procesos');
$app->get('/salidas', App\Controllers\SalidasController::class . ':salidas')->setName('salidas');
$app->get('/entradas', App\Controllers\EntradasController::class . ':entradas')->setName('entradas');
$app->get('/nolocalizados', App\Controllers\NoLocalizadosController::class . ':noLocalizados')->setName('noLocalizados');
$app->get('/cargadescarga', App\Controllers\EntradasController::class . ':cargadescarga')->setName('cargadescarga');
$app->get('/cargaprocesada', App\Controllers\CargaProcesadaController::class . ':cargaprocesada')->setName('cargaprocesada');
$app->get('/resumenrutas', App\Controllers\ResumenRutasController::class . ':resumenrutas')->setName('resumenrutas');
$app->get('/analisisrutas', App\Controllers\AnalisisRutasController::class . ':analisisrutas')->setName('analisisrutas');
$app->get('/entregasrecoleccion', App\Controllers\EntregaRecoleccionController::class . ':entregasrecoleccion')->setName('entregasrecoleccion');
$app->get('/destinosrecolecciones', App\Controllers\DestinosRecoleccionesController::class . ':destinosRecolecciones')->setName('destinosRecolecciones');
$app->get('/cuadraturaservicio', App\Controllers\CuadraturaServicioController::class . ':cuadraturaservicio')->setName('cuadraturaservicio');
$app->get('/llegadas', App\Controllers\LlegadasTr1Controller::class . ':llegadas')->setName('llegadas');

$app->get('/proyeccionentregas', App\Controllers\HomeController::class . ':proyeccion_entregas')->setName('proyeccionentregas');

$app->get('/chart', App\Controllers\ProcesosController::class . ':chart')->setName('chart');

$app->get('/dummie', App\Controllers\DummieController::class . ':dummie');

$app->get('/', App\Controllers\HomeController::class . ':landing')->setName('landing');
$app->get('/js', App\Controllers\HomeController::class . ':js');


$app->group('', function () {

    $this->get('/signup', App\Controllers\Auth\AuthController::class . ':getSignUp')->setName('signup');
    $this->post('/signup', App\Controllers\Auth\AuthController::class . ':postSignUp');

    $this->get('/login', App\Controllers\Auth\AuthController::class . ':getLogin')->setName('login');
    $this->post('/login', App\Controllers\Auth\AuthController::class . ':postLogin');
})->add(new \App\Middleware\GuestMiddleware($container));


//Con sesión
$app->group('', function () {
    
    $this->get('/proyeccion/entregas/d5', App\Controllers\ProyeccionEntregasD5Controller::class . ':index')->setName('proyeccion_entregas_d5');

		
    //Administrar umbrales
    //Listar routes with umbrales
    $this->get('/routes_umbrales', App\Controllers\UmbralesTR1Controller::class . ':routes')->setName('routes_umbrales');

    $this->get('/TR1Cumplimiento', App\Controllers\TR1CumplimientoController::class . ':TR1Cumplimiento')->setName('TR1Cumplimiento');
    $this->get('/CumplimientoGarantias', App\Controllers\CumplimientoGarantiasController::class . ':CumplimientoGarantias')->setName('CumplimientoGarantias');
    $this->get('/CuadraturaServiciosTR1', App\Controllers\CuadraturaServiciosTR1Controller::class . ':CuadraturaServiciosTR1')->setName('CuadraturaServiciosTR1');
	 //Listar Rutas Geocercas
    $this->get('/Geocercas', \App\Controllers\DimGeocercasController::class.':index')->setName('Geocercas');
	$this->get('/RutasTransito', App\Controllers\RutasTransitoController::class . ':index')->setName('RutasTransito');
    //$this->get('/RutasTransito', App\Controllers\RutasTransitoController::class . ':indexTabPTAvsRTA')->setName('RutasTransito');
    
    $this->get('/TendenciaCarga', App\Controllers\TendenciaCargaController::class . ':TendenciaCarga')->setName('TendenciaCarga'); 
    $this->get('/TendenciaCargaG2', App\Controllers\TendenciaCargaG2Controller::class . ':TendenciaCargaG2')->setName('TendenciaCargaG2');



    $this->get('/home', App\Controllers\HomeController::class . ':index')->setName('home');

    $this->get('/logout', App\Controllers\Auth\AuthController::class . ':getLogout')->setName('logout');

    $this->get('/pwchange', App\Controllers\Auth\PasswordController::class . ':getChangePassword')->setName('pwchange');
    $this->post('/pwchange', App\Controllers\Auth\PasswordController::class . ':postChangePassword');
})->add(new \App\Middleware\AuthMiddleware($container));

//Con sesión de administrador
$app->group('', function () {
    //Listar
    $this->get('/usuarios', \App\Controllers\DimUsuariosController::class . ':index')->setName('usuarios');
//administrar
$this->get('/administrar', \App\Controllers\DimUsuariosController::class . ':admin')->setName('administrar');
    //Editar
    $this->get('/usuario/{id:[0-9]+}', \App\Controllers\DimUsuariosController::class . ':getEdit')->setName('usuario');
    $this->post('/usuario/{id:[0-9]+}', \App\Controllers\DimUsuariosController::class . ':postEdit');

    //Eliminar
    $this->post('/usuario/delete', \App\Controllers\DimUsuariosController::class . ':postDelete')->setName('usuario.delete');

    //crear
    $this->get('/usuario/create', \App\Controllers\DimUsuariosController::class . ':getCreate')->setName('usuario.create');
    $this->post('/usuario/create', \App\Controllers\DimUsuariosController::class . ':postCreate');


    //Administrar plazas
    $this->get('/usuario/plazas/{id:[0-9]+}', \App\Controllers\DimUsuariosController::class . ':getAddPlazas')->setName('usuario.plazas');

    //Administrar pantallas
    $this->get('/usuario/pantallas/{id:[0-9]+}', \App\Controllers\DimUsuariosController::class . ':getAddPantallas')->setName('usuario.pantallas');
    //Listar
    $this->get('/perdidos', \App\Controllers\ItemsPerdidosController::class . ':inicio')->setName('items_perdidos');
    //crear
    $this->get('/dist/create', \App\Controllers\ItemsPerdidosController::class . ':getCreate')->setName('dist.create');
    $this->post('/dist/create', \App\Controllers\ItemsPerdidosController::class . ':postCreate');
})->add(new \App\Middleware\AuthAdminMiddleware($container));

$app->get('/asignarcop', \App\Controllers\DimLocationController::class . ':asignarPlazasUsuarios');
//API REST
$app->group('/API', function () {
    //Plazas
    //Listar



    $this->get('/usuario/plazas/{id:[0-9]+}', \App\Controllers\DimUsuariosController::class . ':plazas')->setName('api.usuario.plazas');
    $this->get('/plazas/filtrar/{filterid:[0-9]+}[/{code:.*}]', \App\Controllers\DimLocationController::class . ':filtrarPlazas')->setName('api.plazas');
    $this->post('/plazas/asignar', \App\Controllers\DimLocationController::class . ':asignarPlazas')->setName('api.plazas.asignar');
    $this->get('/plaza/remover[/{id:[0-9]+}]', \App\Controllers\DimLocationController::class . ':removerPlaza')->setName('api.plaza.remover');

    $this->post('/tr1/filtrar', \App\Controllers\ProyeccionEntregasD5Controller::class . ':dias')->setName('api.tr1.filtrar');
    $this->post('/origenant/filtrar', \App\Controllers\ProyeccionEntregasD5Controller::class . ':origenAnteriorDestinoSigiente')->setName('api.origen.ant.filtrar');
    $this->post('/origenant/filtrar/detalle', \App\Controllers\ProyeccionEntregasD5Controller::class . ':detalleGuiasTr1')->setName('api.data.det.guias.tr1');


    //codigo postal
    //$this->get('/cp',  \App\Controllers\ProyeccionEntregasD5Controller::class.':codigoPostal')->setName('api.data.codigopos.g');
    //$this->post('/cp',  \App\Controllers\ProyeccionEntregasD5Controller::class.':codigoPostal')->setName('api.data.codigopos');
//codigo postal
    $this->get('/plazas', \App\Controllers\ProyeccionEntregasD5Controller::class . ':listarFiltrosPlazas');
    $this->get('/cp', \App\Controllers\ProyeccionEntregasD5Controller::class . ':codigoPostal')->setName('api.data.codigopos.g');
    $this->post('/cp', \App\Controllers\ProyeccionEntregasD5Controller::class . ':codigoPostal')->setName('api.data.codigopos');
//tr2
    $this->get('/tr2', \App\Controllers\ProyeccionEntregasD5Controller::class . ':rutaTr2')->setName('api.data.tr2.g');
    $this->post('/tr2', \App\Controllers\ProyeccionEntregasD5Controller::class . ':rutaTr2')->setName('api.data.tr2');
    $this->post('/detalle/guias', \App\Controllers\ProyeccionEntregasD5Controller::class . ':detalleGuias')->setName('api.data.det.guias');
    $this->post('/detalle/guias/tr2', \App\Controllers\ProyeccionEntregasD5Controller::class . ':detalleGuiasTr2')->setName('api.data.det.guias.tr2');
//Entradas TR1 -> Avance de Llegadas
    $this->post('/entradas/filtrar', App\Controllers\EntradasController::class . ':entradasFiltro')->setName('api.ent.filtrar');
//procesos
    $this->post('/procesos/filtrar', App\Controllers\ProcesosController::class . ':procesosFiltro')->setName('api.ent.filtrarProcesos');
   //Get route selected with umbrales
    $this->post('/route/umbrales/',  App\Controllers\UmbralesTR1Controller::class.':route')->setName('api.route.umbrales');
    //Save umbrales of route
    $this->post('/save/umbrales/',  App\Controllers\UmbralesTR1Controller::class.':saveUmbrales')->setName('api.save.umbrales');

    //Get data Cumplimiento tr1
    $this->post('/data/cumplimiento',  \App\Controllers\TR1CumplimientoController::class.':getData')->setName('api.tr1.cumplimiento');

    //Get data Cumplimiento de garantias
    $this->post('/data/cumplimiento/garantias',  \App\Controllers\CumplimientoGarantiasController::class.':getData')->setName('api.tr1.cumplimiento.garantias');
	$this->get('/RutasTransitoG1',  \App\Controllers\RutasTransitoController::class.':detalleRutasTransitoG1')->setName('api.data.RutasTransito1.g');
    $this->post('/RutasTransitoG1',  \App\Controllers\RutasTransitoController::class.':detalleRutasTransitoG1')->setName('api.data.RutasTransito1');
    $this->get('/RutasTransitoG2',  \App\Controllers\RutasTransitoController::class.':detalleRutasTransitoG2')->setName('api.data.RutasTransito2.g');
    $this->post('/RutasTransitoG2',  \App\Controllers\RutasTransitoController::class.':detalleRutasTransitoG2')->setName('api.data.RutasTransito2');

})->add(new \App\Middleware\AuthRESTMiddleware($container));
