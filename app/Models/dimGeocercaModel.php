<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class dimGeocercaModel extends Model
{
    public static function selectGeocerca() {
        $query = "
            SELECT id,locationId,poligonoId,verticeId,longitud,latitud,fechaAlta,fechaCambio,status
            FROM vwGeocerca WHERE locationId=61
            ";
        $query = "

 SELECT l.id,l.code,l.typecode,l.city,l.state,l.longitude,l.latitude,
        g.locationId,g.id,g.poligonoId,g.verticeId,g.longitud,g.latitud,g.fechaAlta,g.fechaCambio,g.status
 FROM tr1.vwGeocerca g
 JOIN tr1.dimlocations l
 ON g.locationId=l.id
 
                ---WHERE g.locationId=61
            ";       
        
        return DB::select(
            $query
        );
    }
    public static function insertGeocerca() {
        $query = "
                INSERT INTO vwGeocerca(locationId,poligonoId,verticeId,longitud,latitud,fechaAlta,fechaCambio,status) 
                VALUES(61,1,1,-99.171738,18.896809,current_timestamp,current_timestamp,true);
            ";
        return DB::select(
            $query
        );
    }

}
