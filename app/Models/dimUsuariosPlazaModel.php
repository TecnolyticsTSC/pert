<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class dimUsuariosPlazaModel extends Model
{
    public $timestamps = false;
    protected $table = "admin.dimusuariosplaza";
    //protected $table = "admin-dimusuariosplaza";
    protected $fillable = ['id', 'usuarioid','locationid','locationcode','fecha_alta'];

    public static function buscar($id, $columns = ['*'])
    {
        return parent::find($id, $columns);
    }
}
