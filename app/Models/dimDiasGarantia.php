<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class dimDiasGarantia extends Model {

    public $timestamps = false;
    protected $table = "guias.dimDiasGarantia";

    //protected $table = "guiasdimGarantias";

    public static function obtenerGarantias() {
        //"select  distinct  substr(tiposervicio, 1,1) as garantiaid, garantia as garantianom  from  guias.dimdiasgarantia"
        $query = "
       select substring(tiposervicio,1,1)as garantiaid, max(garantia)as garantianom 
        from guias.dimGarantias
        where substring(tiposervicio,1,1) in(select distinct garantia from guias.dimdiasgarantia where garantia not in('A','3','F'))
        group by substring(tiposervicio,1,1)
				union all 
        select '3' as garantiaID , '9:30' as garantia 
        union all 
        select 'L' as garantiaID , 'LTL' as garantia 
        order by 1";

        //$query="SELECT * from guias.dimDiasGarantia WHERE GARANTIA='7' AND ZONA =3;";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //METODO PARA PLAZAS 
    public static function obtenerPlazas() {
        $query = "
        select max(id) as locationid, code as locationcode from tr1.dimlocations
        where typecode in ('COP') or  typecode in ('PLZ', 'AER')
        and code IN ('MX2','MXO','CVA','AMX','COR','PAZ','TXC','XAL')
        group by code order by code asc";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }
    
    public static function obtenerPlazasUsuario($id) {
        $query = "
        select locationid, locationcode
        from admin.dimusuariosplaza
        where usuarioid=" . $id . "
        group by locationid, locationcode
        order by locationcode asc";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    public static function obtenerPlazasUsuario($id) {
        $query = "
        select locationid, locationcode 
        from admin.dimusuariosplaza 
        where usuarioid=" . $id . "
        group by locationid, locationcode
        order by locationcode asc";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }
    
        public static function getLlegadas() {
//        $date = str_replace('/', '-', $fecha_filtro);
//        $fecha_filtro = date("Y-m-d", strtotime($date));
        $query = "
        select count(*) llegada
        from admin.dimusuariosplaza 
        where usuarioid=1 
		group by usuarioid";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    public static function getPendientes() {
//        $date = str_replace('/', '-', $fecha_filtro);
//        $fecha_filtro = date("Y-m-d", strtotime($date));
        $query = "
        select count(*) pendiente
        from admin.dimusuariosplaza 
        where usuarioid=1 
		group by usuarioid";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }
    
}
