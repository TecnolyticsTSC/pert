<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
class cumplimientoTR1Model extends Model
{   
    public static function getIdRoutes()
    {
        $query = "
            SELECT DISTINCT code FROM tr1.facttravel
        ";

        return  DB::select(
            $query
        );
    }

    public static function getPlazas()
    {
        $query = "
            SELECT DISTINCT warehouse FROM tr1.dimroute
        ";

        return  DB::select(
            $query
        );
    }

    public static function getTypeRoutes()
    {
        $query = "
            SELECT 
                DISTINCT
                CASE 
                    WHEN isaditionaltravel = true THEN 'Extra'
                    WHEN isaditionaltravel = false THEN 'Programado'
                END AS type
            FROM tr1.facttravel
        ";

        return  DB::select(
            $query
        );
    }
    
    public static function getOrigin()
    {
        $query = "
            SELECT DISTINCT locationcodeorigin FROM tr1.facttravel
        ";

        return  DB::select(
            $query
        );
    }

    public static function getDestiny()
    {
        $query = "
            SELECT DISTINCT locationcodedestiny FROM tr1.facttravel
        ";

        return  DB::select(
            $query
        );
    }

    public static function getNOperator()
    {
        $query = "
            SELECT DISTINCT code FROM tr1.dimdriverstr1
        ";

        return  DB::select(
            $query
        );
    }

    public static function getTypeVehicle()
    {
        $query = "
            SELECT DISTINCT typename FROM tr1.dimtransportunit
        ";

        return  DB::select(
            $query
        );
    }

    public static function getVehicle()
    {
        $query = "
            SELECT DISTINCT code FROM tr1.dimtransportunit
        ";

        return  DB::select(
            $query
        );
    }

    public static function getStatus()
    {
        $query = "
            SELECT DISTINCT status FROM tr1.facttravel
        ";

        return  DB::select(
            $query
        );
    }

    public static function getDataGraphic($id_route_filter, $plaza_filter, $type_route_filter, $origin_filter, $destiny_filter, $n_operator_filter, $type_vehicle_filter,
    $vehicle_filter, $por_ocupation_filter, $status_filter, $type_incidences_filter, $type_event_filter, $start_date_filter, $end_date_filter) {
        
        //Start: condition id_route
        if (count($id_route_filter) > 0) {
            $id_route_filter = "'" . implode("', '", $id_route_filter) . "'";
            $id_route_filter = "AND idroute IN (" . $id_route_filter . ") ";
        } else {
            $id_route_filter = "";
        }
        //End: condition id_route

        //Start: condition plaza_filter
        if (count($plaza_filter) > 0) {
            $plaza_filter = "'" . implode("', '", $plaza_filter) . "'";
            $plaza_filter = "AND plaza IN (" . $plaza_filter . ") ";
        } else {
            $plaza_filter = "";
        }
        //End: condition plaza_filter

        //Start: condition type_route_filter
        if (count($type_route_filter) > 0) {
            $type_route_filter = "'" . implode("', '", $type_route_filter) . "'";
            $type_route_filter = "AND isaditionaltravel IN (" . $type_route_filter . ")  ";
        } else {
            $type_route_filter = "";
        }
        //End: condition type_route_filter

        //Start: condition origin_filter
        if (count($origin_filter) > 0) {
            $origin_filter = "'" . implode("', '", $origin_filter) . "'";
            $origin_filter = "AND locationcodeorigin IN (" . $origin_filter . ") ";
        } else {
            $origin_filter = "";
        }
        //End: condition origin_filter

        //Start: condition destiny_filter
        if (count($destiny_filter) > 0) {
            $destiny_filter = "'" . implode("', '", $destiny_filter) . "'";
            $destiny_filter = "AND locationcodedestiny IN (" . $destiny_filter . ") ";
        } else {
            $destiny_filter = "";
        }
        //End: condition destiny_filter

        //Start: condition n_operator_filter
        if (count($n_operator_filter) > 0) {
            $n_operator_filter = "'" . implode("', '", $n_operator_filter) . "'";
            $n_operator_filter = "AND noperator IN (" . $n_operator_filter . ") ";
        } else {
            $n_operator_filter = "";
        }
        //End: condition n_operator_filter

        //Start: condition type_vehicle_filter
        if (count($type_vehicle_filter) > 0) {
            $type_vehicle_filter = "'" . implode("', '", $type_vehicle_filter) . "'";
            $type_vehicle_filter = "AND typevehicle IN (" . $type_vehicle_filter . ") ";
        } else {
            $type_vehicle_filter = "";
        }
        //End: condition type_vehicle_filter

        //Start: condition vehicle_filter
        if (count($vehicle_filter) > 0) {
            $vehicle_filter = "'" . implode("', '", $vehicle_filter) . "'";
            $vehicle_filter = "AND necovehicle IN (" . $vehicle_filter . ") ";
        } else {
            $vehicle_filter = "";
        }
        //End: condition vehicle_filter

        //Start: condition por_ocupation_filter
        if ($por_ocupation_filter == 0) {
            $por_ocupation_filter = "AND percentagecharge::INTEGER >= " . $por_ocupation_filter . " ";
        } else {
            $por_ocupation_filter = "AND percentagecharge::INTEGER <= " . $por_ocupation_filter . " ";
        }
        //End: condition por_ocupation_filter

        //Start: condition status_filter
        if (count($status_filter) > 0) {
            $status_filter = "'" . implode("', '", $status_filter) . "'";
            $status_filter = "AND status IN (" . $status_filter . ") ";
        } else {
            $status_filter = "";
        }
        //End: condition status_filter

        //Start: condition type_event_filter
        $conditional_type_event_date = "EXTRACT(EPOCH FROM CONCAT(EXTRACT(DAY FROM Planneddatefinish - datefinish),' days ',
            EXTRACT(HOUR FROM Plannedtimefinish - timefinish), ' hours ',
            EXTRACT(MINUTE FROM Plannedtimefinish - timefinish), ' minutes')
                ::INTERVAL)/60 AS minutes";
        $conditional_type_event_status = "status = 'FINALIZADO'";
        if ($type_event_filter == 'Salidas') {
            $conditional_type_event_date = "EXTRACT(EPOCH FROM CONCAT(EXTRACT(DAY FROM Planneddatestart - datestart),' days ',
            EXTRACT(HOUR FROM Plannedtimestart - timestart), ' hours ',
            EXTRACT(MINUTE FROM Plannedtimestart - timestart), ' minutes')
                ::INTERVAL)/60 AS minutes";
            $conditional_type_event_status = "status = 'INICIADO' OR status = 'FINALIZADO'";
        }
        else if ($type_event_filter == 'En tránsito') {
            $conditional_type_event_date = "0 AS minutes";
            $conditional_type_event_status = "status = 'INICIADO'";
        }
        //End: condition type_event_filter

        //Start: condition dates
        $date_filter = "";
        if($start_date_filter !== "" && $end_date_filter != "" ){
            if ($type_event_filter == 'Llegadas') {
                $date_filter = "AND datefinish >= TO_TIMESTAMP('".$start_date_filter."', 'MM-dd-YYYY') AND datefinish <= TO_TIMESTAMP('".$end_date_filter."', 'MM-dd-YYYY')";
            }
            else if ($type_event_filter == 'Salidas' || $type_event_filter == 'Transito') {
                $date_filter = "AND datestart >= TO_TIMESTAMP('".$start_date_filter."', 'MM-dd-YYYY') AND timestart < TO_TIMESTAMP('".$end_date_filter."', 'MM-dd-YYYY') ";
            }
        }
        //End: condition type_event_filter

        $query = "
        SELECT 
            CONCAT(
                TO_CHAR(datestart :: DATE, 'dd/mm/yyyy'),
                ' ',
                TO_CHAR(timestart :: TIME, 'HH:MI:SS PM')
            ) AS dates,
            idroute,
            region,
            regional,
            plaza,
            CASE
                WHEN isaditionaltravel = true THEN 'Extra'
                WHEN isaditionaltravel = false THEN 'Programado'
            END as typeRoute,
            nOperator,
            CONCAT(firstname, ' ', lastname1, ' ', lastname2) AS nameOperator,
            idTramo,
            origendestinyTramo,
            typeVehicle,
            nEcoVehicle,
            CASE
                WHEN UPPER(classname) = 'MOTRIZ' THEN nEcoVehicle
            END as nEcoContainer1,
			CASE
                WHEN UPPER(classname) = 'MOTRIZ' THEN percentagecharge
            END as porContent1,
            CASE
                WHEN UPPER(classname) = 'ARRASTRE' OR UPPER(classname) = 'SEMIREMOLQUE' THEN nEcoVehicle
            END as nEcoContainer2,
			CASE
                WHEN UPPER(classname) = 'ARRASTRE' OR UPPER(classname) = 'SEMIREMOLQUE' THEN percentagecharge
            END as porContent2,
            CASE
                WHEN UPPER(typename) = 'DOLLY' THEN nEcoVehicle
            END as dolly,
            status,
            incidenceType,
            incidenceObservations,
            classcode,
            route,
            $conditional_type_event_date,
            umbralesjson
        FROM
                tr1.factcumplimientotr1
        WHERE
            $conditional_type_event_status
            $id_route_filter
            $plaza_filter 
            $type_route_filter
            $origin_filter
            $destiny_filter 
            $n_operator_filter
            $type_vehicle_filter
            $vehicle_filter
            $por_ocupation_filter
            $status_filter
            $date_filter
        ";

        return  DB::select(
            $query
        );

    }

    public static function createIntervalMinutes($days, $hours, $minutes){
        $query="
            SELECT EXTRACT(EPOCH FROM '$days days $hours hours $minutes minutes'::INTERVAL)/60 AS interval;
        ";

        $r = DB::select(
            $query
        );

        return $r[0]->interval;

    }

}
