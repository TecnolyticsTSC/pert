<?php

namespace App\Models\tr2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class entradasTr1 extends Model {
    /* seccion de consulta para los filtros */

    //funcion para obtener catalodo de tipoVehiculo 
    public static function obtenerTipoVehiculo() {
        $query = "
        SELECT DISTINCT typename FROM tr1.dimtransportunit";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //funcion para obtener estatus ruta
    public static function obtenerEstatusRuta() {
        $query = "
        SELECT DISTINCT status FROM tr1.facttravel";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //funcion para obtener ID Ruta
    public static function obtenerIdRuta() {
        $query = "
        SELECT DISTINCT code FROM tr1.facttravel";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //funcion para obtener tramo()
    public static function obtenerTramo() {
        $query = "
        SELECT DISTINCT code AS tramo FROM tr1.factroutesection";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //funcion para obtener numero eco 
    public static function obtenerNumeroEco() {
        $query = "
        SELECT DISTINCT code AS numeroeco FROM tr1.dimtransportunit";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }
    
    //KPI Avance de Llegadas (Contador de llegadas color verde)
    public static function getLlegadas($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) llegada
         from tr1.factTravel travel
         where travel.status='FINALIZADO'
         and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'
         " . $plazas_filtro . "";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Avance de Llegadas (Contador de pendientes color blanco)
    public static function getPendientes($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) pendiente
        from tr1.factTravel travel
        where travel.status='INICIADO'
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'
        " . $plazas_filtro . "";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Llegadas (Contador de > 3 dias color verde)
    public static function getMayorTresDias($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) mayortres
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='FINALIZADO'
        and fg.fechacompromiso > current_date + 2
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Llegadas (Contador de > 2 dias color amarillo)
    public static function getMayorDosDias($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) mayordos
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='FINALIZADO'
        and   fg.fechacompromiso         between current_date and current_date + 1
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Llegadas (Contador de < hoy color rojo)
    public static function getMenorHoy($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) menorhoy
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='FINALIZADO'
        and   fg.fechacompromiso         < current_date
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Pendientes (Contador de > 3 dias color verde)
    public static function getMayorTresDiasPendientes($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) mayortres
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='INICIADO'
        and   fg.fechacompromiso         > current_date + 2
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Pendientes (Contador de > 2 dias color amarillo)
    public static function getMayorDosDiasPendientes($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) mayordos
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='INICIADO'
        and   fg.fechacompromiso         between current_date and current_date + 1
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //KPI Garabtías de Pendientes (Contador de < hoy color rojo)
    public static function getMenorHoyPendientes($plazas_filtro, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and travel.locationcodedestiny in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and travel.locationcodedestiny in ('GDL')";
        }
        $query = "
        select count(*) menorhoy
        from tr1.factTravel travel
                inner join tr1.factLoadItem li   on li.travelid           = travel.id
                inner join guias.factItem   item on item.id                 = li.itemid
                inner join guias.factGuias  fg   on fg.vwguias_numeroguia = item.Waybill
        where travel.status='INICIADO'
        and   fg.fechacompromiso         < current_date
        " . $plazas_filtro . " 
        and travel.planneddatefinish between '" . $fecha_inicial . "' and '" . $fecha_final . "'";
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

    //Tabla Principal Llegadas TR1
    public static function getLlegadasTr1($plazas_filtro, $estatus_ruta, $id_ruta, $tramo, $cliente, $tipo_vehiculo, $numero_eco, $fecha_inicial, $fecha_final) {
        $date = str_replace('/', '-', $fecha_inicial);
        $fecha_inicial = date("Y-m-d", strtotime($date));
        $date2 = str_replace('/', '-', $fecha_final);
        $fecha_final = date("Y-m-d", strtotime($date2));
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n and plaza in ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "\n and plaza in ('GDL')";
        }
        if ($estatus_ruta !== null && $estatus_ruta !== '0' && count($estatus_ruta) > 0) {
            $estatus_ruta = "'" . implode("', '", $estatus_ruta) . "'";
            $estatus_ruta = "\n and estatus_ruta in ( " . $estatus_ruta . " ) ";
        } else {
            $estatus_ruta = "";
        }
        if ($id_ruta !== null && $id_ruta !== '0' && count($id_ruta) > 0) {
            $id_ruta = "'" . implode("', '", $id_ruta) . "'";
            $id_ruta = "\n and id_ruta in ( " . $id_ruta . " ) ";
        } else {
            $id_ruta = "";
        }
        if ($tramo !== null && $tramo !== '0' && count($tramo) > 0) {
            $tramo = "'" . implode("', '", $tramo) . "'";
            $tramo = "\n and tramo in ( " . $tramo . " ) ";
        } else {
            $tramo = "";
        }
        if ($cliente !== null && $cliente !== '0' && count($cliente) > 0) {
            $cliente = "'" . implode("', '", $cliente) . "'";
            $cliente = "\n and cliente in ( " . $cliente . " ) ";
        } else {
            $cliente = "";
        }
        if ($tipo_vehiculo !== null && $tipo_vehiculo !== '0' && count($tipo_vehiculo) > 0) {
            $tipo_vehiculo = "'" . implode("', '", $tipo_vehiculo) . "'";
            $tipo_vehiculo = "\n and tipo_vehiculo in ( " . $tipo_vehiculo . " ) ";
        } else {
            $tipo_vehiculo = "";
        }
        if ($numero_eco !== null && $numero_eco !== '0' && count($numero_eco) > 0) {
            $numero_eco = "'" . implode("', '", $numero_eco) . "'";
            $numero_eco = "\n and numero_economico in ( " . $numero_eco . " ) ";
        } else {
            $numero_eco = "";
        }
        $query = 'select ruta, 
                tramo, 
                porcentaje_carga, 
                numerador, 
                denominador, 
                excedente, 
                items_valor, 
                "9:30" nueve_treinta, 
                "11:30" once_treinta, 
                "Dia Siguiente" dia_siguiente, 
                "Terrestre" terrestre, 
                "2 Dias" dos_dias, 
                "LTL Guia Embarque" ltl, 
                "Dia Siguiente Terrestre" siguiente_terrestre, 
                items_verdes, 
                items_amarillos, 
                items_rojos
            from tr1.factllegadastr1 
            where fecha between \'' . $fecha_inicial . '\' and \'' . $fecha_final . '\'
                  ' . $plazas_filtro . '
                  ' . $estatus_ruta . '
                  ' . $id_ruta . '
                  ' . $tramo . '
                  ' . $cliente . '
                  ' . $tipo_vehiculo . '
                  ' . $numero_eco . '
            order by ruta'
        ;
        return DB::select(//WHERE table_schema = 'public'
                        $query
        );
    }

}
