<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class RutasTransitoModel extends Model 
{
    private static $TABLE_TRAVEL = "tr1.facttravel"; //vwtravel
    private static $TABLE_ROUTE = " tr1.dimroute"; //vwtravel

    public static function getGraficaPTAvsETAVerde() 
    {
                
         $query ="    SELECT count(*)  Color FROM tr1.facttravel 
	 WHERE status='FINALIZADO' 
	 AND planneddatefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59' 
	 AND datefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59' 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) =0
	";
                return DB::select($query);
    }
    public static function getGraficaPTAvsETAAmarillo() 
    {
                
         $query ="
	 SELECT count(*) as Color FROM tr1.facttravel WHERE status='FINALIZADO' 
	 AND planneddatefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	 AND datefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >0 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS smallint) <=15  ";
                return DB::select($query);
    } 
    public static function getGraficaPTAvsETARojo() 
    {
         $query =" 
	 SELECT count(*) as Color FROM tr1.facttravel WHERE status='FINALIZADO' 
	  AND planneddatefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	  AND datefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	  AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >15 ";
          return DB::select($query);
    }      
    
    public static function getGraficaPTAvsETA() 
    {
        ///count(*) as total,
         $query = 
                "
                SELECT 
		(SELECT count(*) 
		 FROM ".self::$TABLE_TRAVEL." 
		 WHERE status='FINALIZADO' 
		 AND planneddatefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59'  
		 AND datefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59' 
		 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) =0 ) as Verde,
		( SELECT count(*) 
		 FROM ".self::$TABLE_TRAVEL." 
		 WHERE status='FINALIZADO' 
		 AND planneddatefinish BETWEEN  '2020-05-18' AND '2020-05-18'  
		 AND datefinish BETWEEN   '2020-05-18' AND '2020-05-18'  
		 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >0 
		 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS smallint)  <=15  ) as Amarillo,	
		(SELECT count(*) FROM ".self::$TABLE_TRAVEL." 
		 WHERE status='FINALIZADO' 
		 AND planneddatefinish BETWEEN   '2020-05-18' AND '2020-05-18'  
		 AND datefinish BETWEEN  '2020-05-18' AND '2020-05-18'
		 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >15 ) as Rojo		 
                FROM ".self::$TABLE_TRAVEL."
                WHERE status='FINALIZADO' 
                AND planneddatefinish BETWEEN  '2020-05-18' AND '2020-05-18'
                ";
         
         $query ="    SELECT count(*)  Color FROM tr1.facttravel 
	 WHERE status='FINALIZADO' 
	 AND planneddatefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59' 
	 AND datefinish BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59' 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) =0
	 UNION ALL
	 SELECT count(*) as Color FROM tr1.facttravel WHERE status='FINALIZADO' 
	 AND planneddatefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	 AND datefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >0 
	 AND CAST(date_part('hour', timefinish-plannedtimefinish) AS smallint) <=15 
	 UNION ALL
	 SELECT count(*) as Color FROM tr1.facttravel WHERE status='FINALIZADO' 
	  AND planneddatefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	  AND datefinish BETWEEN '2020-05-18' AND '2020-05-18' 
	  AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >15 ";
                return DB::select($query);
    }
    public static function getGraficaPTAvsRTA() {
        return DB::select(
                "
SELECT  (SELECT count(*)  FROM tr1.facttravel
		WHERE status='FINALIZADO' AND datestart BETWEEN  '2020-05-18' AND '2020-05-18')as TotalSalida,
	   (SELECT count(*) FROM tr1.facttravel 
		WHERE status='FINALIZADO' AND datestart BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timestart-plannedtimestart) AS INTEGER) <=0
	    ) as SalidasVerde,
		(SELECT count(*) FROM tr1.facttravel  
		WHERE status='FINALIZADO' AND datefinish BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) <=0
	    ) as LlegadaVerde,
	   (SELECT count(*) FROM tr1.facttravel 
		WHERE status='FINALIZADO' AND datestart BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timestart-plannedtimestart) AS INTEGER) >0 
		AND CAST(date_part('hour', timestart-plannedtimestart) AS smallint)  <=15
	   ) as SalidasAmarillo,
	   (SELECT count(*) FROM tr1.facttravel  
		WHERE status='FINALIZADO' AND datefinish BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >0 
		AND CAST(date_part('hour', timefinish-plannedtimefinish) AS smallint)  <=15
	   ) as LLegadaAmarillo,	   
	   (SELECT count(*) FROM tr1.facttravel 
		WHERE status='FINALIZADO' AND datestart BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timestart-plannedtimestart) AS INTEGER) >15
	   ) as SalidasRojo,
	   (SELECT count(*) FROM tr1.facttravel  
		WHERE status='FINALIZADO' AND datefinish BETWEEN  '2020-05-18' AND '2020-05-18'
		AND CAST(date_part('hour', timefinish-plannedtimefinish) AS INTEGER) >15
	   ) as LLegadaRojo,
	   (SELECT count(*) 
	   FROM tr1.facttravel 
	   WHERE status='FINALIZADO' AND datefinish BETWEEN  '2020-05-18' AND '2020-05-18') as TotalLlegada
           
                "
        );
    }  
    
public static function getDetalleMovimTransito() {
        return DB::select(
               "SELECT t.code as CodeTravel,r.code as CodeRuta,TO_TIMESTAMP(to_char(t.datestart, 'YYYY-MM-DD') || ' ' || 
	   to_char(t.timestart, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')FechaHr,
	   t.id,t.datestart,t.timestart,
	    r.code AS IdRuta,
        CASE WHEN r.regionaltt ='BAOC' THEN 'BAOC'
			 WHEN r.regionaltt = 'PANO' THEN 'PANO' 
		ELSE 'CESU'
		END AS  Region,
	   /*(SELECT shortregional from dimLocations WHERE code=r.Warehouse) AS Regional, */
	   r.Warehouse AS Plaza,
	   CASE WHEN r.isadditionalstraps ='false' THEN 'Fijo'
	   		ELSE 'Extra' END AS TipoRuta,
	   /*(SELECT code  FROM vwRouteSection 
		WHERE id IN (SELECT routesectionId FROM vwRouteSectionRoute  
			 		 WHERE frecuencyid in (SELECT id FROM vwFrecuency where routeid=r.id))) as IdTramo,*/		
	   t.locationcodeorigin,t.locationcodedestiny,
	   /*(SELECT id FROM vwFrecuency Where routeId=r.id) as FrecuencyID ,*/
	   (SELECT case MAX(STARTWEEKDAY) when 'LUNES' then 1 else  0 end 
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute  where frecuencyID=FrecuencyID)
		AND code=t.schedulecode ----AND code='W1900'
	   )as Lun, 
	   (SELECT case MAX(STARTWEEKDAY) when 'MARTES' then 1 else  0 end
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Mar, 
	  (SELECT case MAX(STARTWEEKDAY) when 'MIERCOLES' then 1 else  0 end
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Mier, 
	  (SELECT case MAX(STARTWEEKDAY) when 'JUEVES' then 1 else  0 end
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Juev,
	  (SELECT case MAX(STARTWEEKDAY) when 'VIERNES' then 1 else  0 end
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Vier,
	  (SELECT case MAX(STARTWEEKDAY) when 'SABADO' then 1 else  0 end
	    FROM tr1.dimschedule 
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Sab,	
	  (SELECT case MAX(STARTWEEKDAY) when 'DOMINGO' then 1 else  0 end
	    FROM tr1.dimschedule  
		where routesectionrouteid in(select id from tr1.factroutesectionroute where frecuencyID=FrecuencyID)
		AND code=t.schedulecode 
	   )as Dom,		  
	   ----------Vehiculo-----------------------------
	   t.formationtypecode,
	   (SELECT typename || '-' || Code FROM tr1.dimtransportunit 
		WHERE id in (SELECT transportunitid from tr1.facttraveltransportunitplanned
				     WHERE travelId=t.id AND isexecuted='true')
	   )as Vehiculo,
	   r.cargotype AS TipoCarga, ----MYP Mensajeria y PAqueteria
	   t.status AS Estatus,
	   (select type from tr1.factincidence WHERE travelid=t.id ORDER BY localeventdatetime desc limit 1) AS Incidencia, 
	   (Select localeventdatetime from tr1.facttransportunitclose where travelid=t.id limit 1 ) AS CCON,
	   (SELECT  CASE WHEN CAST(date_part('minute', t.plannedTimeStart-localeventdatetime) AS INTEGER) >=0 
					  AND CAST(date_part('minute', t.plannedTimeStart-localeventdatetime) AS INTEGER) < 20 THEN 'Verde'
				ELSE 'Rojo'
				END AS  colorCCON
		FROM tr1.facttransportunitclose WHERE travelid=t.id 
		AND localeventdatetime BETWEEN '2020-05-18 00:00:00' AND '2020-05-18 23:59:59'
	   )ColorCCON,
	   TO_TIMESTAMP(to_char(t.planneddatestart, 'YYYY-MM-DD') || ' ' || 
	   to_char(t.plannedTimeStart, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')PlanSalida,
	   TO_TIMESTAMP(to_char(t.planneddatefinish, 'YYYY-MM-DD') || ' ' || 
	   to_char(t.plannedTimeFinish, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')PlanLlegada,	   
	   '' as EstimadoSalida,
	   '' as EstimadoLlegada,	
	   TO_TIMESTAMP(to_char(t.datestart, 'YYYY-MM-DD') || ' ' || 
	   to_char(t.timestart, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')RealSalida,
	   TO_TIMESTAMP(to_char(t.datefinish, 'YYYY-MM-DD') || ' ' || 
	   to_char(t.timefinish, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')RealLlegada
FROM  tr1.facttravel t
JOIN  tr1.dimroute r ON r.id=t.routeid
WHERE t.datestart BETWEEN '2020-05-18' AND '2020-05-18'::date + INTERVAL '1 day'
AND '[2020-05-18 00:00:00,2020-05-19 23:59:59)'::tsrange @> 
	TO_TIMESTAMP(to_char(t.datestart, 'YYYY-MM-DD') || ' ' || 
	to_char(t.timestart, 'HH24:MI:SS'), 'YYYY/MM/DD HH24:MI:SS')::timestamp =true
AND status='FINALIZADO'
ORDER BY r.id
"
        );
    }  
    
    
    
    
 
}
