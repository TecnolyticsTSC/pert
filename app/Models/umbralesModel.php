<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class umbralesModel extends Model
{
    public $updated_at = 'updated_at';
    public $created_at = 'created_at';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $table = "tr1.factumbralestr1";
    protected $fillable = ['idroute','umbralesjson', 'green', 'yellow','red' ,'updated_at', 'created_at'];

    public static function getRoutesUmbrals()
    {
        //Query for get routes and umbrals
        $query = "
            SELECT 
                travel.code AS idRoute,
                route.id AS route,
                travel.description,
                travel.locationcodeorigin AS Origen,
                travel.locationcodedestiny AS Destino,
                CONCAT(TO_CHAR(travel.Planneddatestart :: DATE, 'dd/mm/yyyy') ,' ', TO_CHAR(travel.Plannedtimestart :: TIME, 'HH:MI:SS PM')) AS ptaSalida,
                CONCAT(TO_CHAR(travel.Planneddatefinish :: DATE, 'dd/mm/yyyy') ,' ', TO_CHAR(travel.Plannedtimefinish :: TIME, 'HH:MI:SS PM')) AS ptaLlegada,
                ABS(EXTRACT(HOUR FROM travel.Plannedtimefinish - travel.Plannedtimestart)) as hours,
                ABS(EXTRACT(MINUTE FROM travel.Plannedtimefinish - travel.Plannedtimestart)) as minutes,
                schedule.startweekday,
                umbral.umbralesjson
            FROM 
                tr1.facttravel AS travel   
                INNER JOIN tr1.dimroute AS route ON route.id = travel.routeid 
                INNER JOIN tr1.dimschedule AS schedule ON schedule.routesectionrouteid = route.id
                LEFT JOIN tr1.factumbralestr1 AS umbral ON route.id  = umbral.idroute
        ";

        return  DB::select(
            $query
        );
    }
    
    public static function getRouteUmbral($id)
    {
        //Query for get route and umbral selected by user
        $query = "
        SELECT 
            route.code AS idRoute,
            route.id AS Route,
            umbral.umbralesjson 
        FROM 
            tr1.dimroute AS route
            LEFT JOIN tr1.factumbralestr1 AS umbral ON route.id = umbral.idroute
        WHERE 
            route.id = '$id'
        ";

        return  DB::select(
            $query
        );
    }

}
