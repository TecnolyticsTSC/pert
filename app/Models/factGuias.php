<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class factGuias extends Model {

    //vars statics
    private static $TABLE_NAME_GUIAS = "guias.factGuias";
    private static $TABLE_NAME_RUTAS = "tr2.dimRutaTR2CP";
    private static $TABLE_NAME_FREC_MP = "tr2.dimFrecuenciasMP";
    private static $TABLE_NAME_FREC_LTL = "tr2.dimFrecuenciasLTL";
    public $timestamps = false;
    protected $table = "guias.factguias";

    //protected $table = "guiasdimGarantias";

    public static function getCodigosClientes() {
        return DB::select(
                        "
            
            select distinct
            --COALESCE(vwenvios_numerocliente, vwopguia_numcte) as numerocliente
            CASE WHEN vwenvios_numerocliente IS NULL THEN vwopguia_numcte ELSE vwenvios_numerocliente END as numerocliente
            
            from guias.factGuias
            order by 1;
            "
        );
    }

    /* ___________________DATOS PARA LA GRÁFICA PRINCIPAL STACK 0________________________________________ */

    public static function getRecoleccionTR1($fecha_filtro = '2020-03-04', $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro) {

        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));

        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";

            $plazas_filtro = "\n AND fg.vwopenvio_siglasdes IN ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = "\n AND fg.garantia_usar IN ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }

        if ($cliente_filtro !== null && $cliente_filtro !== '0') {
            $cliente_filtro = "\n AND fg.vwenvios_numerocliente  = '" . $cliente_filtro . "'\n";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '') {
            $guia_filtro = "\n AND fg.vwguias_numeroguia = '" . $guia_filtro . "'\n";
        } else {
            $guia_filtro = "";
        }

        $query = "


with
    diasgar0 as
  (
    /* DIA-30*/
    select 'D-0'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'F'::text as garantia, 0 as cantguias
    UNION ALL
    /* DIA 0*/
    select 'D+0'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'F'::text as garantia, 0 as cantguias
    UNION ALL
    /* DIA 1*/
    select 'D+1'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 2*/
    UNION ALL
    select 'D+2'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 3*/
    UNION ALL
    select 'D+3'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 4*/
    UNION ALL
    select 'D+4'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 5*/
    UNION ALL
    select 'D+5'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'F'::text as garantia, 0 as cantguias
  ),
    diasgarreal as
  (
    /* Select Real */
      SELECT
            'D-0' dia,
            fg.garantia_usar,
            count(fg.vwguias_numeroguia) as cantguias
        FROM
            tr2.factPickupVisitItem fpv
            inner join guias.factItem fi on fi.id = fpv.vwpickupvisititem_itemid
            inner join guias.factGuias fg on fg.vwguias_numeroguia = fi.waybill
        WHERE
            vwopenvio_fechaentrega IS NULL 
            AND fechacompromiso between CURRENT_DATE - 30 AND CURRENT_DATE
            $plazas_filtro
            $garantias_filtro
            $cliente_filtro
            $guia_filtro
                AND fg.garantia_usar IS NOT NULL
            AND substring(fg.vwguias_numeroguia,14,1) IN (
                select  distinct  substr(tiposervicio, 1,1) as garantiaid
                from  guias.dimGarantias
                where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                )
        GROUP BY 1,2
      UNION ALL
      SELECT
                CASE 
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '0 day' THEN 'D+0'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '1 day' THEN 'D+1'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '2 day' THEN 'D+2'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '3 day' THEN 'D+3'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '4 day' THEN 'D+4'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                    WHEN fpv.vwpickupvisit_planneddate::date = '$fecha_filtro'::date + INTERVAL '5 day' THEN 'D+5'-- || to_char(fpv.vwpickupvisit_planneddate, 'Dy')
                END
                dia,
            fg.garantia_usar,
            count(*) as cantguias
            FROM
            tr2.factPickupVisitItem fpv
            inner join guias.factItem fi on fi.id = fpv.vwpickupvisititem_itemid
            inner join guias.factGuias fg on fg.vwguias_numeroguia = fi.waybill
            WHERE
            fpv.vwpickupvisit_planneddate::date BETWEEN '$fecha_filtro'::date AND '$fecha_filtro'::date + INTERVAL '5 day'
            $plazas_filtro
            $garantias_filtro
            $cliente_filtro
            $guia_filtro
            AND fg.garantia_usar IS NOT NULL
            AND substring(fg.vwguias_numeroguia,14,1) IN (
                select  distinct  substr(tiposervicio, 1,1) as garantiaid
                from  guias.dimGarantias
                where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                order by 1
            )
            GROUP BY 1,2
            ORDER BY 1 DESC
  )
select * from diasgarreal
union all
select * from diasgar0
where not exists
(select * from diasgarreal
    where diasgar0.dia = diasgarreal.dia
          and diasgar0.garantia = diasgarreal.garantia_usar
)
order by 1,2
            ";

        //die($query);
        return DB::select(
                        $query
        );
    }

    public static function getPlazasAgrupadas($plaza_param) {
        $plaza_param = "'" . implode("', '", $plaza_param) . "'";
        $query = "select sector from tr2.dimsectores where cop in (" . $plaza_param . ") order by sector";
        //select sector from tr2.dimsectores where cop = 'GDL' order by sector;
        return DB::select($query);
    }

    public static function getEntregasTR1($fecha_filtro, $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro, $fecha_entrega, $codigo_postalNull) {
        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));

        if ($codigo_postalNull == "false") {
            $qrfiltroCP = "\n AND vwopenvio_codigopos IS NOT NULL \n";
        } else {
            $qrfiltroCP = "";
        }

        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $plazas_filtro = "\n AND fg.vwopenvio_siglasdes IN ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "";
        }
        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = "\n AND fg.garantia_usar IN ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }
        if ($cliente_filtro !== null && $cliente_filtro !== '0') {
            $cliente_filtro = "\n AND fg.vwenvios_numerocliente  = '" . $cliente_filtro . "'\n";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '') {
            $guia_filtro = "\n AND fg.vwguias_numeroguia = '" . $guia_filtro . "'\n";
        } else {
            $guia_filtro = "";
        }
        if ($fecha_entrega == "false") {
            $fecha_entrega = "\n fg.vwopenvio_fechaentrega IS NULL AND \n";
        } else {
            $fecha_entrega = "";
        }
        $query = "
            with
    diasgar0 as
  (
  /* DIA -30*/
    select 'D-0'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D-0'::text as dia, 'F'::text as garantia, 0 as cantguias
    UNION ALL
    /* DIA 0*/
    select 'D+0'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+0'::text as dia, 'F'::text as garantia, 0 as cantguias
    UNION ALL
    /* DIA 1*/
    select 'D+1'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+1'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 2*/
    UNION ALL
    select 'D+2'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+2'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 3*/
    UNION ALL
    select 'D+3'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+3'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 4*/
    UNION ALL
    select 'D+4'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+4'::text as dia, 'F'::text as garantia, 0 as cantguias
    /* DIA 5*/
    UNION ALL
    select 'D+5'::text as dia, '5'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'D'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'A'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '3'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '6'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'T'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, '7'::text as garantia, 0 as cantguias
    UNION ALL
    select 'D+5'::text as dia, 'F'::text as garantia, 0 as cantguias
  ),
    diasgarreal as
  (
    /* Select Real */
        SELECT
            'D-0' dia,
            fg.garantia_usar,
            count(fg.vwguias_numeroguia) as cantguias
        FROM
            guias.factGuias fg
        WHERE
            $fecha_entrega
            fechacompromiso IS NOT NULL
            $qrfiltroCP
            AND fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1
            $plazas_filtro
            $garantias_filtro
            $cliente_filtro
            $guia_filtro
        AND substring(fg.vwguias_numeroguia,14,1) IN (
        select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
        order by 1
                                                    )
        AND  NOT exists (
		   SELECT DISTINCT fh.idguia FROM guias.facthistoria fh 
		   WHERE fh.idguia = fg.vwguias_numeroguia AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
        AND fg.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = fg.VWGUIAS_NUMEROGUIA)
        AND fg.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date	
        GROUP BY 1,2
      UNION ALL
      SELECT
        CASE
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '0 day' THEN 'D+0'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '1 day' THEN 'D+1'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '2 day' THEN 'D+2'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '3 day' THEN 'D+3'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '4 day' THEN 'D+4'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '5 day' THEN 'D+5'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
        END
        dia,
        fg.garantia_usar,
        count(fg.vwguias_numeroguia) as cantguias
      FROM
        guias.factGuias fg
      WHERE
        $fecha_entrega 
        fechacompromiso IS NOT NULL
        $qrfiltroCP
        AND fg.fechacompromiso::date BETWEEN '$fecha_filtro'::date AND  '$fecha_filtro'::date + INTERVAL '5 day'
        $plazas_filtro
        $garantias_filtro
        $cliente_filtro
        $guia_filtro
       AND substring(fg.vwguias_numeroguia,14,1) IN (
      select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
      order by 1
                                                    )
                                                    AND  NOT exists (
           SELECT DISTINCT fh.idguia FROM guias.facthistoria fh
           WHERE fh.idguia = fg.vwguias_numeroguia 
           AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
      AND fg.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = fg.VWGUIAS_NUMEROGUIA)
      AND fg.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
      GROUP BY 1,2
      ORDER BY 1
  )
  select * from diasgarreal
  union all
  select * from diasgar0
  where not exists
  (select * from diasgarreal
    where diasgar0.dia = diasgarreal.dia
          and diasgar0.garantia = diasgarreal.garantia_usar
)
order by 1,2;
            ";
        //die($query);
        return DB::select(
                        $query
        );
    }

//        public static function getEntregasTR1($fecha_filtro, $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro, $fecha_entrega, $codigo_postalNull) {
//        $date = str_replace('/', '-', $fecha_filtro);
//        $fecha_filtro = date("Y-m-d", strtotime($date));
//        
//        if ($codigo_postalNull == "false" ) {
//            $qrfiltroCP = "\n AND vwopenvio_codigopos IS NOT NULL \n";
//        } else {
//            $qrfiltroCP = "";
//        }         
//
//        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
//            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
//            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
//            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
//            $plazas_filtro = "\n AND fg.vwopenvio_siglasdes IN ( " . $plazas_filtro . " ) ";
//        } else {
//            $plazas_filtro = "";
//        }
//        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {
//            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
//            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";
//
//            $garantias_filtro = "\n AND fg.garantia_usar IN ( " . $garantias_filtro . " ) ";
//        } else {
//            $garantias_filtro = "";
//        }
//        if ($cliente_filtro !== null && $cliente_filtro !== '0') {
//            $cliente_filtro = "\n AND fg.vwenvios_numerocliente  = '" . $cliente_filtro . "'\n";
//        } else {
//            $cliente_filtro = "";
//        }
//        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '') {
//            $guia_filtro = "\n AND fg.vwguias_numeroguia = '" . $guia_filtro . "'\n";
//        } else {
//            $guia_filtro = "";
//        }
//        if ($fecha_entrega == "true" ){
//            $fecha_entrega = "\n fg.vwopenvio_fechaentrega IS NULL AND \n";
//        }else  {
//            $fecha_entrega = "";            
//        }
//        $query = "
//            with
//    diasgar0 as
//  (
//  /* DIA -30*/
//    select 'D-0'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D-0'::text as dia, 'F'::text as garantia, 0 as cantguias
//    UNION ALL
//    /* DIA 0*/
//    select 'D+0'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+0'::text as dia, 'F'::text as garantia, 0 as cantguias
//    UNION ALL
//    /* DIA 1*/
//    select 'D+1'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+1'::text as dia, 'F'::text as garantia, 0 as cantguias
//    /* DIA 2*/
//    UNION ALL
//    select 'D+2'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+2'::text as dia, 'F'::text as garantia, 0 as cantguias
//    /* DIA 3*/
//    UNION ALL
//    select 'D+3'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+3'::text as dia, 'F'::text as garantia, 0 as cantguias
//    /* DIA 4*/
//    UNION ALL
//    select 'D+4'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+4'::text as dia, 'F'::text as garantia, 0 as cantguias
//    /* DIA 5*/
//    UNION ALL
//    select 'D+5'::text as dia, '5'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, 'D'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, 'A'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, '3'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, '6'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, 'T'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, '7'::text as garantia, 0 as cantguias
//    UNION ALL
//    select 'D+5'::text as dia, 'F'::text as garantia, 0 as cantguias
//  ),
//    diasgarreal as
//  (
//    /* Select Real */
//        SELECT
//            'D-0' dia,
//            fg.garantia_usar,
//            count(fg.vwguias_numeroguia) as cantguias
//        FROM
//            guias.factGuias fg
//        WHERE
//            $fecha_entrega
//            fechacompromiso IS NOT NULL
//            $qrfiltroCP
//            AND fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1
//            $plazas_filtro
//            $garantias_filtro
//            $cliente_filtro
//            $guia_filtro
//        AND substring(fg.vwguias_numeroguia,14,1) IN (
//        select  distinct  substr(tiposervicio, 1,1) as garantiaid
//                                                  from  guias.dimGarantias
//                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
//        order by 1
//                                                    )
//        AND  NOT exists (
//		   SELECT DISTINCT fh.idguia FROM guias.facthistoria fh 
//		   WHERE fh.idguia = fg.vwguias_numeroguia AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
//	GROUP BY 1,2
//      UNION ALL
//      SELECT
//        CASE
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '0 day' THEN 'D+0'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '1 day' THEN 'D+1'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '2 day' THEN 'D+2'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '3 day' THEN 'D+3'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '4 day' THEN 'D+4'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        WHEN fg.fechacompromiso::date = '$fecha_filtro'::date + INTERVAL '5 day' THEN 'D+5'-- || CASE to_char(fg.fechacompromiso, 'Dy') WHEN 'Mon' THEN 'Lun' WHEN 'Tue' THEN 'Mar' WHEN 'Wed' THEN 'Mie' WHEN 'Thu' THEN 'Jue' WHEN 'Fri' THEN 'Vie' WHEN 'Sat' THEN 'Sab' WHEN 'Sun' THEN 'Dom' END || ' ' || to_char(fg.fechacompromiso, 'DD')
//        END
//        dia,
//        fg.garantia_usar,
//        count(fg.vwguias_numeroguia) as cantguias
//      FROM
//        guias.factGuias fg
//      WHERE
//        $fecha_entrega 
//        fechacompromiso IS NOT NULL
//        $qrfiltroCP
//        AND fg.fechacompromiso::date BETWEEN '$fecha_filtro'::date AND  '$fecha_filtro'::date + INTERVAL '5 day'
//        $plazas_filtro
//        $garantias_filtro
//        $cliente_filtro
//        $guia_filtro
//       AND substring(fg.vwguias_numeroguia,14,1) IN (
//      select  distinct  substr(tiposervicio, 1,1) as garantiaid
//                                                  from  guias.dimGarantias
//                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
//      order by 1
//                                                    )
//      GROUP BY 1,2
//      ORDER BY 1
//  )
//  select * from diasgarreal
//  union all
//  select * from diasgar0
//  where not exists
//  (select * from diasgarreal
//    where diasgar0.dia = diasgarreal.dia
//          and diasgar0.garantia = diasgarreal.garantia_usar
//)
//order by 1,2;
//            ";
//        //die($query);
//        return DB::select(
//                        $query
//        );
//    }

    public static function getOrigenAnterior($fecha_filtro = '2020-03-04', $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro, $dia_seleccionado) {

        $plzs = self::findSectores($plazas_filtro);



        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));

        if ($plzs == !"") {
            $plazas_filtro = " ( " . $plzs . " ) ";
        } else {
            $plazas_filtro = "";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = " ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }

        $fg_garantia_usar = "";
        if ($garantias_filtro != "") {
            //AND fg.garantia_usar IN ( '' )
            $fg_garantia_usar = "AND fg.garantia_usar IN " . $garantias_filtro;
        }
        $ffg_garantia_usar = "";
        if ($garantias_filtro != "") {
            //AND fg.garantia_usar IN ( '' )
            $ffg_garantia_usar = "AND ffg.garantia_usar IN " . $garantias_filtro;
        }

        if ($cliente_filtro !== null && $cliente_filtro !== '0') {
            $cliente_filtro = " '" . $cliente_filtro . "' ";
        } else {
            $cliente_filtro = "";
        }


        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '') {
            $guia_filtro = " '" . $guia_filtro . "' ";
        } else {
            $guia_filtro = "";
        }
        $fg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $fg_vwguias_numeroguia = "AND fg.vwguias_numeroguia = " . $guia_filtro;
        }
        $ffg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $ffg_vwguias_numeroguia = "AND ffg.vwguias_numeroguia = " . $guia_filtro;
        }

        //Inicializar la variable en vacío para evitar missing
        $fg_siglasdes_usar = "";
        if ($plazas_filtro != "") {
            $fg_siglasdes_usar = "AND fg.siglasdes_usar IN " . $plazas_filtro;
        }




        $fli_unloadwarehousecode = "";
        if ($plazas_filtro != "") {
            $fli_unloadwarehousecode = "AND fli.unloadwarehousecode IN " . $plazas_filtro;
        }

        $fmp_siglasplaza = "";
        if ($plazas_filtro != "") {
            $fmp_siglasplaza = "AND fmp.siglasplaza IN " . $plazas_filtro;
        }
//Start: condition -30
        $conditional = "fechacompromiso::date = '" . $fecha_filtro . "'::date + interval '" . $dia_seleccionado . " day'";

        if ($dia_seleccionado == '-30') {
            $conditional = 'fechacompromiso between CURRENT_DATE -30 AND CURRENT_DATE';
        }
        //End: condition -30

        $query = "
 SELECT
 fli.warehousecode AS origen_anterior,
 fg.garantia_usar,
 count(*) AS cantguias
 FROM guias.factguias fg
 INNER JOIN guias.factitem fi ON fi.waybill = fg.vwguias_numeroguia
 INNER JOIN tr1.factloaditem fli ON fli.itemid = fi.id
 WHERE
 " . $conditional . "
 AND fg.garantia_usar IN ('D', '7', 'L') /* Garantias de más de 1 día */
 $fli_unloadwarehousecode --AND fli.unloadwarehousecode = 'MTY' --FILTRO PRINCIPAL, OBLIGATORIO
 $fg_garantia_usar
 $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia = ''
 
 
 GROUP BY fli.warehousecode,fg.garantia_usar
 
 UNION ALL
 
 SELECT
 'En Transito' AS origen_anterior,
 ffg.garantia_usar,
 count(*) AS cantguias
 FROM guias.factguias ffg
 INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(ffg.vwopenvio_codigopos, ffg.vwdestino_codigopostal)
 WHERE ffg.garantia_usar IN ('D', '7', 'L') AND
 " . $conditional . "
 $fmp_siglasplaza --AND fmp.siglasplaza = 'GDL' --FILTRO PRINCIPAL, OBLIGATORIO
 $ffg_garantia_usar
 $ffg_vwguias_numeroguia
 AND ffg.vwguias_numeroguia NOT IN
 (
 SELECT fg.vwguias_numeroguia
 FROM guias.factguias fg
 INNER JOIN guias.factitem fi ON fi.waybill = fg.vwguias_numeroguia
 INNER JOIN tr1.factloaditem fli ON fli.itemid = fi.id
 WHERE
 " . $conditional . "
 AND fg.garantia_usar IN ('D', '7', 'L') /* Garantias de más de 1 día */
 $fli_unloadwarehousecode --AND fli.unloadwarehousecode = 'MTY' --FILTRO PRINCIPAL, OBLIGATORIO
 $fg_garantia_usar
 $fg_vwguias_numeroguia
 )
 GROUP BY 2
 UNION ALL
 SELECT
 fg.vwopenvio_siglasori AS origen_anterior,
 fg.garantia_usar,
 count(*) AS cantguias
 FROM guias.factguias fg
 INNER JOIN tr2.dimfrecuenciasmp fmp
 ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal)
 WHERE
 fg.garantia_usar NOT IN ('D', '7', 'L') /* Garantias de día siguiente */
 AND " . $conditional . "
 $fmp_siglasplaza --AND fmp.siglasplaza = 'GDL' --FILTRO PRINCIPAL, OBLIGATORIO
 $fg_garantia_usar --AND fg.garantia_usar IN ( '' )
 $fg_vwguias_numeroguia
 GROUP BY 1, 2
 
 ";
        //die($query);
        return DB::select(
                        $query
        );
    }

    public static function getDetalleGuiasOrigenAnterior($plazas_filtro = [], $fecha_filtro = '2020-03-04', $cliente_filtro = "", $guia_filtro = "", $garantias_filtro = [], $plaza_filtro_param = 'GDL', $dia_seleccionado = 0) {
        //$plazas_filtro = self::findSectores($plazas_filtro,true);
        $plzf = $plazas_filtro;

        $alt_plazas_filtro = $plazas_filtro;



        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));

        $alt_garantias_filtro = $garantias_filtro;


        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = self::findSectores($plzf); //"'" . implode ( "', '", $plazas_filtro ) . "'";

            $plazas_filtro = " ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "";
        }

        $fmp_siglasplaza = "";
        if (count($alt_plazas_filtro) > 0) {
            $fmp_siglasplaza = self::findSectores($plzf); //"'" . implode ( "', '", $alt_plazas_filtro ) . "'";
            $fmp_siglasplaza = "AND fmp.siglasplaza IN  ( " . $fmp_siglasplaza . " ) ";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = " ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }


        $fg_garantia_usar = "";
        if ($garantias_filtro != "") {
            //AND fg.garantia_usar IN ( '' )
            $fg_garantia_usar = "AND fg.garantia_usar IN " . $garantias_filtro;
        }


        if ($cliente_filtro !== null && $cliente_filtro !== 0) {
            $cliente_filtro = " '" . $cliente_filtro . "' ";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== 0 && $guia_filtro !== '') {
            $guia_filtro = " '" . $guia_filtro . "' ";
        } else {
            $guia_filtro = "";
        }

        //Inicializar la variable en vacío para evitar missing
        $fg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $fg_vwopenvio_siglasori = "AND fg.vwopenvio_siglasori IN " . $plazas_filtro;
        }
        $fg_siglasdes_usar = "";
        if ($plazas_filtro != "") {
            $fg_siglasdes_usar = "AND fg.siglasdes_usar IN " . $plazas_filtro;
        }
        $fg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $fg_garantia_usar = " AND fg.garantia_usar IN " . $garantias_filtro;
        }

        $fg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $fg_vwenvios_numerocliente = " AND (  fg.vwenvios_numerocliente = " . $cliente_filtro . "  OR fg.vwopguia_numcte = " . $cliente_filtro . " )";
        }

        $gfg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $gfg_vwenvios_numerocliente = " AND (  gfg.vwenvios_numerocliente = " . $cliente_filtro . "  OR gfg.vwopguia_numcte = " . $cliente_filtro . " )";
        }
        $fg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $fg_vwguias_numeroguia = " AND fg.vwguias_numeroguia = " . $guia_filtro;
        }

        $ffg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $ffg_vwopenvio_siglasori = " AND ffg.vwopenvio_siglasori IN " . $plazas_filtro;
        }

        $fli_unloadwarehousecode = "";
        if ($plazas_filtro != "") {
            $fli_unloadwarehousecode = " AND fli.unloadwarehousecode IN " . $plazas_filtro;
        }

        $fag_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $fag_garantia_usar = " AND fag.garantia_usar IN " . $garantias_filtro;
        }

        $ffgg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            $ffgg_vwenvios_numerocliente = " AND ffgg.vwenvios_numerocliente  = " . $cliente_filtro . "  OR ffgg.vwopguia_numcte = " . $cliente_filtro;
        }

        $fag_vwguias_numeroguia = "";
        if ($guia_filtro !== "") {
            $fag_vwguias_numeroguia = " AND fag.vwguias_numeroguia = " . $guia_filtro;
        }

        $ffg_vwopenvio_siglasdes = "";
        if ($plazas_filtro != "") {
            $ffg_vwopenvio_siglasdes = " AND ffg.vwopenvio_siglasdes IN " . $plazas_filtro;
        }



        $ffg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $ffg_garantia_usar = " AND ffg.garantia_usar IN  " . $garantias_filtro;
        }

        $ffg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            //AND ffg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  ffg.vwopguia_numcte = 'FILTRO CLIENTE'
            $ffg_vwenvios_numerocliente = " AND ffg.vwenvios_numerocliente = " . $cliente_filtro . "  OR  ffg.vwopguia_numcte = " . $cliente_filtro;
        }

        $ffg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $ffg_vwguias_numeroguia = " AND ffg.vwguias_numeroguia = " . $guia_filtro;
        }
        $tfli_unloadwarehousecode = "";
        if ($plazas_filtro != "") {
            $tfli_unloadwarehousecode = " AND tfli.unloadwarehousecode IN " . $plazas_filtro;
        }
        $fli_warehousecode = "";
        if ($plazas_filtro != "") {
            $fli_warehousecode = " AND fli.warehousecode IN " . $plazas_filtro;
        }
        $tfli_warehousecode = "";
        if ($plazas_filtro != "") {
            $tfli_warehousecode = " AND tfli.warehousecode IN " . $plazas_filtro;
        }

        $fag_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $fag_vwenvios_numerocliente = " AND fag.vwenvios_numerocliente = " . $cliente_filtro . " OR  fag.vwopguia_numcte = " . $cliente_filtro;
        }

        $gfg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $gfg_vwguias_numeroguia = " AND gfg.vwguias_numeroguia = " . $guia_filtro;
        }


        $alt_plazas_filtro = $plazas_filtro;

        /* $garantia_usar="";
          if( count($alt_garantias_filtro)>0){

          //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
          $garantia_usar = "'" . implode ( "', '", $alt_garantias_filtro ) . "'";

          $garantia_usar = "\n AND garantia_usar IN ( ".$garantia_usar." ) \n";
          }else{
          $garantia_usar="";
          } */


        $fg_vwopenvio_siglasori_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fg_vwopenvio_siglasori_param = " AND fg.vwopenvio_siglasori = '" . $plaza_filtro_param . "'";
        }

        $fli_warehousecode_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fli_warehousecode_param = " AND fli.warehousecode = '" . $plaza_filtro_param . "'";
        }

        $fmp_siglasplaza_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fmp_siglasplaza_param = " AND fmp.siglasplaza = '" . $plaza_filtro_param . "'";
        }

        $fli_unloadwarehousecode_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fli_unloadwarehousecode_param = " AND fli.unloadwarehousecode =  '" . $plaza_filtro_param . "'";
        }

        $fg_vwopenvio_siglasdes = "";
        if ($plazas_filtro != "") {
            $fg_vwopenvio_siglasdes = " AND fg.vwopenvio_siglasdes IN " . $plazas_filtro;
        }
        //die($fg_vwopenvio_siglasori_param);





        $sql = "
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
  fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
  fg.vwinfoenvio_contenido,
  CASE
  --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  fg.garantia_usar
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  WHEN  'L'::text THEN 'LTL'
  ELSE
    --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
    fg.garantia_usar
  END  garantia,
  COALESCE(fg.vwopguia_numcte, fg.vwenvios_numerocliente) as numero_cliente,
  fg.vwdestino_razonsocial,
  COALESCE(dl.district,'') as distrito,
  COALESCE(dl.shortregional,'') as regional,
  fli.unloadwarehousecode as plaza,
  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion,
  fg.fechacompromiso
FROM guias.factguias fg
  LEFT JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN guias.factitem fi ON fi.waybill = fg.vwguias_numeroguia
  INNER JOIN tr1.factloaditem fli ON fli.itemid = fi.id
WHERE
  fg.fechacompromiso :: DATE = '$fecha_filtro' :: DATE + INTERVAL '$dia_seleccionado day'
  AND fg.garantia_usar IN ('D', '7', 'L') /* Garantias de más de 1 día */
  $fli_unloadwarehousecode --AND fli.unloadwarehousecode = 'MTY' --PLAZA DESTINO, ES DONDE ESTOY, FILTRO PRINCIPAL
  $fli_warehousecode_param --AND fli.warehousecode = 'MEX' -- PLAZA ORIGEN, PARAMETRO PLAZA SELECCIONADA
  $fg_garantia_usar
  $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia ='302556507861A700759105'
  $fg_vwenvios_numerocliente
UNION DISTINCT

SELECT
  DISTINCT
  fg.vwguias_numeroguia,
  fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
  fg.vwinfoenvio_contenido,
  CASE
  --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  fg.garantia_usar
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  WHEN  'L'::text THEN 'LTL'
  ELSE
    --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
    fg.garantia_usar
  END  garantia,
  COALESCE(fg.vwopguia_numcte, fg.vwenvios_numerocliente) as numero_cliente,
  fg.vwdestino_razonsocial,
  COALESCE(dl.district,'') as distrito,
  COALESCE(dl.shortregional,'') as regional,
  fmp.siglasplaza as plaza,
  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion,
  fg.fechacompromiso
FROM guias.factguias fg
  LEFT JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal) --Nuevo INNER JOIN
WHERE
  fg.garantia_usar NOT IN ('D', '7', 'L') /* Garantias de día siguiente */
  AND fg.fechacompromiso :: DATE = '$fecha_filtro' :: DATE + INTERVAL '$dia_seleccionado day'
  $fmp_siglasplaza --AND fmp.siglasplaza = 'MTY' --PLAZA DESTINO, ES DONDE ESTOY, FILTRO PRINCIPAL
  $fg_vwopenvio_siglasori_param --AND fg.vwopenvio_siglasori = 'GDL' -- PLAZA ORIGEN, PARAMETRO PLAZA SELECCIONADA
  $fg_garantia_usar
  $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia ='302556507861A700759105'
  $fg_vwenvios_numerocliente

ORDER BY 1, 2
";

        if ($plaza_filtro_param === 'En Transito') {
            //Ejecuar el de enmedio
            $sql = "
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
  fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
  fg.vwinfoenvio_contenido,
  CASE
  --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  fg.garantia_usar
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  WHEN  'L'::text THEN 'LTL'
  ELSE
    --SUBSTRING(fg.vwguias_numeroguia,14,1)::text
    fg.garantia_usar
  END  garantia,
  COALESCE(fg.vwopguia_numcte, fg.vwenvios_numerocliente) as numero_cliente,
  fg.vwdestino_razonsocial,
  COALESCE(dl.district,'') as distrito,
  COALESCE(dl.shortregional,'') as regional,
  fmp.siglasplaza as plaza,
  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion,
  fg.fechacompromiso
FROM guias.factguias fg
  LEFT JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal) --Nuevo INNER JOIN
WHERE fg.garantia_usar IN ('D', '7', 'L')
      AND fg.fechacompromiso :: DATE = '$fecha_filtro' :: DATE + INTERVAL '$dia_seleccionado day'
      $fmp_siglasplaza --AND fmp.siglasplaza = 'GDL'  -- -- PLAZA DESTINO (FILTRO PRINCIPAL)
      $fg_garantia_usar
      $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia ='302556507861A700759105'
      $fg_vwenvios_numerocliente
      AND fg.vwguias_numeroguia NOT IN
          (
            SELECT fg.vwguias_numeroguia
            FROM guias.factguias fg
              INNER JOIN guias.factitem fi ON fi.waybill = fg.vwguias_numeroguia
              INNER JOIN tr1.factloaditem fli ON fli.itemid = fi.id
            WHERE
              fg.fechacompromiso :: DATE = '$fecha_filtro' :: DATE + INTERVAL '$dia_seleccionado day'
              AND fg.garantia_usar IN ('D', '7', 'L') /* Garantias de más de 1 día */
              $fli_unloadwarehousecode --AND fli.unloadwarehousecode = 'GDL'  -- PLAZA DESTINO (FILTRO PRINCIPAL)
              $fg_garantia_usar
              $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia ='302556507861A700759105'
              $fg_vwenvios_numerocliente
          )

ORDER BY 1, 2";
        }
        //die($sql);

        return DB::select($sql);
    }

    public static function getDestinoSiguiente($fecha_filtro = '2020-03-04', $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro, $dia_seleccionado) {

        /* var_dump($plazas_filtro);
          die(); */
        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));



        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";

            $plazas_filtro = " ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = " ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }
        if ($cliente_filtro !== null && $cliente_filtro !== '0') {
            $cliente_filtro = " '" . $cliente_filtro . "' ";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '') {
            $guia_filtro = " '" . $guia_filtro . "' ";
        } else {
            $guia_filtro = "";
        }

        //Inicializar la variable en vacío para evitar missing
        $fg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $fg_vwopenvio_siglasori = "AND fg.vwopenvio_siglasori IN " . $plazas_filtro;
        }
        $fg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $fg_garantia_usar = " AND fg.garantia_usar IN " . $garantias_filtro;
        }

        $fg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $fg_vwenvios_numerocliente = " AND (  fg.vwenvios_numerocliente = " . $cliente_filtro . "  OR fg.vwopguia_numcte = " . $cliente_filtro;
        }
        $fg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $fg_vwguias_numeroguia = " fg.vwguias_numeroguia " . $guia_filtro;
        }
        $fli_warehousecode = "";
        if ($plazas_filtro != "") {
            $fli_warehousecode = " AND fli.warehousecode IN " . $plazas_filtro;
        }

        $ffgg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $ffgg_garantia_usar = " AND ffgg.garantia_usar IN " . $garantias_filtro;
        }

        $ffgg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            $ffgg_vwenvios_numerocliente = " AND ffgg.vwenvios_numerocliente  = " . $cliente_filtro . "  OR ffgg.vwopguia_numcte = " . $cliente_filtro;
        }

        $ffgg_vwguias_numeroguia = "";
        if ($guia_filtro !== "") {
            $ffgg_vwguias_numeroguia = " AND ffgg.vwguias_numeroguia = " . $guia_filtro;
        }

        $ffg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $ffg_vwopenvio_siglasori = " AND ffg.vwopenvio_siglasori IN " . $plazas_filtro;
        }

        $ffg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $ffg_garantia_usar = " AND ffg.garantia_usar IN  " . $garantias_filtro;
        }

        $ffg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            //AND ffg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  ffg.vwopguia_numcte = 'FILTRO CLIENTE'
            $ffg_vwenvios_numerocliente = " AND ffg.vwenvios_numerocliente = " . $cliente_filtro . "  OR  ffg.vwopguia_numcte = " . $cliente_filtro;
        }

        $ffg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $ffg_vwguias_numeroguia = " AND ffg.vwguias_numeroguia = " . $guia_filtro;
        }

        $tfli_warehousecode = "";
        if ($plazas_filtro) {
            $tfli_warehousecode = " AND tfli.warehousecode IN " . $plazas_filtro;
        }

        $gfg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $gfg_vwenvios_numerocliente = " AND gfg.vwenvios_numerocliente = " . $cliente_filtro . " OR  gfg.vwopguia_numcte = " . $cliente_filtro;
        }

        $gfg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $gfg_vwguias_numeroguia = " AND gfg.vwguias_numeroguia = " . $guia_filtro;
        }

        $query = "
           
SELECT
  fli.unloadwarehousecode as destino_siguiente,
  ffgg.garantia_usar,
  count(*) as cantguias
FROM guias.factguias ffgg
  INNER JOIN guias.factitem fi on fi.waybill = ffgg.vwguias_numeroguia
  INNER JOIN tr1.factloaditem fli on fli.itemid = fi.id
WHERE
      ffgg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
      AND ffgg.garantia_usar IN ('D','7','L') /* Garantias de más de 1 día */
      $fli_warehousecode -- AND fli.warehousecode IN ('PLAZAS FILTRO', 'MTY')
      $ffgg_garantia_usar -- AND ffgg.garantia_usar IN ('Garantiais filtro','D') -- ESTO LIMITA A 
      $ffgg_vwenvios_numerocliente -- AND ffgg.vwenvios_numerocliente  = 'CLIENTE FILTO' OR ffgg.vwopguia_numcte = 'CLIENTE FILTO'
      $ffgg_vwguias_numeroguia --AND ffgg.vwguias_numeroguia = 'guia FINTRO'
GROUP BY 1,ffgg.vwopenvio_siglasori, 2
UNION ALL

/* El resto de guias (temporalmente) */
SELECT
  'En Transito' as destino_siguiente,
  ffg.garantia_usar,
  count(*) as cantguias
FROM guias.factguias ffg
WHERE ffg.garantia_usar IN ( 'D', '7', 'L')
      AND ffg.fechacompromiso::date = '$fecha_filtro'::DATE + INTERVAL '$dia_seleccionado day'
      $ffg_vwopenvio_siglasori -- AND ffg.vwopenvio_siglasori IN ('FILTRO PLAZAS') --ESTO LIMITA A
      $ffg_garantia_usar -- AND ffg.garantia_usar IN ('GARANTIA FILTROS')
      $ffg_vwenvios_numerocliente -- AND ffg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  ffg.vwopguia_numcte = 'FILTRO CLIENTE'
      $ffg_vwguias_numeroguia -- AND ffg.vwguias_numeroguia = 'FILTRO GUIA'
      AND ffg.vwguias_numeroguia NOT IN (
  SELECT gfg.vwguias_numeroguia
  FROM guias.factguias gfg
    INNER JOIN guias.factitem gfi on gfi.waybill = gfg.vwguias_numeroguia
    INNER JOIN tr1.factloaditem tfli on tfli.itemid = gfi.id
  WHERE
        gfg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
        AND gfg.garantia_usar IN ('D','7', 'L')
        $tfli_warehousecode -- AND tfli.warehousecode IN ('pnaza filtro')
        $gfg_vwenvios_numerocliente -- AND gfg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  gfg.vwopguia_numcte = 'FILTRO CLIENTE'
        $gfg_vwguias_numeroguia -- AND gfg.vwguias_numeroguia = 'FILTRO GUIA'

)
--_____________________________________________________________________________
GROUP BY 2
UNION ALL
SELECT
  fmp.siglasplaza as destino_siguiente,--cambio
  fg.garantia_usar,
  count(*) as cantguias
FROM guias.factguias fg
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal)--Nuevo INNER JOIN
WHERE
  fg.garantia_usar not in ('D','7','L')/* Garantias de día siguiente */
  AND fg.fechacompromiso::DATE = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
$fg_vwopenvio_siglasori --AND fg.vwopenvio_siglasori IN  ('PLAZAS FILTRO', 'MTY')
$fg_garantia_usar --AND fg.garantia_usar IN  ('gGARANTIAS FILTRO',)
$fg_vwenvios_numerocliente --AND (  fg.vwenvios_numerocliente = 'cliente filtro' OR fg.vwopguia_numcte = 'cliente filtro' )
$fg_vwguias_numeroguia --AND fg.vwguias_numeroguia = 'guia filtro'
GROUP BY 1, 2--;
ORDER BY 1,2";

        //die($query);
        return DB::select(
                        $query
        );
    }

    public static function getInfoEntregasByCodigoPostal($plazas_filtro = [], $fecha_filtro = 0, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0, $fecha_entrega, $codigo_postalNull) {
        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));
        $codigosMP = '0';
        $codigosLTL = '0';
        $list_plazas = null;
//        if( count($plazas_filtro)>0) {
//            $plazas_filtro = self::findSectores($plazas_filtro);//"'" . implode ( "', '", $plazas_filtro ) . "'";
//            $plazas_filtro = "( ".$plazas_filtro." ) ";
//            // $plaza_filtro = "MTY";
//            // $codigosMP = DB::select("select codigopostal from " . self::$TABLE_NAME_FREC_MP . " where siglasplaza =    '" . $plaza_filtro . "' ");
//            // $codigosLTL = DB::select("select codigopostal from " . self::$TABLE_NAME_FREC_LTL . " where  siglasplaza =  '" . $plaza_filtro . "' ");
//            //die(json_encode($codigosMP));
//            // die("select codigopostal from " . self::$TABLE_NAME_FREC_MP . " where siglasplaza =    '" . $plaza_filtro . "' ");
//            //$list_plazas = " vwopenvio_codigopos in (select codigopostal from tr2.dimFrecuenciasMp where siglasplaza IN (".$plazas_filtro.") ) AND ";
//            $list_plazas = " vwopenvio_siglasdes IN (".$plazas_filtro.") AND";
//        }else{
//            $list_plazas = "";
//        }
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            //$plazas_filtro = $this->getPlazasAgrupadas($plazas_filtro);
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $list_plazas = " AND vwopenvio_siglasdes IN ( " . $plazas_filtro . " )";
            //$list_plazas = " vwopenvio_siglasdes IN ('MEX','MX1','MX3') AND";
        } else {
            $list_plazas = "";
        }

        if (count($garantias_filtro) > 0) {
            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = "\n AND garantia_usar IN ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }
        if ($cliente_filtro !== null && $cliente_filtro !== '0' && $cliente_filtro != 0) {
            $cliente_filtro = "\n AND vwenvios_numerocliente  = '" . $cliente_filtro . "'\n";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '' && $guia_filtro != 0) {
            $guia_filtro = "\n AND vwguias_numeroguia = '" . $guia_filtro . "'\n";
        } else {
            $guia_filtro = "";
        }
        
        if ($fecha_entrega == "false") {
            $fecha_entrega = "\n vwopenvio_fechaentrega IS NULL AND \n";
        } else {
            $fecha_entrega = "";
        }

        if ($codigo_postalNull == "false") {
            $qrfiltroCP = "\n vwopenvio_codigopos IS NOT NULL AND \n";
        } else {
            $qrfiltroCP = "";
        }
//        $operator = '+';
//        if($dia_filtro == '-30'){
//            $operator = '-';
//            $dia_filtro = 30;
//        }
        $sql = "";
        if ($dia_filtro == '-30') {
            $sql = "
            select
                vwopenvio_codigopos as codigopostal,
                count(*) as cantguias,
                garantia_usar
            from guias.factGuias fg
            WHERE
                $fecha_entrega
                fechacompromiso IS NOT NULL AND
                $qrfiltroCP
                fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1
                $list_plazas
                $garantias_filtro
                $cliente_filtro
                $guia_filtro
                AND substring(vwguias_numeroguia,14,1) IN (
                select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                order by 1
                                                    )
                AND  NOT exists (
		   SELECT DISTINCT fh.idguia FROM guias.facthistoria fh 
		   WHERE fh.idguia = vwguias_numeroguia AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
            AND fg.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = fg.VWGUIAS_NUMEROGUIA)
            AND vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
            GROUP by garantia_usar, vwopenvio_codigopos
            order by 1, 2";
        } else {
            $sql = "
            select
                vwopenvio_codigopos as codigopostal,
                count(*) as cantguias,
                garantia_usar
            from guias.factGuias fg
            WHERE
                $fecha_entrega
                fechacompromiso IS NOT NULL AND
                $qrfiltroCP
                fechacompromiso::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' 
                $list_plazas
                $garantias_filtro
                $cliente_filtro
                $guia_filtro
                AND substring(vwguias_numeroguia,14,1) IN (
                select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                order by 1
                                                    )
                AND fg.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = fg.VWGUIAS_NUMEROGUIA)
            AND vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
            GROUP by garantia_usar, vwopenvio_codigopos
            order by 1, 2";
        }
        return DB::select(//WHERE table_schema = 'public'
                        $sql
        );
    }

    public static function getInfoRecoleccionesByCodigoPostal($plazas_filtro = [], $fecha_filtro = null, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0) {

        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));
        //die($fecha_filtro);
        $codigosMP = '0';
        $codigosLTL = '0';

        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";

            $plazas_filtro = " pvi.vwpickupvisit_warehousecode IN ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = " TRUE ";
        }


        if (count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = "\n AND garantia_usar IN ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }

        $sql = "
            
            select 
                 vwpickupvisit_zipcode as codigopostal,
                 --SUBSTRING(vwguias_numeroguia,13,1) as 
                 garantia_usar ,
                 count(*)  as cantguias
            from tr2.factPickupVisitItem  as pvi
                 
            inner join guias.factItem as fi on fi.id = pvi.vwpickupvisititem_itemid
            inner join guias.factGuias as fg on fg.vwguias_numeroguia = fi.waybill
            inner join  tr2.dimRutaTR2CP as tr2cp   on tr2cp.codigopostal = pvi.vwpickupvisit_zipcode	
                    
                 WHERE
                      -- PROBAR
                      $plazas_filtro -- pvi.vwpickupvisit_warehousecode =
                      
                    AND pvi.vwpickupvisit_planneddate::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' 
                  
                  --AND garantia_usar = CASE  ''::text WHEN '0' THEN garantia_usar ELSE ''::text END
                    $garantias_filtro

    AND
       ( vwenvios_numerocliente = CASE  '" . $cliente_filtro . "' WHEN '0' THEN vwenvios_numerocliente ELSE '" . $cliente_filtro . "' END
             OR vwopguia_numcte =CASE '" . $cliente_filtro . "' WHEN '0' THEN vwopguia_numcte ELSE '" . $cliente_filtro . "' END  )



      AND vwguias_numeroguia = CASE '" . $guia_filtro . "'::TEXT WHEN '0' THEN vwguias_numeroguia ELSE '" . $guia_filtro . "'::TEXT END

                        
                           
                 GROUP by 
                      fg.garantia_usar,
                      vwpickupvisit_zipcode
                      --count(*) 
                  order by 1 ,2
                 limit 70
                   ";

        //die(($sql));
        //return $sql;
        return DB::select(//WHERE table_schema = 'public'
                        $sql
        );
    }

    public static function getInfoEntregasByTr2($plazas_filtro = [], $fecha_filtro = null, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0, $fecha_entrega, $codigo_postalNull) {

//Start: Date format
        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));
//End: Date format
//Start: Get Plazas
        $list_plazas = null;
        if (count($plazas_filtro) > 0) {
            $plazas_filtro = self::findSectores($plazas_filtro);
            $plazas_filtro = "( " . $plazas_filtro . " ) ";
            $list_plazas = "fg.vwopenvio_siglasdes IN " . $plazas_filtro . " AND";
        } else {
            $list_plazas = "";
        }
//End: Get Plazas
//Start: Get Garantias
        if (count($garantias_filtro) > 0) {
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";
            $garantias_filtro = "garantia_usar IN (" . $garantias_filtro . ") AND";
        } else {
            $garantias_filtro = "";
        }
//End: Get Garantias

        if ($cliente_filtro !== null && $cliente_filtro !== '0' && $cliente_filtro != 0) {
            $cliente_filtro = "\n vwenvios_numerocliente  = '" . $cliente_filtro . "' AND\n";
        } else {
            $cliente_filtro = "";
        }

        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '' && $guia_filtro != 0) {
            $guia_filtro = "\n vwguias_numeroguia = '" . $guia_filtro . "' AND\n";
        } else {
            $guia_filtro = "";
        }

        if ($fecha_entrega == "false") {
            $fecha_entrega = "\n vwopenvio_fechaentrega IS NULL AND \n";
        } else {
            $fecha_entrega = "";
        }

        if ($codigo_postalNull == "false") {
            $qrfiltroCP = "\n vwopenvio_codigopos IS NOT NULL AND \n";
        } else {
            $qrfiltroCP = "";
        }

//Start: condition -30
        $conditional = "fg.fechacompromiso::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' AND";
        $sentence = "";

        if ($dia_filtro == '-30') {
            $conditional = 'fg.fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1 AND';
            $sentence = "
                AND  NOT exists (
                SELECT DISTINCT fh.idguia FROM guias.facthistoria fh
                WHERE fh.idguia = fg.vwguias_numeroguia
                AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
            ";
        }
//End: condition -30

        $sql = "
    SELECT
        COUNT(*) AS cantguias,
        fg.garantia_usar, 
        fg.vwopenvio_siglasdes,
        fg.vwopenvio_codigopos,
        fg.fechacompromiso,
        CASE 
            WHEN fg.vwopenvio_tipoenvio = 1 THEN tr2cp.sob_ruta
            WHEN fg.vwopenvio_tipoenvio = 4 or fg.vwopenvio_tipoenvio is NULL  THEN tr2cp.paq_ruta
        END as rutaTR2
    FROM
        guias.factGuias  as fg
                    inner join tr2.dimRutaTR2CP as tr2cp
                    on tr2cp.codigopostal = fg.vwopenvio_codigopos
    WHERE
        $fecha_entrega
        fg.fechacompromiso IS NOT NULL AND
        $qrfiltroCP
        $conditional
        $list_plazas
        $garantias_filtro
        $cliente_filtro
        $guia_filtro
        substring(vwguias_numeroguia,14,1) IN (
            select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                    from  guias.dimGarantias
                                    where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                                    order by 1
                                        )
        $sentence    
        AND fg.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                                    FROM guias.factGuias  fg2
                                    WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                                    AND fg2.VWGUIAS_NUMEROGUIA = fg.VWGUIAS_NUMEROGUIA)
        AND fg.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
        GROUP by garantia_usar, vwopenvio_codigopos, vwopenvio_siglasdes, 
        fechacompromiso, vwopenvio_tipoenvio, sob_ruta, vwopenvio_tipoenvio,
        vwopenvio_tipoenvio, paq_ruta
";

        return DB::select(
                        $sql
        );
    }

    public static function getInfoRecoleccionesByTr2($plazas_filtro = [], $fecha_filtro = null, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0) {
        //when date is null
        /* if (is_null($fecha_filtro)) {
          $now = new \DateTime($fecha_filtro);
          if($dia_filtro !=0) {

          $now->add(new \DateInterval('P' . $dia_filtro . 'D'));
          }

          $fecha_filtro = $now->format('Y-m-d');
          //$fecha_filtro = $now->modify('+'.$dia_filtro.' day');
          } */

        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));
        //die($fecha_filtro);
        $codigosMP = '0';
        $codigosLTL = '0';
        if (true) {
            // $codigosMP = DB::select("select codigopostal from " . self::$TABLE_NAME_FREC_MP . " where siglasplaza =    '" . $plaza_filtro . "' ");
            //$codigosLTL = DB::select("select codigopostal from " . self::$TABLE_NAME_FREC_LTL . " where  siglasplaza =  '" . $plaza_filtro . "' ");
            // return $codigosMP;
        }


        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";

            $plazas_filtro = " pvi.vwpickupvisit_warehousecode IN ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = " TRUE ";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = "\n AND fg.garantia_usar IN ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }
//Start: condition -30
        $conditional = "fechacompromiso::date = '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day'";

        if ($dia_filtro == '-30') {
            $conditional = 'fechacompromiso between CURRENT_DATE -30 AND CURRENT_DATE';
        }
        //End: condition -30

        $sql = "
            
            select 
                case when COALESCE (max(fg.vwopenvio_tipoenvio), max(fg.vwinfoenvio_tipoenvio)) = 1 then max(tr2cp.sob_ruta) 
            else max(tr2cp.paq_ruta) end as rutaTR2,
                 --SUBSTRING(vwguias_numeroguia,13,1) as 
               max(fg.garantia_usar) as garantia_usar ,
                 count(*)  as cantguias
            from tr2.factPickupVisitItem  as pvi
                 
            inner join guias.factItem as fi on fi.id = pvi.vwpickupvisititem_itemid
            inner join guias.factGuias as fg on fg.vwguias_numeroguia = fi.waybill
            inner join  tr2.dimRutaTR2CP as tr2cp   on tr2cp.codigopostal = pvi.vwpickupvisit_zipcode	
                    
                WHERE
                
                        $plazas_filtro
                    AND
                    
                     pvi.vwpickupvisit_planneddate::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' 
                  
                  $garantias_filtro


    AND
       ( vwenvios_numerocliente = CASE  '" . $cliente_filtro . "' WHEN '0' THEN vwenvios_numerocliente ELSE '" . $cliente_filtro . "' END
             OR vwopguia_numcte =CASE '" . $cliente_filtro . "' WHEN '0' THEN vwopguia_numcte ELSE '" . $cliente_filtro . "' END  )



      AND vwguias_numeroguia = CASE '" . $guia_filtro . "'::TEXT WHEN '0' THEN vwguias_numeroguia ELSE '" . $guia_filtro . "'::TEXT END

                   
                  order by 1 ,2
                   limit 40
                   ";

        //die($sql);
        return DB::select($sql);

        // return $sql;
    }

    public static function getDetalleGuias($plazas_filtro = [], $fecha_filtro = null, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0, $codigopostal, $fecha_entrega, $codigo_postalNull) {
        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));
        $alt_plazas_filtro = $plazas_filtro;
        $list_plazas = null;

        if ($codigo_postalNull == "false") {
            $qrfiltroCP = "\n vwopenvio_codigopos IS NOT NULL AND \n";
        } else {
            $qrfiltroCP = "";
        }
        if (count($garantias_filtro) > 0) {
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";
            $garantias_filtro = "\n AND garantia_usar IN ( " . $garantias_filtro . " ) \n";
        } else {
            $garantias_filtro = "";
        }
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $list_plazas = "factGuias.vwopenvio_siglasdes IN ( " . $plazas_filtro . " )    AND ";
        } else {
            $list_plazas = "";
        }
//        if (count($alt_plazas_filtro) > 0) {
//            $alt_plazas_filtro = "'" . implode("', '", $alt_plazas_filtro) . "'";
//            $alt_plazas_filtro = " pvi.vwpickupvisit_warehousecode IN ( " . $alt_plazas_filtro . " ) ";
//        } else {
//            $alt_plazas_filtro = " TRUE ";
//        }
        if ($cliente_filtro !== null && $cliente_filtro !== '0' && $cliente_filtro != 0) {
            $cliente_filtro = "\n AND factGuias.vwenvios_numerocliente  = '" . $cliente_filtro . "'\n";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '' && $guia_filtro != 0) {
            $guia_filtro = "\n AND factGuias.vwguias_numeroguia = '" . $guia_filtro . "'\n";
        } else {
            $guia_filtro = "";
        }

        if ($fecha_entrega == "false") {
            $fecha_entrega = "\n vwopenvio_fechaentrega IS NULL AND \n";
        } else {
            $fecha_entrega = "";
        }
        $sql = "";
        if ($dia_filtro == '-30') {
            $sql = "
  select
    factGuias.vwguias_numeroguia,
    factGuias.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
    factGuias.vwinfoenvio_contenido,
    CASE factGuias.garantia_usar
    WHEN  '5'::text THEN '11:30'
    WHEN  'D'::text THEN '2 Días'
    WHEN  'A'::text THEN '9:30'
    WHEN  '3'::text THEN '9:30'
    WHEN  '6'::text THEN 'Día siguiente'
    WHEN  'T'::text THEN 'Día siguiente Terrestre'
    WHEN  '7'::text THEN 'Terrestre'
    WHEN  'F'::text THEN 'Paquetería Aerea'
    WHEN  '9'::text THEN 'Apoyo a Operaciones'
    WHEN  'L'::text THEN 'LTL'
    ELSE
      factGuias.garantia_usar
    END  garantia,
    COALESCE(factGuias.vwopguia_numcte,
           factGuias.vwenvios_numerocliente) as numero_cliente,
    factGuias.vwdestino_razonsocial,
    coalesce(dimLocation.district,'') as distrito,
    coalesce(dimLocation.shortregional,'') as regional,
    factGuias.vwopenvio_siglasdes as plaza,
    factGuias.vwopenvio_codigopos as codigo_postal,
    (trim(' ' from vwdestino_direccion1) || ' ' || trim(' ' from vwdestino_direccion2) || ' ' || trim(' ' from vwdestino_colonia) || ' ' ||
    trim(' ' from vwdestino_municipio) || ' ' ||trim(' ' from vwdestino_ciudad) ||' ' || trim(' ' from vwdestino_estado)) as direccion,
    factGuias.fechacompromiso as fecha_compromiso,
    factGuias.vwopenvio_fechaentrega as fecha_entrega
from     guias.factGuias as factGuias
  inner join tr2.dimFrecuenciasMP fmp on codigopostal= factGuias.vwopenvio_codigopos
  left join tr1.dimLocations dimLocation on dimLocation.code = fmp.siglasplaza
WHERE
    $fecha_entrega
    fechacompromiso IS NOT NULL AND
    $qrfiltroCP
    $list_plazas 
    fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1
    $garantias_filtro
    $cliente_filtro
    $guia_filtro
    and vwopenvio_codigopos = '" . $codigopostal . "'
    AND substring(vwguias_numeroguia,14,1) IN (
        select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
        order by 1
                                                    )
    AND  NOT exists (
		   SELECT DISTINCT fh.idguia FROM guias.facthistoria fh 
		   WHERE fh.idguia = vwguias_numeroguia AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
    AND factGuias.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = factGuias.VWGUIAS_NUMEROGUIA)
    AND factGuias.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
    ORDER BY factGuias.garantia_usar
    ";
        } else {
            $sql = "
  select
    factGuias.vwguias_numeroguia,
    factGuias.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
    factGuias.vwinfoenvio_contenido,
    CASE factGuias.garantia_usar
    WHEN  '5'::text THEN '11:30'
    WHEN  'D'::text THEN '2 Días'
    WHEN  'A'::text THEN '9:30'
    WHEN  '3'::text THEN '9:30'
    WHEN  '6'::text THEN 'Día siguiente'
    WHEN  'T'::text THEN 'Día siguiente Terrestre'
    WHEN  '7'::text THEN 'Terrestre'
    WHEN  'F'::text THEN 'Paquetería Aerea'
    WHEN  '9'::text THEN 'Apoyo a Operaciones'
    WHEN  'L'::text THEN 'LTL'
    ELSE
      factGuias.garantia_usar
    END  garantia,
    COALESCE(factGuias.vwopguia_numcte,
           factGuias.vwenvios_numerocliente) as numero_cliente,
    factGuias.vwdestino_razonsocial,
    coalesce(dimLocation.district,'') as distrito,
    coalesce(dimLocation.shortregional,'') as regional,
    factGuias.vwopenvio_siglasdes as plaza,
    factGuias.vwopenvio_codigopos as codigo_postal,
    (trim(' ' from vwdestino_direccion1) || ' ' || trim(' ' from vwdestino_direccion2) || ' ' || trim(' ' from vwdestino_colonia) || ' ' ||
    trim(' ' from vwdestino_municipio) || ' ' ||trim(' ' from vwdestino_ciudad) ||' ' || trim(' ' from vwdestino_estado)) as direccion,
    factGuias.fechacompromiso as fecha_compromiso,
    factGuias.vwopenvio_fechaentrega as fecha_entrega
from     guias.factGuias as factGuias
  inner join tr2.dimFrecuenciasMP fmp on codigopostal= factGuias.vwopenvio_codigopos
  left join tr1.dimLocations dimLocation on dimLocation.code = fmp.siglasplaza
WHERE
    $fecha_entrega
     fechacompromiso IS NOT NULL AND
    $qrfiltroCP
    $list_plazas 
    fechacompromiso::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' 
    $garantias_filtro
    $cliente_filtro
    $guia_filtro
    AND substring(vwguias_numeroguia,14,1) IN (
        select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                  from  guias.dimGarantias
                                                  where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
        order by 1
                                                    )
    and vwopenvio_codigopos = '" . $codigopostal . "'
    AND factGuias.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                        FROM guias.factGuias  fg2
                        WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                        AND fg2.VWGUIAS_NUMEROGUIA = factGuias.VWGUIAS_NUMEROGUIA)
    AND factGuias.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
    ORDER BY factGuias.garantia_usar
    ";
        }
        //die($sql);
        return DB::select($sql);
    }

    public static function getDetalleGuiasTr2($plazas_filtro = [], $fecha_filtro = null, $cliente_filtro = 0, $guia_filtro = 0, $garantias_filtro = [], $dia_filtro = 0, $ruta = 0, $fecha_entrega, $codigo_postalNull) {
        $fecha_filtro = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($fecha_filtro));

        if (count($garantias_filtro) > 0) {
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";
            $garantias_filtro = "factGuias.garantia_usar IN ( " . $garantias_filtro . " ) AND ";
        } else {
            $garantias_filtro = "";
        }

        $list_plazas = null;
        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";
            $list_plazas = "factGuias.vwopenvio_siglasdes IN ( " . $plazas_filtro . " )    AND ";
        } else {
            $list_plazas = "";
        }

        if ($cliente_filtro !== null && $cliente_filtro !== '0' && $cliente_filtro != 0) {
            $cliente_filtro = "\n factGuias.vwenvios_numerocliente  = '" . $cliente_filtro . "' AND\n";
        } else {
            $cliente_filtro = "";
        }

        if ($guia_filtro !== null && $guia_filtro !== '0' && $guia_filtro !== '' && $guia_filtro != 0) {
            $guia_filtro = "\n factGuias.vwguias_numeroguia = '" . $guia_filtro . "' AND\n";
        } else {
            $guia_filtro = "";
        }

        if ($fecha_entrega == "false") {
            $fecha_entrega = "\n vwopenvio_fechaentrega IS NULL AND \n";
        } else {
            $fecha_entrega = "";
        }

        if ($codigo_postalNull == "false") {
            $qrfiltroCP = "\n vwopenvio_codigopos IS NOT NULL AND \n";
        } else {
            $qrfiltroCP = "";
        }

        //Start: condition -30
        $conditional = "factGuias.fechacompromiso::date =  '" . $fecha_filtro . "'::date + interval '" . $dia_filtro . " day' AND ";
        $sentence = "";

        if ($dia_filtro == '-30') {
            $conditional = 'factGuias.fechacompromiso between (CURRENT_DATE-1) - 30 AND CURRENT_DATE - 1 AND ';
            $sentence = "
                        AND  NOT exists (
                        SELECT DISTINCT fh.idguia FROM guias.facthistoria fh
                        WHERE fh.idguia = factGuias.vwguias_numeroguia
                        AND claveex IN ('C11','C18','C58','C64','C65','C70','C71','D25','G01','S01','S09','S17','S20','Y00','Y0P','Z00'))
                    ";
        }
        //End: condition -30

        $sql = "
            SELECT
                factGuias.vwguias_numeroguia,
                factGuias.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
                factGuias.vwinfoenvio_contenido,
                CASE factGuias.garantia_usar
                WHEN  '5'::text THEN '11:30'
                WHEN  'D'::text THEN '2 Días'
                WHEN  'A'::text THEN '9:30'
                WHEN  '3'::text THEN '9:30'
                WHEN  '6'::text THEN 'Día siguiente'
                WHEN  'T'::text THEN 'Día siguiente Terrestre'
                WHEN  '7'::text THEN 'Terrestre'
                WHEN  'F'::text THEN 'Paquetería Aerea'
                WHEN  '9'::text THEN 'Apoyo a Operaciones'
                WHEN  'L'::text THEN 'LTL'
                ELSE
                factGuias.garantia_usar
                END  garantia,
                COALESCE(factGuias.vwopguia_numcte,
                    factGuias.vwenvios_numerocliente) as numero_cliente,
                factGuias.vwdestino_razonsocial,
                coalesce(dimLocation.district,'') as distrito,
                coalesce(dimLocation.shortregional,'') as regional,
                factGuias.vwopenvio_siglasdes as plaza,
                factGuias.vwopenvio_codigopos as codigo_postal,
                (trim(' ' from vwdestino_direccion1) || ' ' || trim(' ' from vwdestino_direccion2) || ' ' || trim(' ' from vwdestino_colonia) || ' ' ||
                trim(' ' from vwdestino_municipio) || ' ' ||trim(' ' from vwdestino_ciudad) ||' ' || trim(' ' from vwdestino_estado)) as direccion,
                factGuias.fechacompromiso as fecha_compromiso,
                factGuias.vwopenvio_fechaentrega as fecha_entrega
            FROM
                guias.factGuias as factGuias
                INNER JOIN tr2.dimFrecuenciasMP fmp ON codigopostal= factGuias.vwopenvio_codigopos
                INNER JOIN tr2.dimrutatr2cp AS rtr2 
                ON rtr2.codigopostal = factGuias.vwopenvio_codigopos
                left join tr1.dimLocations dimLocation on dimLocation.code = fmp.siglasplaza
            WHERE
                $fecha_entrega
                fechacompromiso IS NOT NULL AND
                $qrfiltroCP
                $list_plazas
                $conditional
                $garantias_filtro
                $cliente_filtro
                $guia_filtro
		        (( rtr2.sob_ruta='$ruta' and factGuias.vwopenvio_tipoenvio = 1)
                        OR ( rtr2.paq_ruta='$ruta' and (factGuias.vwopenvio_tipoenvio =4 or factGuias.vwopenvio_tipoenvio is NULL))) 
						
                AND substring(vwguias_numeroguia,14,1) IN (
                    select  distinct  substr(tiposervicio, 1,1) as garantiaid
                                                from  guias.dimGarantias
                                                where substr(tiposervicio, 1,1) in (select distinct garantia from guias.dimdiasgarantia)
                                                order by 1
                                                    )
                $sentence
                AND factGuias.vwopenvio_fecharecep = (SELECT max(fg2.vwopenvio_fecharecep) 
                                                    FROM guias.factGuias  fg2
                                                    WHERE fg2.VWOPENVIO_FECHARECEP <= '" . $fecha_filtro . "'::date
                                                    AND fg2.VWGUIAS_NUMEROGUIA = factGuias.VWGUIAS_NUMEROGUIA)
                AND factGuias.vwopenvio_fecharecep <= '" . $fecha_filtro . "'::date
                ORDER BY factGuias.garantia_usar
        ";


        return DB::select($sql);
    }

    public static function getDetalleGuiasTr1($plazas_filtro = [], $fecha_filtro = '2020-03-04', $cliente_filtro = "", $guia_filtro = "", $garantias_filtro = [], $plaza_filtro_param = 'GDL', $dia_seleccionado = 0) {

        /* var_dump($plazas_filtro);
          die(); */
        $alt_plazas_filtro = $plazas_filtro;
        $date = str_replace('/', '-', $fecha_filtro);
        $fecha_filtro = date("Y-m-d", strtotime($date));

        $alt_garantias_filtro = $garantias_filtro;
        /* $fecha_filtro = str_replace('/', '-', $fecha_filtro);
          $fecha_filtro =  date("Y-m-d", strtotime($fecha_filtro)); */


        if ($plazas_filtro !== null && $plazas_filtro !== '0' && count($plazas_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $plazas_filtro = "'" . implode("', '", $plazas_filtro) . "'";

            $plazas_filtro = " ( " . $plazas_filtro . " ) ";
        } else {
            $plazas_filtro = "";
        }

        $fmp_siglasplaza = "";
        if (count($alt_plazas_filtro) > 0) {
            $fmp_siglasplaza = "'" . implode("', '", $alt_plazas_filtro) . "'";
            $fmp_siglasplaza = "AND fmp.siglasplaza IN  ( " . $fmp_siglasplaza . " ) ";
        }

        if ($garantias_filtro !== null && $garantias_filtro !== '0' && count($garantias_filtro) > 0) {

            //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
            $garantias_filtro = "'" . implode("', '", $garantias_filtro) . "'";

            $garantias_filtro = " ( " . $garantias_filtro . " ) ";
        } else {
            $garantias_filtro = "";
        }
        if ($cliente_filtro !== null && $cliente_filtro !== 0) {
            $cliente_filtro = " '" . $cliente_filtro . "' ";
        } else {
            $cliente_filtro = "";
        }
        if ($guia_filtro !== null && $guia_filtro !== 0 && $guia_filtro !== '') {
            $guia_filtro = " '" . $guia_filtro . "' ";
        } else {
            $guia_filtro = "";
        }

        //Inicializar la variable en vacío para evitar missing
        $fg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $fg_vwopenvio_siglasori = "AND fg.vwopenvio_siglasori IN " . $plazas_filtro;
        }
        $fg_siglasdes_usar = "";
        if ($plazas_filtro != "") {
            $fg_siglasdes_usar = "AND fg.siglasdes_usar IN " . $plazas_filtro;
        }
        $fg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $fg_garantia_usar = " AND fg.garantia_usar IN " . $garantias_filtro;
        }

        $fg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $fg_vwenvios_numerocliente = " AND (  fg.vwenvios_numerocliente = " . $cliente_filtro . "  OR fg.vwopguia_numcte = " . $cliente_filtro . " )";
        }

        $gfg_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $gfg_vwenvios_numerocliente = " AND (  gfg.vwenvios_numerocliente = " . $cliente_filtro . "  OR gfg.vwopguia_numcte = " . $cliente_filtro . " )";
        }
        $fg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $fg_vwguias_numeroguia = " AND fg.vwguias_numeroguia " . $guia_filtro;
        }

        $ffg_vwopenvio_siglasori = "";
        if ($plazas_filtro != "") {
            $ffg_vwopenvio_siglasori = " AND ffg.vwopenvio_siglasori IN " . $plazas_filtro;
        }

        $fli_unloadwarehousecode = "";
        if ($plazas_filtro != "") {
            $fli_unloadwarehousecode = " AND fli.unloadwarehousecode IN " . $plazas_filtro;
        }

        $fag_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $fag_garantia_usar = " AND fag.garantia_usar IN " . $garantias_filtro;
        }

        $ffgg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            $ffgg_vwenvios_numerocliente = " AND ffgg.vwenvios_numerocliente  = " . $cliente_filtro . "  OR ffgg.vwopguia_numcte = " . $cliente_filtro;
        }

        $fag_vwguias_numeroguia = "";
        if ($guia_filtro !== "") {
            $fag_vwguias_numeroguia = " AND fag.vwguias_numeroguia = " . $guia_filtro;
        }

        $ffg_vwopenvio_siglasdes = "";
        if ($plazas_filtro != "") {
            $ffg_vwopenvio_siglasdes = " AND ffg.vwopenvio_siglasdes IN " . $plazas_filtro;
        }



        $ffg_garantia_usar = "";
        if ($garantias_filtro !== "") {
            $ffg_garantia_usar = " AND ffg.garantia_usar IN  " . $garantias_filtro;
        }

        $ffg_vwenvios_numerocliente = "";
        if ($cliente_filtro != "") {
            //AND ffg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  ffg.vwopguia_numcte = 'FILTRO CLIENTE'
            $ffg_vwenvios_numerocliente = " AND ffg.vwenvios_numerocliente = " . $cliente_filtro . "  OR  ffg.vwopguia_numcte = " . $cliente_filtro;
        }

        $ffg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $ffg_vwguias_numeroguia = " AND ffg.vwguias_numeroguia = " . $guia_filtro;
        }
        $tfli_unloadwarehousecode = "";
        if ($plazas_filtro != "") {
            $tfli_unloadwarehousecode = " AND tfli.unloadwarehousecode IN " . $plazas_filtro;
        }
        $fli_warehousecode = "";
        if ($plazas_filtro != "") {
            $fli_warehousecode = " AND fli.warehousecode IN " . $plazas_filtro;
        }
        $tfli_warehousecode = "";
        if ($plazas_filtro != "") {
            $tfli_warehousecode = " AND tfli.warehousecode IN " . $plazas_filtro;
        }

        $fag_vwenvios_numerocliente = "";
        if ($cliente_filtro !== "") {
            $fag_vwenvios_numerocliente = " AND fag.vwenvios_numerocliente = " . $cliente_filtro . " OR  fag.vwopguia_numcte = " . $cliente_filtro;
        }

        $gfg_vwguias_numeroguia = "";
        if ($guia_filtro != "") {
            $gfg_vwguias_numeroguia = " AND gfg.vwguias_numeroguia = " . $guia_filtro;
        }


        $alt_plazas_filtro = $plazas_filtro;

        /* $garantia_usar="";
          if( count($alt_garantias_filtro)>0){

          //$plazas_filtro = " ( ".implode(', ', $plazas_filtro)." ) ";
          $garantia_usar = "'" . implode ( "', '", $alt_garantias_filtro ) . "'";

          $garantia_usar = "\n AND garantia_usar IN ( ".$garantia_usar." ) \n";
          }else{
          $garantia_usar="";
          } */


        $fg_vwopenvio_siglasori_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fg_vwopenvio_siglasori_param = " AND     fg.vwopenvio_siglasori      = '" . $plaza_filtro_param . "'";
        }

        $fli_warehousecode_param = "";
        if ($plaza_filtro_param != "" && $plaza_filtro_param != NULL) {
            $fli_warehousecode_param = " AND fli.warehousecode = '" . $plaza_filtro_param . "'";
        }

        $fg_vwopenvio_siglasdes = "";
        if ($plazas_filtro != "") {
            $fg_vwopenvio_siglasdes = " AND fg.vwopenvio_siglasdes IN " . $plazas_filtro;
        }
        //die($fg_vwopenvio_siglasori_param);

        $sql = "

-- Guias de Más de 1 Día AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO 
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
fg.vwinfoenvio_contenido,
CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
--gdg.garantia as garantia,
--SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
--
COALESCE(fg.vwopguia_numcte, 
fg.vwenvios_numerocliente) as numero_cliente,
fg.vwdestino_razonsocial,
dl.district as distrito, 
dl.shortregional as regional,
 fg.vwopenvio_siglasdes as plaza,


  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN guias.factitem fi on fi.waybill = fg.vwguias_numeroguia
  INNER JOIN tr1.factloaditem fli on fli.itemid = fi.id
  --inner join guias.dimGarantias gdg ON substr(gdg.tiposervicio, 1,1) = SUBSTRING(vwguias_numeroguia,14,1)
WHERE
  fg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
  AND fg.garantia_usar IN ('D','7','L') -- Garantias de más de 1 día
  
  $fli_warehousecode_param --PLAZA GRAFICA PARAMETRO --AND fg.vwopenvio_siglasori = 'PUE'
  $fli_unloadwarehousecode      --AND fli.unloadwarehousecode IN  ( 'PUE' )
  $fg_garantia_usar             --AND fg.garantia_usar IN  ( '5' ) 
  $fg_vwenvios_numerocliente    --AND fg.vwenvios_numerocliente  = 'CLIENTE FILTO' OR ffgg.vwopguia_numcte = 'CLIENTE FILTO'
  $fg_vwguias_numeroguia        --AND fg.vwguias_numeroguia = 'guia FINTRO'
UNION ALL


-- El resto de guias (temporalmente) AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
SELECT
    DISTINCT
    fg.vwguias_numeroguia,
    fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
    fg.vwinfoenvio_contenido,
    CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
    --gdg.garantia as garantia,
    --SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
    --
    COALESCE(fg.vwopguia_numcte, 
    fg.vwenvios_numerocliente) as numero_cliente,
    fg.vwdestino_razonsocial,
    dl.district as distrito, 
    dl.shortregional as regional,
    fg.vwopenvio_siglasdes as plaza,

  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal)--Nuevo INNER JOIN
  --inner join guias.dimGarantias gdg ON substr(gdg.tiposervicio, 1,1) = SUBSTRING(vwguias_numeroguia,14,1)
WHERE fg.garantia_usar IN ( 'D', '7','L')
      AND fg.fechacompromiso::date = '$fecha_filtro'::DATE + INTERVAL '$dia_seleccionado day'
      
      $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETR
      $fg_vwopenvio_siglasdes      -- AND fg.vwopenvio_siglasdes IN ('FILTRO PLAZAS')
      $fg_garantia_usar             --AND fg.garantia_usar IN   ( '5' )
      $fg_vwenvios_numerocliente --AND (  fg.vwenvios_numerocliente = 'cliente filtro' OR fg.vwopguia_numcte = 'cliente filtro' )
      $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia = 'guia filtro'
      AND fg.vwguias_numeroguia NOT IN (
                  SELECT gfg.vwguias_numeroguia
                  FROM guias.factguias gfg
                    INNER JOIN guias.factitem gfi on gfi.waybill = gfg.vwguias_numeroguia
                    INNER JOIN tr1.factloaditem tfli on tfli.itemid = gfi.id
                  WHERE
                    gfg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
                    AND gfg.garantia_usar IN ('D','7','L')
                    
                    $tfli_unloadwarehousecode -- AND tfli.unloadwarehousecode IN  ( 'PUE' )  -- AND tfli.unloadwarehousecode IN ('pnaza filtro')
                    $gfg_vwenvios_numerocliente -- AND gfg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  gfg.vwopguia_numcte = 'FILTRO CLIENTE'
                    $gfg_vwguias_numeroguia -- AND gfg.vwguias_numeroguia = 'FILTRO GUIA'
)

UNION ALL


  --Destino siguiente AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
fg.vwinfoenvio_contenido,
CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
--gdg.garantia as garantia,
--SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
--
COALESCE(fg.vwopguia_numcte, 
fg.vwenvios_numerocliente) as numero_cliente,
fg.vwdestino_razonsocial,
dl.district as distrito, 
dl.shortregional as regional,
 fg.vwopenvio_siglasdes as plaza,

  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal)--Nuevo INNER JOIN
  --inner join guias.dimGarantias gdg ON substr(gdg.tiposervicio, 1,1) = SUBSTRING(vwguias_numeroguia,14,1)
WHERE
  fg.garantia_usar not in ('D','7','L')
  AND fg.fechacompromiso::DATE = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
  $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
  $fg_vwopenvio_siglasori --CORRECTO --AND fg.vwopenvio_siglasori IN  ( 'PUE' )  --AND fg.vwopenvio_siglasori IN  ('PLAZAS FILTRO', 'MTY') 
  $fg_garantia_usar --AND fg.garantia_usar IN  ( '5' )  --AND fg.garantia_usar IN  ('gGARANTIAS FILTRO',)
  $fg_vwenvios_numerocliente --AND (  fg.vwenvios_numerocliente = 'cliente filtro' OR fg.vwopguia_numcte = 'cliente filtro' )
  $fg_vwguias_numeroguia --AND fg.vwguias_numeroguia = 'guia filtro'
UNION ALL


-- Guias de Más de 1 Día  AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
fg.vwinfoenvio_contenido,
CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
--gdg.garantia as garantia,
--SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
--
COALESCE(fg.vwopguia_numcte, 
fg.vwenvios_numerocliente) as numero_cliente,
fg.vwdestino_razonsocial,
dl.district as distrito, 
dl.shortregional as regional,
 fg.vwopenvio_siglasdes as plaza,

  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN guias.factitem fi on fi.waybill = fg.vwguias_numeroguia
  INNER JOIN tr1.factloaditem fli on fli.itemid = fi.id
  --inner join guias.dimGarantias gdg ON substr(gdg.tiposervicio, 1,1) = SUBSTRING(vwguias_numeroguia,14,1)
WHERE
  fg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
  AND fg.garantia_usar IN ('D','7','L') -- Garantias de más de 1 día 
  
  $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
  $fli_warehousecode --AND fli.warehousecode IN  ( 'PUE' )  -- AND fli.warehousecode IN ('PLAZAS FILTRO', 'MTY')
  $fg_garantia_usar --AND ffgg.garantia_usar IN  ( '5' )  -- AND ffgg.garantia_usar IN ('Garantiais filtro','D')
  $fg_vwenvios_numerocliente -- AND ffgg.vwenvios_numerocliente  = 'CLIENTE FILTO' OR ffgg.vwopguia_numcte = 'CLIENTE FILTO'
  $fg_vwguias_numeroguia --AND ffgg.vwguias_numeroguia = 'guia FINTRO'
--_________________________________________________
UNION ALL



--El resto de guias (temporalmente)  AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
fg.vwinfoenvio_contenido,
CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
--gdg.garantia as garantia,
--SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
--
COALESCE(fg.vwopguia_numcte, 
fg.vwenvios_numerocliente) as numero_cliente,
fg.vwdestino_razonsocial,
dl.district as distrito, 
dl.shortregional as regional,
 fg.vwopenvio_siglasdes as plaza,

  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  --inner join guias.dimGarantias gdg ON substr(gdg.tiposervicio, 1,1) = SUBSTRING(vwguias_numeroguia,14,1)
WHERE fg.garantia_usar IN ( 'D', '7', 'L')
      AND fg.fechacompromiso::date = '$fecha_filtro'::DATE + INTERVAL '$dia_seleccionado day'
      
      $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
      
      $fg_vwopenvio_siglasori --AND fg.vwopenvio_siglasdes IN  ( 'PUE' )
      $fg_garantia_usar --AND ffg.garantia_usar IN   ( '5' )  -- AND ffg.garantia_usar IN ('GARANTIA FILTROS')
      $fg_vwenvios_numerocliente -- AND ffg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  ffg.vwopguia_numcte = 'FILTRO CLIENTE'
      $fg_vwguias_numeroguia -- AND ffg.vwguias_numeroguia = 'FILTRO GUIA'
      AND fg.vwguias_numeroguia NOT IN (
                SELECT gfg.vwguias_numeroguia
                FROM guias.factguias gfg
                  INNER JOIN guias.factitem gfi on gfi.waybill = gfg.vwguias_numeroguia
                  INNER JOIN tr1.factloaditem tfli on tfli.itemid = gfi.id
                WHERE
                  gfg.fechacompromiso::date = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
                  AND gfg.garantia_usar IN ('D','7','L')
                  $tfli_warehousecode -- AND tfli.warehousecode IN  ( 'PUE' )  -- AND tfli.warehousecode IN ('pnaza filtro')
                  $gfg_vwenvios_numerocliente -- AND gfg.vwenvios_numerocliente = 'FILTRO CLIENTE' OR  gfg.vwopguia_numcte = 'FILTRO CLIENTE'
                  $gfg_vwguias_numeroguia -- AND gfg.vwguias_numeroguia = 'FILTRO GUIA'
)

UNION ALL




    --Guias de Día Siguiente AJUSTADO $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
SELECT
  DISTINCT
  fg.vwguias_numeroguia,
  fg.vwinfoenvio_tipoenvio vwinfoenvio_nomtipoenvio,
  fg.vwinfoenvio_contenido,
  CASE SUBSTRING(fg.vwguias_numeroguia,14,1)::text
  WHEN  '5'::text THEN '11:30'
  WHEN  'D'::text THEN '2 Días'
  WHEN  'A'::text THEN '9:30'
  WHEN  '3'::text THEN '9:30'
  WHEN  '6'::text THEN 'Día siguiente'
  WHEN  'T'::text THEN 'Día siguiente Terrestre'
  WHEN  '7'::text THEN 'Terrestre'
  WHEN  'F'::text THEN 'Paquetería Aerea'
  WHEN  '9'::text THEN 'Apoyo a Operaciones'
  ELSE
  SUBSTRING(fg.vwguias_numeroguia,14,1)::text
END  garantia,
  --SUBSTRING(fg.vwguias_numeroguia,14,1) as garantia,
  --
  COALESCE(fg.vwopguia_numcte,
           fg.vwenvios_numerocliente) as numero_cliente,
  fg.vwdestino_razonsocial,
  dl.district as distrito,
  dl.shortregional as regional,
  fg.vwopenvio_siglasdes as plaza,  
  COALESCE(fg.vwopenvio_codigopos,fg.vwdestino_codigopostal) as codigo_postal,
  ltrim(rtrim(fg.vwdestino_direccion1)) || ' ' || ltrim(rtrim(fg.vwdestino_direccion2)) || ' '
  || ltrim(rtrim(fg.vwdestino_colonia)) || ' ' ||  ltrim(rtrim(fg.vwdestino_municipio)) || ' '
  ||  ltrim(rtrim(fg.vwdestino_ciudad)) || ' ' ||  ltrim(rtrim(fg.vwdestino_estado)) as direccion
FROM guias.factguias fg
  INNER JOIN tr1.dimlocations dl ON dl.code = fg.vwopenvio_siglasdes
  INNER JOIN tr2.dimfrecuenciasmp fmp ON fmp.codigopostal = COALESCE(fg.vwopenvio_codigopos, fg.vwdestino_codigopostal)--Nuevo INNER JOIN
WHERE
  fg.garantia_usar not in ('D','7','L')--Nuevo L
  AND fg.fechacompromiso::DATE = '$fecha_filtro'::date  + INTERVAL '$dia_seleccionado day'
  
  $fg_vwopenvio_siglasori_param --PLAZA GRAFICA PARAMETRO
  $fg_vwopenvio_siglasdes       --AND fg.vwopenvio_siglasdes IN 
  $fg_garantia_usar             --AND fg.garantia_usar IN  ( '5' )
  $fg_vwenvios_numerocliente    --AND (  fg.vwenvios_numerocliente = 'cliente filtro' OR fg.vwopguia_numcte = 'cliente filtro' )
  $fg_vwguias_numeroguia        --AND fg.vwguias_numeroguia = 'guia filtro'
--GROUP BY 2,3,4,5,6,7,8,9,10,11
 ORDER BY 1,2
";
        //die($sql);

        return DB::select($sql);
    }

    public static function getSectoresByPlaza($plaza) {
        $sectores = [];
        $sql = "SELECT sector from tr2.dimsectores WHERE cop = '$plaza' ORDER BY id;";
        $res = DB::select($sql);
        if ($res) {
            
        }
        return $sectores;
    }

    public static function findSectores($plazas, $array = false) {
        $resultado_final = [];
        $sectores = [];
        if (count($plazas) == 0) {
            return $array ? [] : "";
        }
        //iterar plazas seleccionadas y consultar sectores
        foreach ($plazas as $plaza) {
            $s = DB::select("select sector from tr2.dimsectores WHERE cop = '" . $plaza . "' ");
            if (count($s) > 0) {//si obtuvo registros
                foreach ($s as $sec) {
                    $sectores[] = $sec->sector;
                }
                //$resultado_final = array_merge($resultado_final,$sectores);//agregar los registros
            }
        }

        //agregar las visibles seleccionadas
        $resultado_final = array_merge($sectores, $plazas);

        //formatear
        $plazas_filtro = "'" . implode("', '", $resultado_final) . "'";
        return $plazas_filtro;
    }

}
