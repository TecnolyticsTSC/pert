<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class dimLocations extends Model
{
    public $timestamps = false;
    protected $table = "tr1.dimlocations";
    //protected $table = "tr1dimlocations";

    public static function buscar($id, $columns = ['*'])
    {
        return parent::find($id, $columns);
    }

    public function desactivar(){

        $this->update([
            'activo'=>(int)$this->activo==1?0:1,
            'fecha_cambio'=>new \DateTime()
        ]);
    }


}
