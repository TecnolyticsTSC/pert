<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class dimUsuariosModel extends Model
{
    public $timestamps = false;
    public $updated_at = 'fecha_cambio';
    public $created_at = 'fecha_alta';
    protected $table = "admin.dimusuarios";
    //protected $table = "admin-dimusuarios";
    protected $fillable = ['nombres', 'apellidopat', 'apellidomat', 'correo','contrasena','admin','activo','fecha_alta','fecha_cambio'];

    public static function buscar($id, $columns = ['*'])
    {
        return parent::find($id, $columns);
    }

    public function cambiarContrasena($contrasena){
        $this->update([
            'contrasena'=>password_hash($contrasena, PASSWORD_DEFAULT),
            'fecha_cambio'=>new \DateTime()
        ]);
    }
    public function swichActivar(){
        $this->activo==1?$this->desactivar():$this->activar();
    }

    public function desactivar(){
        $this->update([
            'activo'=>(int)$this->activo==1?0:1,
            'fecha_cambio'=>new \DateTime()
        ]);
    }

    public function updateFechaCambio(){
        parent::update([
            'fecha_cambio'=>new \DateTime(),
        ]);
    }


    public function plazas(){
        return $this->hasMany( dimUsuariosPlazaModel::class,'usuarioid');
    }

}
