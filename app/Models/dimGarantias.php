<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Capsule\Manager as DB;
class dimGarantias extends Model
{
    public $timestamps = false;
    protected $table = "guias.dimGarantias";
    //protected $table = "guiasdimGarantias";

    public static function obtenerGarantias()
    {
        return  DB::select(//WHERE table_schema = 'public'
            "select  distinct  substr(tiposervicio, 1,1) as garantiaid, garantia as garantianom  from  guias.dimgarantias"
        );
    }



}
