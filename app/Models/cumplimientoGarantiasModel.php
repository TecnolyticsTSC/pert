<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
class cumplimientoGarantiasModel extends Model
{   
    
    public static function getCharge()
    {
        $query = "
            SELECT DISTINCT cargotype FROM tr1.dimroute 
        ";

        return  DB::select(
            $query
        );
    }

    public static function getGaranties()
    {
        $query = "
            SELECT DISTINCT garantia FROM tr1.factopenvio WHERE tipoenvio=5 
        ";

        return  DB::select(
            $query
        );
    }

    public static function getAllData($id_route_filter,$plaza_filter,$type_route_filter,$type_vehicle_filter,$vehicle_filter,$origin_filter,$destiny_filter,$status_filter,
        $type_incidences_filter,$type_charge_filter,$type_garanties_filter,$client_filter,$start_date_filter,$end_date_filter
    ) {
        
        //Start: condition id_route
        if (count($id_route_filter) > 0) {
            $id_route_filter = "'" . implode("', '", $id_route_filter) . "'";
            $id_route_filter = "AND travel.code IN (" . $id_route_filter . ") ";
        } else {
            $id_route_filter = "";
        }
        //End: condition id_route

        //Start: condition plaza_filter
        if (count($plaza_filter) > 0) {
            $plaza_filter = "'" . implode("', '", $plaza_filter) . "'";
            $plaza_filter = "AND route.warehouse IN (" . $plaza_filter . ") ";
        } else {
            $plaza_filter = "";
        }
        //End: condition plaza_filter

        //Start: condition type_route_filter
        if (count($type_route_filter) > 0) {
            $type_route_filter = "'" . implode("', '", $type_route_filter) . "'";
            $type_route_filter = "AND travel.isaditionaltravel IN (" . $type_route_filter . ")  ";
        } else {
            $type_route_filter = "";
        }
        //End: condition type_route_filter

        //Start: condition type_vehicle_filter
        if (count($type_vehicle_filter) > 0) {
            $type_vehicle_filter = "'" . implode("', '", $type_vehicle_filter) . "'";
            $type_vehicle_filter = "AND transportunit.typename IN (" . $type_vehicle_filter . ") ";
        } else {
            $type_vehicle_filter = "";
        }
        //End: condition type_vehicle_filter

        //Start: condition vehicle_filter
        if (count($vehicle_filter) > 0) {
            $vehicle_filter = "'" . implode("', '", $vehicle_filter) . "'";
            $vehicle_filter = "AND transportunit.code IN (" . $vehicle_filter . ") ";
        } else {
            $vehicle_filter = "";
        }
        //End: condition vehicle_filter

        //Start: condition origin_filter
        if (count($origin_filter) > 0) {
            $origin_filter = "'" . implode("', '", $origin_filter) . "'";
            $origin_filter = "AND travel.locationcodeorigin IN (" . $origin_filter . ") ";
        } else {
            $origin_filter = "";
        }
        //End: condition origin_filter

        //Start: condition destiny_filter
        if (count($destiny_filter) > 0) {
            $destiny_filter = "'" . implode("', '", $destiny_filter) . "'";
            $destiny_filter = "AND travel.locationcodedestiny IN (" . $destiny_filter . ") ";
        } else {
            $destiny_filter = "";
        }
        //End: condition destiny_filter

        //Start: condition status_filter
        if (count($status_filter) > 0) {
            $status_filter = "'" . implode("', '", $status_filter) . "'";
            $status_filter = "AND travel.status IN (" . $status_filter . ") ";
        } else {
            $status_filter = "";
        }
        //End: condition status_filter

        //Start: condition type_charge_filter
        if (count($type_charge_filter) > 0) {
            $type_charge_filter = "'" . implode("', '", $type_charge_filter) . "'";
            $type_charge_filter = "AND  route.cargotype IN (" . $type_charge_filter . ") ";
        } else {
            $type_charge_filter = "";
        }
        //End: condition type_charge_filter

        //Start: condition type_garanties_filter
        if (count($type_garanties_filter) > 0) {
            $type_garanties_filter = "'" . implode("', '", $type_garanties_filter) . "'";
            $type_garanties_filter = "AND  openvio.garantia IN (" . $type_garanties_filter . ") ";
        } else {
            $type_garanties_filter = "";
        }
        //End: condition type_charge_filter

        //Start: condition dates
        //End: condition type_event_filter

        $query = "
            SELECT 
                idroute,
                region,
                regional,
                plaza,
                CASE
                    WHEN isaditionaltravel = true THEN 'Extra'
                    WHEN isaditionaltravel = false THEN 'Programado'
                END as typeRoute,
                route,
                idtramo,
                origendestinytramo,
                typevehicle,
                necovehicle,
                status,
                typechage,
                CONCAT(
                    TO_CHAR(planneddatestart :: DATE, 'dd/mm/yyyy'),
                    ' ',
                    TO_CHAR(plannedtimestart :: TIME, 'HH:MI:SS PM')
                ) AS PTA,
                '' AS ETA,
                CONCAT(
                    TO_CHAR(planneddatefinish :: DATE, 'dd/mm/yyyy'),
                    ' ',
                    TO_CHAR(plannedtimefinish :: TIME, 'HH:MI:SS PM')
                ) AS RTA,
                '' AS incidenceType,
                CASE
                    WHEN EXTRACT(
                        DAY
                        FROM
                            fechacompromiso - current_date
                    ) >= 2 THEN 'Verde'
                    WHEN EXTRACT(
                        DAY
                        FROM
                            fechacompromiso - current_date
                ) >= 0
                AND EXTRACT(
                        DAY
                        FROM
                            fechacompromiso - current_date
                ) <= 1 THEN 'Amarillo'
                WHEN EXTRACT(
                        DAY
                        FROM
                            fechacompromiso - current_date
                ) < 0 THEN 'Rojo'
                END AS umbral
            FROM
                tr1.factanalisiscumplimiento
            WHERE
                status = 'INICIADO'
                $id_route_filter
                $plaza_filter
                $type_route_filter
                $type_vehicle_filter
                $vehicle_filter
                $origin_filter 
                $destiny_filter
                $status_filter
                $type_charge_filter
                $type_garanties_filter
        ";

        return  DB::select(
            $query
        );

    }

    

}
