<?php

namespace App\Auth;


use App\Models\dimUsuariosModel;

class Auth
{

    public function attempt($email, $password){
        $user  = dimUsuariosModel::where('correo', $email)->first();

        if (!$user){
            return false;
        }
        if (password_verify($password, $user->contrasena)){
            $_SESSION['id_usuario'] = $user->id;
            return true;
        }
        return false;
    }


    public function user(){
        return isset( $_SESSION['id_usuario'] ) ? dimUsuariosModel::find($_SESSION['id_usuario']) : [];
    }


    public function check(){
        return isset($_SESSION['id_usuario']);
    }

    public function logout()

    {
        unset($_SESSION['id_usuario']);
    }


}