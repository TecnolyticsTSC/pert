<?php


namespace App\Controllers;

use App\Models\dimLocations;
use App\Models\dimUsuariosModel;
use App\Models\dimUsuariosPlazaModel;
use Illuminate\Database\QueryException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Respect\Validation\Validator as v;

class DimLocationController extends BaseController

{
    /*Devoluciones JSON*/
    public function filtrarPlazas(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        $this->container->logger->info("/API/plazas");

        $filterid = $args['filterid'];
        $code = isset($args['code'])?$args['code']:null;

        $data = dimLocations::
        where('typecode','=','COP');
        if (!empty($code)){
            $data = dimLocations::
            where('code','LIKE', "%{$code}%");
        }

        if ((int)$filterid>0){
            $data->whereNotIn('id',
                dimUsuariosPlazaModel::where('usuarioid' ,'=' ,$filterid)->pluck('locationid')->toArray()
            );
        }

        return $response->withJson([
            "success" => true,
            "data" => $data->get()
        ], 200,
            JSON_PRETTY_PRINT);
    }

    public function asignarPlazas(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        $this->container->logger->info("/API/plazas");
        $data = ($request->getParsedBody());


        $uid = !isset($data['id'])?null:$data['id'];
        $ids = !isset($data['ids'])?null:$data['ids'];

        $errors = [];

        if (!$uid || empty($ids)) {
            $errors[] = 'Datos incompletos';
            $this->container->logger->warning("Sin datos para registrar");
        }

        $data = [];
        if (!$errors) {
            try {
                $data['usuarioid']=$uid;
                foreach ($ids as $item){
                    //Validar que no existan
                    $existe = dimUsuariosPlazaModel::
                    where('usuarioid','=',$uid)
                        ->where('locationid','=',$item['id'])
                        ->where('locationcode','=',$item['code'])
                        ->get();

                    if ($existe->count()===0){
                        $uP = new dimUsuariosPlazaModel();
                        $uP->usuarioid = $uid;
                        $uP->locationid = $item['id'];
                        $uP->locationcode = $item['code'];
                        $uP->fecha_alta = new \DateTime();
                        $uP->save();
                        $data['nuevos'] = $uP;
                        $this->container->logger->info("Nuevo registro exitoso de plaza uid:".$uid." plaza:".$item['code']);
                    }


                }
            }catch (QueryException $e){
                $this->container->logger->error("Falló el registro".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }catch (\Exception $e){
                $this->container->logger->error("Falló el registro".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }

            return $response->withJson([
                "success" => true,
                "data" => $data
            ], 200,
                JSON_PRETTY_PRINT);
        } else {
            // Error occured
            return $response->withJson([
                'success' => false,
                'errors' => $errors
            ], 400,
                JSON_PRETTY_PRINT);
        }

        return $response->withJson([
            "success" => true,
            "data" => $data,
            "errors" => $errors
        ], 200,
            JSON_PRETTY_PRINT);
    }

    public function asignarPlazasUsuarios(ServerRequestInterface $request, ResponseInterface $response,$args)
    {

        //$uids = dimUsuariosModel::where('correo','LIKE','%estafeta.com%')->get();

        $uids='[
{
"id": 14,
"nombres": "Samuel ",
"apellidopat": "Vazquez",
"apellidomat": "E",
"correo": "samuel.vazquez@estafeta.com",
"contrasena": "$2y$10$QV8o.CfxXO0hd0Xc6P6RxOsQ72aQVD6cL8.gSbtB9cdHPm6QQmIyO",
"activo": true,
"fecha_alta": "2020-04-15 14:54:10.525307",
"fecha_cambio": "2020-04-15 14:54:10.525307",
"admin": false
},
{
"id": 16,
"nombres": "Javier Eduardo ",
"apellidopat": "Gonzalez ",
"apellidomat": "de la Garza",
"correo": "javier.gonzalez@estafeta.com",
"contrasena": "$2y$10$QvYIVKGVn7ydCSpLRIiHw.hvUhUwt/eNy794en/0i6gYFX5Hr.uFu",
"activo": true,
"fecha_alta": "2020-04-15 14:55:27.949934",
"fecha_cambio": "2020-04-15 14:55:27.949934",
"admin": false
},
{
"id": 18,
"nombres": "Fernando ",
"apellidopat": "Ruiz ",
"apellidomat": "Corona",
"correo": "fernando.ruiz@estafeta.com",
"contrasena": "$2y$10$LewhJ6hZoWnY5rlqLyzr1eAWMfaxrcMHb5m9pdZx0rvEl9yEAR0DO",
"activo": true,
"fecha_alta": "2020-04-15 14:56:58.831622",
"fecha_cambio": "2020-04-15 14:56:58.831622",
"admin": false
},
{
"id": 20,
"nombres": "Brayan ",
"apellidopat": "Beltran",
"apellidomat": "E",
"correo": "brayan.beltran@estafeta.com",
"contrasena": "$2y$10$aPpg9fBOlXfmmj7zvgJN6OgcRHEStdt8oyP9j3/wYphHVGjWgyixy",
"activo": true,
"fecha_alta": "2020-04-15 14:58:22.154359",
"fecha_cambio": "2020-04-15 14:58:22.154359",
"admin": false
},
{
"id": 31,
"nombres": "Yuri ",
"apellidopat": "Rodriguez",
"apellidomat": "E",
"correo": "yuri.rodriguez@estafeta.com",
"contrasena": "$2y$10$JpTSTMg3gN/a/P6GefQvP.5lyhcVxlB.x/h0cKd6j98kV8pNITliS",
"activo": true,
"fecha_alta": "2020-04-15 15:10:21.961288",
"fecha_cambio": "2020-04-15 15:10:21.961288",
"admin": false
},
{
"id": 3,
"nombres": "Marcos",
"apellidopat": "Jimenez",
"apellidomat": "Burgos",
"correo": "marcos.jimenez@estafeta.com",
"contrasena": "$2y$10$oIG2Ziq0Mw6gFh2ddHYto.n3MXya2D.II110ih.PJWcWsvv3231Fu",
"activo": false,
"fecha_alta": "2020-04-05 21:56:29.196506",
"fecha_cambio": "2020-04-08 16:20:58",
"admin": false
},
{
"id": 9,
"nombres": "Lucila ",
"apellidopat": "Lopez ",
"apellidomat": "Cortes",
"correo": "lucila.lopez@estafeta.com",
"contrasena": "$2y$10$tdxectW0Xi0N21D9vrNOD.0astSjnZ5Lhbnb4rtTcX26nuxlqLq9S",
"activo": true,
"fecha_alta": "2020-04-15 14:49:34.132396",
"fecha_cambio": "2020-04-15 14:49:34.132396",
"admin": false
},
{
"id": 17,
"nombres": "Raul Fernando ",
"apellidopat": "Sanchez",
"apellidomat": "E",
"correo": "raul.sanchez@estafeta.com",
"contrasena": "$2y$10$OcC2cW3qoQCMSnwT4B9C7Omf5rfHFaVLzxTiAuTAdkhySNiuP5uQW",
"activo": true,
"fecha_alta": "2020-04-15 14:56:05.384345",
"fecha_cambio": "2020-04-15 14:56:05.384345",
"admin": false
},
{
"id": 39,
"nombres": "Vladimir ",
"apellidopat": "Lopez ",
"apellidomat": "Robles",
"correo": "vladimir.lopez@estafeta.com",
"contrasena": "$2y$10$GdxlUZMn6zkUP8fVkjIF.OxkEAIgZjkuNTRNEHeNu5.ZjVNqIGkZS",
"activo": true,
"fecha_alta": "2020-04-15 15:14:33.879687",
"fecha_cambio": "2020-04-15 15:14:33.879687",
"admin": false
},
{
"id": 12,
"nombres": "Roberto ",
"apellidopat": "Arriaga",
"apellidomat": "E",
"correo": "roberto.arriaga@estafeta.com",
"contrasena": "$2y$10$XW0EnyNpsDm2f6jQG.IOyuGXe7YYQcXUjA.my4aMX7hBL6YaP7gwG",
"activo": true,
"fecha_alta": "2020-04-15 14:52:11.888403",
"fecha_cambio": "2020-04-15 14:52:11.888403",
"admin": false
},
{
"id": 22,
"nombres": "Lorenzo ",
"apellidopat": "Guerrero",
"apellidomat": "E",
"correo": "lorenzo.guerrero@estafeta.com",
"contrasena": "$2y$10$xQ/oC8NizL6KMvtaKvXad.02g3JYceut84adTAXKf2j70w/1.cCXa",
"activo": true,
"fecha_alta": "2020-04-15 14:59:24.611338",
"fecha_cambio": "2020-04-15 14:59:24.611338",
"admin": false
},
{
"id": 24,
"nombres": "Pablo ",
"apellidopat": "Ramirez ",
"apellidomat": "Zarrazaga",
"correo": "pablo.ramirez@estafeta.com",
"contrasena": "$2y$10$FH9wLy.5nHDHk8c03ARxtuuoTqpWdUpV6Fvu.47Km2T3plDWmMMFS",
"activo": true,
"fecha_alta": "2020-04-15 15:00:37.733758",
"fecha_cambio": "2020-04-15 15:00:37.733758",
"admin": false
},
{
"id": 25,
"nombres": "Cesar Octavio ",
"apellidopat": "Velazquez",
"apellidomat": "E",
"correo": "cesar.velazquez@estafeta.com",
"contrasena": "$2y$10$eWIZ3TfoYv.BFvLpVrauk.6TiyXxFuQEVyHa17W4BQT.FiXSIWVim",
"activo": true,
"fecha_alta": "2020-04-15 15:01:08.017844",
"fecha_cambio": "2020-04-15 15:01:08.017844",
"admin": false
},
{
"id": 27,
"nombres": "Ignacio David ",
"apellidopat": "Perez ",
"apellidomat": "Perez",
"correo": "ignacio.perez@estafeta.com",
"contrasena": "$2y$10$sRycRtcPwYAN14YNDZ4ZeubHpK03Ki/tnTuor.c31u0oAmDvRfcS6",
"activo": true,
"fecha_alta": "2020-04-15 15:08:14.960023",
"fecha_cambio": "2020-04-15 15:08:14.960023",
"admin": false
},
{
"id": 29,
"nombres": "Luis Raul ",
"apellidopat": "Rodriguez ",
"apellidomat": "Ordaz",
"correo": "luis.rodriguez@estafeta.com",
"contrasena": "$2y$10$5utXs2O6cyldpHqK51IdcuQ23QiRJasTzi60vdhmWs6Ve.blWR5wy",
"activo": true,
"fecha_alta": "2020-04-15 15:09:23.914465",
"fecha_cambio": "2020-04-15 15:09:23.914465",
"admin": false
},
{
"id": 33,
"nombres": "Jose Francisco ",
"apellidopat": "Gomez ",
"apellidomat": "Cerda",
"correo": "francisco.gomez@estafeta.com",
"contrasena": "$2y$10$iyY8qNoHSh61Mmay/wvUe.4lmXQ0Nq.RUNt1J4kGMsLcAQOJQMLsK",
"activo": true,
"fecha_alta": "2020-04-15 15:11:23.290271",
"fecha_cambio": "2020-04-15 15:11:23.290271",
"admin": false
},
{
"id": 5,
"nombres": "Misael",
"apellidopat": "Vargas",
"apellidomat": "Padilla",
"correo": "misael.vargas@estafeta.com",
"contrasena": "$2y$10$.KN9WfBFcD1ZBJaq/PKTeeXpUbEQhaVuQYEAgfg9ZRRExhKvuRclO",
"activo": true,
"fecha_alta": "2020-04-08 16:15:23.16084",
"fecha_cambio": "2020-04-08 16:15:23.16084",
"admin": false
},
{
"id": 10,
"nombres": "Ricardo ",
"apellidopat": "Hernandez ",
"apellidomat": "Molina",
"correo": "ricardo.hernandezm@estafeta.com",
"contrasena": "$2y$10$gkciz9N4d8J5JRfsLEXFAODVjqh0PSwxABSfmEiXaKme2c7TLWyRq",
"activo": true,
"fecha_alta": "2020-04-15 14:50:22.711989",
"fecha_cambio": "2020-04-15 14:50:22.711989",
"admin": false
},
{
"id": 11,
"nombres": "Luis ",
"apellidopat": "Molina",
"apellidomat": "E",
"correo": "luis.molina@estafeta.com",
"contrasena": "$2y$10$LGWehygcHGRtv1EzQL1tM.t9CkkqFvSsjiG9Hyw6wO6nQz.GZTmg2",
"activo": true,
"fecha_alta": "2020-04-15 14:51:08.394852",
"fecha_cambio": "2020-04-15 14:51:08.394852",
"admin": false
},
{
"id": 21,
"nombres": "Fernando ",
"apellidopat": "Muro",
"apellidomat": "E",
"correo": "fernando.muro@estafeta.com",
"contrasena": "$2y$10$uiGYNhbeg0frG/F643RoHemO4CBb0QpQ6l9qlogYqCoNGvezA/jmO",
"activo": true,
"fecha_alta": "2020-04-15 14:58:54.656184",
"fecha_cambio": "2020-04-15 14:58:54.656184",
"admin": false
},
{
"id": 32,
"nombres": "Donovan ",
"apellidopat": "Romo ",
"apellidomat": "Duran",
"correo": "donovan.romo@estafeta.com",
"contrasena": "$2y$10$BTNUBJijvewXPjreA3LVO.x54PoE/E.RpSspVqvmm9PhxmO/21RTy",
"activo": true,
"fecha_alta": "2020-04-15 15:10:46.958103",
"fecha_cambio": "2020-04-15 15:10:46.958103",
"admin": false
},
{
"id": 37,
"nombres": "Francisco ",
"apellidopat": "Villa ",
"apellidomat": "Perez",
"correo": "francisco.villa@estafeta.com",
"contrasena": "$2y$10$0bvAGPwvtkOBOImaMF8sE.xrGRpWkFX2zZYdgx.lxcnbFV3NBCNRO",
"activo": true,
"fecha_alta": "2020-04-15 15:13:29.217518",
"fecha_cambio": "2020-04-15 15:13:29.217518",
"admin": false
},
{
"id": 1,
"nombres": "Usuario",
"apellidopat": "Administrador",
"apellidomat": "PERT",
"correo": "prtdw@estafeta.com",
"contrasena": "$2y$10$YJg2Qbn53A8gweqJcV4Dy.2BmJqR.1mRob34Tx15G7QTZp4Z6YBKm",
"activo": true,
"fecha_alta": "2020-04-05 21:38:53.306433",
"fecha_cambio": "2020-04-05 21:38:53.306433",
"admin": true
},
{
"id": 13,
"nombres": "Paulo Alberto ",
"apellidopat": "Gonzalez",
"apellidomat": "E",
"correo": "paulo.gonzalez@estafeta.com",
"contrasena": "$2y$10$svFL4A8Qb8HdD/JMml0B4.Rkn2aI12GTEkSTuYCghhuP/C3NewE0W",
"activo": true,
"fecha_alta": "2020-04-15 14:53:32.694758",
"fecha_cambio": "2020-04-15 14:53:32.694758",
"admin": false
},
{
"id": 19,
"nombres": "Salvador ",
"apellidopat": "Moreno ",
"apellidomat": "Rubio",
"correo": "salvador.moreno@estafeta.com",
"contrasena": "$2y$10$gcYFUxFgJqj7/CjyG9533.u7.NeP75tBdu9K8lT3d5TlmKjM5s95q",
"activo": true,
"fecha_alta": "2020-04-15 14:57:40.112015",
"fecha_cambio": "2020-04-15 14:57:40.112015",
"admin": false
},
{
"id": 26,
"nombres": "Xavier Marcelo ",
"apellidopat": "Canto ",
"apellidomat": "Solis",
"correo": "xavier.canto@estafeta.com",
"contrasena": "$2y$10$n5S7JXvDUrLSclJ7imNb5eqVVagL.y.xPy6WVOgsaynrdKb48Pta6",
"activo": true,
"fecha_alta": "2020-04-15 15:07:34.109638",
"fecha_cambio": "2020-04-15 15:07:34.109638",
"admin": false
},
{
"id": 30,
"nombres": "Gustavo ",
"apellidopat": "Garcia ",
"apellidomat": "Aguilera",
"correo": "gustavo.garcia@estafeta.com",
"contrasena": "$2y$10$32UiWHMx6HNTiN8mzq4hte1H0yRtdk7dxKcD2V7NSu19PApx7GgKG",
"activo": true,
"fecha_alta": "2020-04-15 15:09:52.504952",
"fecha_cambio": "2020-04-15 15:09:52.504952",
"admin": false
},
{
"id": 36,
"nombres": "Luis Fernando ",
"apellidopat": "Herrera ",
"apellidomat": "Ojeda",
"correo": "luis.herrera@estafeta.com",
"contrasena": "$2y$10$dqtsNPlK6v98/bFQtyJs5el5MwsEHOGtvIV.fZ6R0XLFJohvCQjam",
"activo": true,
"fecha_alta": "2020-04-15 15:13:03.142179",
"fecha_cambio": "2020-04-15 15:13:03.142179",
"admin": false
},
{
"id": 38,
"nombres": "Eduardo Rafael ",
"apellidopat": "Barrientos ",
"apellidomat": "Diaz",
"correo": "eduardo.barrientos@estafeta.com",
"contrasena": "$2y$10$iHaPZHM2gxtK6Fi9BvDFgOd.5PW0ggt8yR8dZA8sHKc3lJ/Sw4Eey",
"activo": true,
"fecha_alta": "2020-04-15 15:14:04.754753",
"fecha_cambio": "2020-04-15 15:14:04.754753",
"admin": false
},
{
"id": 40,
"nombres": "Jose Alejandro ",
"apellidopat": "Gonzalez ",
"apellidomat": "Escalante",
"correo": "alejandro.gonzalez@estafeta.com",
"contrasena": "$2y$10$VQDuKpaEEon3AE3hotIA9uMuQqYcuaq4/R2HpEPunxovd1E86VLXy",
"activo": true,
"fecha_alta": "2020-04-15 15:15:07.536305",
"fecha_cambio": "2020-04-15 15:15:07.536305",
"admin": false
},
{
"id": 7,
"nombres": "Fatima",
"apellidopat": "Verdugo",
"apellidomat": "E",
"correo": "fatima.verdugo@estafeta.com",
"contrasena": "$2y$10$nGSW5dQcSid7kWmtawMMqe7Hzode7HXneZxV9SoH6wUTG6M9MOLUC",
"activo": true,
"fecha_alta": "2020-04-15 14:37:58.104028",
"fecha_cambio": "2020-04-15 14:37:58.104028",
"admin": false
},
{
"id": 28,
"nombres": "Miguel Angel ",
"apellidopat": "Martinez ",
"apellidomat": "Lopez",
"correo": "miguel.martinezl@estafeta.com",
"contrasena": "$2y$10$fZSG5ynM4Mxeq50uj3YXzekI0zf8ADkA3XZXQWkh6q0teUTpGd0Ji",
"activo": true,
"fecha_alta": "2020-04-15 15:08:44.65151",
"fecha_cambio": "2020-04-15 15:08:44.65151",
"admin": false
},
{
"id": 15,
"nombres": "Usiel Martin ",
"apellidopat": "Corona",
"apellidomat": "E",
"correo": "usiel.corona@estafeta.com",
"contrasena": "$2y$10$G09yst1VHwSJOAxy0zrtY.dbPNUJ.Zq.qaNTzsK1vaLisDXpyz3nC",
"activo": true,
"fecha_alta": "2020-04-15 14:54:46.288161",
"fecha_cambio": "2020-04-15 14:54:46.288161",
"admin": false
},
{
"id": 23,
"nombres": "Humberto Ramiro ",
"apellidopat": "Rodriguez",
"apellidomat": "E",
"correo": "humberto.ramiro@estafeta.com",
"contrasena": "$2y$10$8w02UgYqdIA06zcDI5CdueXjNj0au9L8qlS8.uOAtY4vEF65i1h7C",
"activo": true,
"fecha_alta": "2020-04-15 15:00:06.492425",
"fecha_cambio": "2020-04-15 15:00:06.492425",
"admin": false
},
{
"id": 35,
"nombres": "Ricardo ",
"apellidopat": "Castro ",
"apellidomat": "Castro Hernandez",
"correo": "ricardo.castro@estafeta.com",
"contrasena": "$2y$10$niE/V5WCrjCv0a7FIDLP.u7mrdqGyI7GvCrcyd5xy9iMIOeFhq5qS",
"activo": true,
"fecha_alta": "2020-04-15 15:12:25.927414",
"fecha_cambio": "2020-04-15 15:12:25.927414",
"admin": false
},
{
"id": 8,
"nombres": "Martha Luz Ailed ",
"apellidopat": "Estrada ",
"apellidomat": "Ordonez",
"correo": "martha.estrada@estafeta.com",
"contrasena": "$2y$10$gvA7PmyamFdybDb3YXHS3.Rq.83tB51WPReP7uN3An.fpOnNzqs9K",
"activo": true,
"fecha_alta": "2020-04-15 14:39:27.033836",
"fecha_cambio": "2020-04-15 14:39:27.033836",
"admin": false
},
{
"id": 34,
"nombres": "Jesus Eduardo ",
"apellidopat": "Hermosillo",
"apellidomat": "E",
"correo": "jesus.hermosillo@estafeta.com",
"contrasena": "$2y$10$naEguOI.IDHJ91f3.XlBlOtnRmRtHUzzJU.De6dAsPK.KVWtHTSBi",
"activo": true,
"fecha_alta": "2020-04-15 15:11:55.558041",
"fecha_cambio": "2020-04-15 15:11:55.558041",
"admin": false
},
{
"id": 41,
"nombres": "Luis Manuel ",
"apellidopat": "Castrejon",
"apellidomat": "E",
"correo": "luis.castrejon@estafeta.com",
"contrasena": "$2y$10$pkIdpJ1aF7jtwrRwk6.d9.qOGXxbBxZpxqwpZZPDOthn9JxNwiCRS",
"activo": true,
"fecha_alta": "2020-04-15 15:15:35.672306",
"fecha_cambio": "2020-04-15 15:15:35.672306",
"admin": false
}
]';

        $ids='[
        {
            "id": 2,
            "usuarioid": "2",
            "locationid": "8",
            "locationcode": "MTY",
            "fecha_alta": "2020-04-06 20:57:53",
            "fecha_cambio": "2020-04-06 20:57:50.927358"
        },
        {
            "id": 112,
            "usuarioid": "2",
            "locationid": "176",
            "locationcode": "TXC",
            "fecha_alta": "2020-04-14 15:15:44",
            "fecha_cambio": "2020-04-14 15:15:49.187778"
        },
        {
            "id": 113,
            "usuarioid": "2",
            "locationid": "58",
            "locationcode": "XAL",
            "fecha_alta": "2020-04-14 15:17:52",
            "fecha_cambio": "2020-04-14 15:17:57.475157"
        },
        {
            "id": 57,
            "usuarioid": "2",
            "locationid": "93",
            "locationcode": "MXL",
            "fecha_alta": "2020-04-13 05:21:19",
            "fecha_cambio": "2020-04-13 05:21:19.371323"
        },
        {
            "id": 64,
            "usuarioid": "2",
            "locationid": "90",
            "locationcode": "TIJ",
            "fecha_alta": "2020-04-13 05:21:21",
            "fecha_cambio": "2020-04-13 05:21:21.330962"
        },
        {
            "id": 56,
            "usuarioid": "2",
            "locationid": "68",
            "locationcode": "CUL",
            "fecha_alta": "2020-04-13 05:21:19",
            "fecha_cambio": "2020-04-13 05:21:19.087508"
        },
        {
            "id": 62,
            "usuarioid": "2",
            "locationid": "121",
            "locationcode": "CYL",
            "fecha_alta": "2020-04-13 05:21:20",
            "fecha_cambio": "2020-04-13 05:21:20.757767"
        },
        {
            "id": 110,
            "usuarioid": "2",
            "locationid": "60",
            "locationcode": "COR",
            "fecha_alta": "2020-04-14 15:14:02",
            "fecha_cambio": "2020-04-14 15:14:06.962353"
        },
        {
            "id": 111,
            "usuarioid": "2",
            "locationid": "56",
            "locationcode": "PAZ",
            "fecha_alta": "2020-04-14 15:14:43",
            "fecha_cambio": "2020-04-14 15:14:48.301516"
        },
        {
            "id": 55,
            "usuarioid": "2",
            "locationid": "59",
            "locationcode": "TGZ",
            "fecha_alta": "2020-04-13 05:21:18",
            "fecha_cambio": "2020-04-13 05:21:18.791508"
        },
        {
            "id": 114,
            "usuarioid": "2",
            "locationid": "46",
            "locationcode": "TIN",
            "fecha_alta": "2020-04-14 15:19:00",
            "fecha_cambio": "2020-04-14 15:19:04.690312"
        },
        {
            "id": 1,
            "usuarioid": "2",
            "locationid": "2",
            "locationcode": "GDL",
            "fecha_alta": "2020-04-06 20:57:27",
            "fecha_cambio": "2020-04-06 20:57:24.747936"
        },
        {
            "id": 63,
            "usuarioid": "2",
            "locationid": "74",
            "locationcode": "HMO",
            "fecha_alta": "2020-04-13 05:21:21",
            "fecha_cambio": "2020-04-13 05:21:21.043515"
        },
        {
            "id": 92,
            "usuarioid": "2",
            "locationid": "1",
            "locationcode": "MEX",
            "fecha_alta": "2020-04-13 07:27:55",
            "fecha_cambio": "2020-04-13 07:27:57.39316"
        },
        {
            "id": 97,
            "usuarioid": "2",
            "locationid": "20",
            "locationcode": "MAM",
            "fecha_alta": "2020-04-13 07:28:24",
            "fecha_cambio": "2020-04-13 07:28:26.311876"
        },
        {
            "id": 98,
            "usuarioid": "2",
            "locationid": "106",
            "locationcode": "COL",
            "fecha_alta": "2020-04-13 07:28:26",
            "fecha_cambio": "2020-04-13 07:28:27.692632"
        },
        {
            "id": 58,
            "usuarioid": "2",
            "locationid": "76",
            "locationcode": "LAP",
            "fecha_alta": "2020-04-13 05:21:19",
            "fecha_cambio": "2020-04-13 05:21:19.646778"
        },
        {
            "id": 61,
            "usuarioid": "2",
            "locationid": "47",
            "locationcode": "MID",
            "fecha_alta": "2020-04-13 05:21:20",
            "fecha_cambio": "2020-04-13 05:21:20.474341"
        },
        {
            "id": 65,
            "usuarioid": "2",
            "locationid": "61",
            "locationcode": "PUE",
            "fecha_alta": "2020-04-13 05:21:21",
            "fecha_cambio": "2020-04-13 05:21:21.606122"
        },
        {
            "id": 3,
            "usuarioid": "2",
            "locationid": "80",
            "locationcode": "LEN",
            "fecha_alta": "2020-04-06 20:58:05",
            "fecha_cambio": "2020-04-06 20:58:02.643937"
        },
        {
            "id": 60,
            "usuarioid": "2",
            "locationid": "64",
            "locationcode": "VER",
            "fecha_alta": "2020-04-13 05:21:20",
            "fecha_cambio": "2020-04-13 05:21:20.190102"
        },
        {
            "id": 95,
            "usuarioid": "2",
            "locationid": "19",
            "locationcode": "REX",
            "fecha_alta": "2020-04-13 07:28:20",
            "fecha_cambio": "2020-04-13 07:28:22.299594"
        },
        {
            "id": 96,
            "usuarioid": "2",
            "locationid": "38",
            "locationcode": "MLM",
            "fecha_alta": "2020-04-13 07:28:22",
            "fecha_cambio": "2020-04-13 07:28:24.184162"
        },
        {
            "id": 100,
            "usuarioid": "2",
            "locationid": "116",
            "locationcode": "TTT",
            "fecha_alta": "2020-04-13 07:28:28",
            "fecha_cambio": "2020-04-13 07:28:29.906817"
        },
        {
            "id": 59,
            "usuarioid": "2",
            "locationid": "96",
            "locationcode": "CUU",
            "fecha_alta": "2020-04-13 05:21:19",
            "fecha_cambio": "2020-04-13 05:21:19.917697"
        },
        {
            "id": 93,
            "usuarioid": "2",
            "locationid": "30",
            "locationcode": "TLC",
            "fecha_alta": "2020-04-13 07:28:18",
            "fecha_cambio": "2020-04-13 07:28:20.097968"
        },
        {
            "id": 94,
            "usuarioid": "2",
            "locationid": "26",
            "locationcode": "CVM",
            "fecha_alta": "2020-04-13 07:28:19",
            "fecha_cambio": "2020-04-13 07:28:21.197955"
        },
        {
            "id": 99,
            "usuarioid": "2",
            "locationid": "87",
            "locationcode": "QRO",
            "fecha_alta": "2020-04-13 07:28:27",
            "fecha_cambio": "2020-04-13 07:28:28.794988"
        },
        {
            "id": 109,
            "usuarioid": "2",
            "locationid": "37",
            "locationcode": "CVA",
            "fecha_alta": "2020-04-14 15:12:29",
            "fecha_cambio": "2020-04-14 15:12:33.74493"
        }
    ]';

        $errors = [];


        $data = [];
        if (!$errors) {
            try {
                $uid = 0;
                $data['usuarioid']=$uid;
                $ids = json_decode($ids,true);
                foreach ($ids as $item){
                    //Validar que no existan
                    /*$existe = dimUsuariosPlazaModel::
                    where('usuarioid','=',$uid)
                        ->where('locationid','=',$item['id'])
                        ->where('locationcode','=',$item['code'])
                        ->get();*/

                    if (true){
                        $uP = new dimUsuariosPlazaModel();
                        $uP->usuarioid = $uid;
                        $uP->locationid = $item['locationid'];
                        $uP->locationcode = $item['locationcode'];
                        $uP->fecha_alta = new \DateTime();
                        $uP->save();
                        $data['nuevos'][] = $uP;
                    }
                }
            }catch (QueryException $e){
                $this->container->logger->error("Falló el registro".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }catch (\Exception $e){
                $this->container->logger->error("Falló el registro".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }

            return $response->withJson([
                "success" => true,
                "data" => $data
            ], 200,
                JSON_PRETTY_PRINT);
        } else {
            // Error occured
            return $response->withJson([
                'success' => false,
                'errors' => $errors
            ], 400,
                JSON_PRETTY_PRINT);
        }

        return $response->withJson([
            "success" => true,
            "data" => $data,
            "errors" => $errors
        ], 200,
            JSON_PRETTY_PRINT);
    }

    public function removerPlaza(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        $this->container->logger->info("/API/plazas/remover/");
        $id = $args['id'];

        $errors = [];

        if (!(int)$id>0) {
            $errors[] = 'Datos incompletos';
            $this->container->logger->warning("Datos incompletos");
        }

        $data = [];
        if (!$errors) {
            try {
                $data['removed']=$id;
                dimUsuariosPlazaModel::where('id','=',$id)->first()->delete();
                $this->container->logger->info("Se ha borrado id:".$id);

            }catch (QueryException $e){
                $this->container->logger->error("Falló el borrado".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }catch (\Exception $e){
                $this->container->logger->error("Falló el borrado".$e->getMessage());
                $errors[] = "Ha ocurrido un error interno";
            }

            return $response->withJson([
                "success" => true,
                "data" => $data
            ], 200,
                JSON_PRETTY_PRINT);
        } else {
            // Error occured
            return $response->withJson([
                'success' => false,
                'errors' => $errors
            ], 400,
                JSON_PRETTY_PRINT);
        }

        return $response->withJson([
            "success" => true,
            "data" => $data,
            "errors" => $errors
        ], 200,
            JSON_PRETTY_PRINT);
    }




}