<?php
namespace App\Controllers;

use App\Models\cumplimientoTR1Model;
use App\Models\cumplimientoGarantiasModel;

class CumplimientoGarantiasController extends BaseController

{
    public function CumplimientoGarantias($request, $response, $urlparams)
    {
        $this->container->logger->info("/RutasTransito"); 

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }

        $d = date("d");
        $m = date("F");
        $y = date("Y");

        $date = $m . ' ' .$d. ', ' .$y;

        return $this->container->view->render($response, 'plataforma/cumplimiento-tr1/CumplimientoGarantias.twig',
            array(
                'titulo' => 'Análisis de Cumplimiento de Garantías',
                'titulo_principal' => 'Análisis de Cumplimiento de Garantías de ' .$date,
                'callback' => $callback,
                'idRoutes' => cumplimientoTR1Model::getIdRoutes(),
                'plazas' => cumplimientoTR1Model::getPlazas(),
                'typeRoutes' => cumplimientoTR1Model::getTypeRoutes(),
                'typeVehicle' => cumplimientoTR1Model::getTypeVehicle(),
                'vehicle' => cumplimientoTR1Model::getVehicle(),
                'origin' => cumplimientoTR1Model::getOrigin(),
                'destiny' => cumplimientoTR1Model::getDestiny(),
                'status' => cumplimientoTR1Model::getStatus(),
                'typeCharge' => cumplimientoGarantiasModel::getCharge(),
                'typeGaranties' => cumplimientoGarantiasModel::getGaranties()
            ));
    }

    public function getData($request, $response, $args)
    {
        $data = ($request->getParsedBody());

        $id_route_filter = !empty($data['id_route_filter']) ? $data['id_route_filter'] : [];
        $plaza_filter = !empty($data['plaza_filter']) ? $data['plaza_filter'] : [];
        $type_route_filter = !empty($data['type_route_filter']) ? $data['type_route_filter'] : [];
        $type_vehicle_filter = !empty($data['type_vehicle_filter']) ? $data['type_vehicle_filter'] : [];
        $vehicle_filter = !empty($data['vehicle_filter']) ? $data['vehicle_filter'] : [];
        $origin_filter = !empty($data['origin_filter']) ? $data['origin_filter'] : [];
        $destiny_filter = !empty($data['destiny_filter']) ? $data['destiny_filter'] : [];
        $status_filter = !empty($data['status_filter']) ? $data['status_filter'] : [];
        $type_incidences_filter = !empty($data['type_incidences_filter']) ? $data['type_incidences_filter'] : [];
        $type_charge_filter = !empty($data['type_charge_filter']) ? $data['type_charge_filter'] : [];
        $type_garanties_filter = !empty($data['type_garanties_filter']) ? $data['type_garanties_filter'] : [];
        $client_filter = !isset($data['client_filter']) ? null : $data['client_filter'];
        $start_date_filter = !isset($data['start_date_filter']) ? new \DateTime() : $data['start_date_filter'];
        $end_date_filter = !isset($data['end_date_filter']) ? new \DateTime() : $data['end_date_filter'];

        $res = CumplimientoGarantiasModel::getAllData(
            $id_route_filter,
            $plaza_filter,
            $type_route_filter,
            $type_vehicle_filter,
            $vehicle_filter,
            $origin_filter,
            $destiny_filter,
            $status_filter,
            $type_incidences_filter,
            $type_charge_filter,
            $type_garanties_filter,
            $client_filter,
            $start_date_filter,
            $end_date_filter
        );

        //Convert stdClass in array
        $stdRoutes = json_decode(json_encode($res), true);
        //Group by region
        $byGroupRoute = self::group_by("idroute", $stdRoutes);
        //Group by region
        $byGroupRegion = self::group_by("umbral", $stdRoutes);

        $formatData = self::format_data($byGroupRoute);

        $totalUmbrals = array(array(
            'green' => !isset($byGroupRegion['Verde']) ? [] : $byGroupRegion['Verde'],
            'yellow' => !isset($byGroupRegion['Amarillo']) ? [] : $byGroupRegion['Amarillo'],
            'red' => !isset($byGroupRegion['Rojo']) ? [] : $byGroupRegion['Rojo'],
        ));
       

        die(json_encode(array("data" => $formatData, "umbrales" => $totalUmbrals)));
       
    }

    //Functions for operations
    private static function group_by($key, $data) {
        $result = array();
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
        return $result;
    }

    private static function format_data($data){
        $result = [];

        foreach ($data as $item) {
            $count_green = 0;
            $count_yellow = 0;
            $count_red = 0;
            $resultAux = [];
            for ($i=0; $i < COUNT($item); $i++) { 
                if($item[$i]['umbral'] == 'Verde'){
                    $count_green++;
                }
                if($item[$i]['umbral'] == 'Amarillo'){
                    $count_yellow++;
                }
                if($item[$i]['umbral'] == 'Rojo'){
                    $count_red++;
                }
                $total = $count_green + $count_yellow + $count_red;
                $resultAux = array(
                    'idroute' => $item[$i]['idroute'],
                    'region' => $item[$i]['region'],
                    'regional' => $item[$i]['regional'],
                    'plaza' => $item[$i]['plaza'],
                    'typeroute' => $item[$i]['typeroute'],
                    'route' => $item[$i]['route'],
                    'idtramo' => $item[$i]['idtramo'],
                    'origendestinytramo' => $item[$i]['origendestinytramo'],
                    'typevehicle' => $item[$i]['typevehicle'],
                    'necovehicle' => $item[$i]['necovehicle'],
                    'status' => $item[$i]['status'],
                    'typechage' => $item[$i]['typechage'],
                    'intime' => self::numberWithCommas($count_green),
                    '%intime' => round($count_green / $total) * 100,
                    'delay' => self::numberWithCommas($count_yellow),
                    '%delay' => round($count_yellow / $total) * 100,
                    'late' => self::numberWithCommas($count_red),
                    '%late' => round($count_red / $total) * 100,
                    'pta' => $item[$i]['pta'],
                    'eta' => $item[$i]['eta'],
                    'rta' => $item[$i]['rta'],
                    'incidencetype' => $item[$i]['incidencetype'],
                );
            }
            array_push($result, $resultAux);
        }

        return $result;
    }

    private static  function numberWithCommas($x) {
        return number_format($x);
    }
}
