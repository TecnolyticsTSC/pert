<?php

namespace App\Controllers;

use App\Models\cumplimientoTR1Model;

class TR1CumplimientoController extends BaseController

{
    public function TR1Cumplimiento($request, $response, $urlparams)
    {
        $this->container->logger->info("/TR1Cumplimiento");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }

        return $this->container->view->render(
            $response,
            'plataforma/cumplimiento-tr1/TR1Cumplimiento.twig',
            array(
                'titulo' => 'Cumplimiento TR1',
                'titulo_principal' => 'Cumplimiento TR1',
                'callback' => $callback,
                'idRoutes' => cumplimientoTR1Model::getIdRoutes(),
                'plazas' => cumplimientoTR1Model::getPlazas(),
                'typeRoutes' => cumplimientoTR1Model::getTypeRoutes(),
                'origin' => cumplimientoTR1Model::getOrigin(),
                'destiny' => cumplimientoTR1Model::getDestiny(),
                'nOperator' => cumplimientoTR1Model::getNOperator(),
                'typeVehicle' => cumplimientoTR1Model::getTypeVehicle(),
                'vehicle' => cumplimientoTR1Model::getVehicle(),
                'status' => cumplimientoTR1Model::getStatus()
            )
        );
    }

    public function getData($request, $response, $args)
    {
        $data = ($request->getParsedBody());

        $id_route_filter = !empty($data['id_route_filter']) ? $data['id_route_filter'] : [];
        $plaza_filter = !empty($data['plaza_filter']) ? $data['plaza_filter'] : [];
        $type_route_filter = !empty($data['type_route_filter']) ? $data['type_route_filter'] : [];
        $origin_filter = !empty($data['origin_filter']) ? $data['origin_filter'] : [];
        $destiny_filter = !empty($data['destiny_filter']) ? $data['destiny_filter'] : [];
        $n_operator_filter = !empty($data['n_operator_filter']) ? $data['n_operator_filter'] : [];
        $type_vehicle_filter = !empty($data['type_vehicle_filter']) ? $data['type_vehicle_filter'] : [];
        $vehicle_filter = !empty($data['vehicle_filter']) ? $data['vehicle_filter'] : [];
        $por_ocupation_filter = !isset($data['por_ocupation_filter']) ? 0 : $data['por_ocupation_filter'];
        $status_filter = !empty($data['status_filter']) ? $data['status_filter'] : [];
        $type_incidences_filter = !empty($data['type_incidences_filter']) ? $data['type_incidences_filter'] : [];
        $type_event_filter = !isset($data['type_event_filter']) ? null : $data['type_event_filter'];
        $start_date_filter = !isset($data['start_date_filter']) ? new \DateTime() : $data['start_date_filter'];
        $end_date_filter = !isset($data['end_date_filter']) ? new \DateTime() : $data['end_date_filter'];

        $res = cumplimientoTR1Model::getDataGraphic(
            $id_route_filter,
            $plaza_filter,
            $type_route_filter,
            $origin_filter,
            $destiny_filter,
            $n_operator_filter,
            $type_vehicle_filter,
            $vehicle_filter,
            $por_ocupation_filter,
            $status_filter,
            $type_incidences_filter,
            $type_event_filter,
            $start_date_filter,
            $end_date_filter
        );

        //Convert stdClass in array
        $stdRoutes = json_decode(json_encode($res), true);
        //Group by region
        $byGroup = self::group_by("region", $stdRoutes);
        
        $count_green = 0;
        $count_yellow = 0;
        $count_red = 0;

        $count_green_p = 0;
        $count_yellow_p = 0;
        $count_red_p = 0;

        $count_green_c = 0;
        $count_yellow_c = 0;
        $count_red_c = 0;

        $count_green_b = 0;
        $count_yellow_b = 0;
        $count_red_b = 0;

        foreach ($res as $travel) {
            $umbrales = json_decode($travel->umbralesjson);
            if (!isset($umbrales)) {
                $count_green++;
            } else {
               $intervalg = self::getIntervalMinutes($umbrales->green->a);
               $intervalgb = self::getIntervalMinutes($umbrales->green->b);
               
               $intervaly = self::getIntervalMinutes($umbrales->yellow->a);
               $intervalyb = self::getIntervalMinutes($umbrales->yellow->b);

               $intervalr = self::getIntervalMinutes($umbrales->red->a);
               $intervalrb = self::getIntervalMinutes($umbrales->red->b);

               if(self::runCondition($travel->minutes, $umbrales->green->a->operator, $intervalg) && 
                    self::runCondition($travel->minutes, $umbrales->green->b->operator, $intervalgb))
                {
                    $count_green ++;
                }
                else if(self::runCondition($travel->minutes, $umbrales->yellow->a->operator, $intervaly) && 
                self::runCondition($travel->minutes, $umbrales->yellow->b->operator, $intervalyb))
                {
                    $count_yellow ++;
                }
                else if(self::runCondition($travel->minutes, $umbrales->red->a->operator, $intervalr) && 
                self::runCondition($travel->minutes, $umbrales->red->b->operator, $intervalrb))
                {
                    $count_red ++;
                }
                else {
                    $count_green ++;
                }

            }
        }

        if(isset($byGroup['PANO'])) {
            foreach ($byGroup['PANO'] as $travel) {
                $umbrales = !isset($travel->umbralesjson) ? null : json_decode($travel->umbralesjson);
                if (!isset($umbrales)) {
                    $count_green_p++;
                } else {
                   $intervalg = self::getIntervalMinutes($umbrales->green->a);
                   $intervalgb = self::getIntervalMinutes($umbrales->green->b);
                   
                   $intervaly = self::getIntervalMinutes($umbrales->yellow->a);
                   $intervalyb = self::getIntervalMinutes($umbrales->yellow->b);
    
                   $intervalr = self::getIntervalMinutes($umbrales->red->a);
                   $intervalrb = self::getIntervalMinutes($umbrales->red->b);
    
                   if(self::runCondition($travel->minutes, $umbrales->green->a->operator, $intervalg) && 
                        self::runCondition($travel->minutes, $umbrales->green->b->operator, $intervalgb))
                    {
                        $count_green_p ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->yellow->a->operator, $intervaly) && 
                    self::runCondition($travel->minutes, $umbrales->yellow->b->operator, $intervalyb))
                    {
                        $count_yellow_p ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->red->a->operator, $intervalr) && 
                    self::runCondition($travel->minutes, $umbrales->red->b->operator, $intervalrb))
                    {
                        $count_red_p ++;
                    }
                    else {
                        $count_green_p ++;
                    }
    
                }
            }
        } 
        
        if(isset($byGroup['CESU'])) {
            foreach ($byGroup['CESU'] as $travel) {
                $umbrales = !isset($travel->umbralesjson) ? null : json_decode($travel->umbralesjson);
                if (!isset($umbrales)) {
                    $count_green_c++;
                } else {
                   $intervalg = self::getIntervalMinutes($umbrales->green->a);
                   $intervalgb = self::getIntervalMinutes($umbrales->green->b);
                   
                   $intervaly = self::getIntervalMinutes($umbrales->yellow->a);
                   $intervalyb = self::getIntervalMinutes($umbrales->yellow->b);
    
                   $intervalr = self::getIntervalMinutes($umbrales->red->a);
                   $intervalrb = self::getIntervalMinutes($umbrales->red->b);
    
                   if(self::runCondition($travel->minutes, $umbrales->green->a->operator, $intervalg) && 
                        self::runCondition($travel->minutes, $umbrales->green->b->operator, $intervalgb))
                    {
                        $count_green_c ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->yellow->a->operator, $intervaly) && 
                    self::runCondition($travel->minutes, $umbrales->yellow->b->operator, $intervalyb))
                    {
                        $count_yellow_c ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->red->a->operator, $intervalr) && 
                    self::runCondition($travel->minutes, $umbrales->red->b->operator, $intervalrb))
                    {
                        $count_red_c ++;
                    }
                    else {
                        $count_green_c ++;
                    }
    
                }
            }
        } 

        if(isset($byGroup['BAOC'])) {
            foreach ($byGroup['BAOC'] as $travel) {
                $umbrales = !isset($travel->umbralesjson) ? null : json_decode($travel->umbralesjson);
                if (!isset($umbrales)) {
                    $count_green_b++;
                } else {
                   $intervalg = self::getIntervalMinutes($umbrales->green->a);
                   $intervalgb = self::getIntervalMinutes($umbrales->green->b);
                   
                   $intervaly = self::getIntervalMinutes($umbrales->yellow->a);
                   $intervalyb = self::getIntervalMinutes($umbrales->yellow->b);
    
                   $intervalr = self::getIntervalMinutes($umbrales->red->a);
                   $intervalrb = self::getIntervalMinutes($umbrales->red->b);
    
                   if(self::runCondition($travel->minutes, $umbrales->green->a->operator, $intervalg) && 
                        self::runCondition($travel->minutes, $umbrales->green->b->operator, $intervalgb))
                    {
                        $count_green_b ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->yellow->a->operator, $intervaly) && 
                    self::runCondition($travel->minutes, $umbrales->yellow->b->operator, $intervalyb))
                    {
                        $count_yellow_b ++;
                    }
                    else if(self::runCondition($travel->minutes, $umbrales->red->a->operator, $intervalr) && 
                    self::runCondition($travel->minutes, $umbrales->red->b->operator, $intervalrb))
                    {
                        $count_red_b ++;
                    }
                    else {
                        $count_green_b ++;
                    }
    
                }
            }
        }
    
        $dataTotal = array(
            'labels' => array('A tiempo', 'Retrasado', 'Tarde'),
            'datasets' => array(array(
                "backgroundColor" => array('#8DE971', '#FFF36D', '#FF7276'),
                "borderWidth" => 0,
                "data" => array($count_green, $count_yellow, $count_red)
            ))
        );

        $dataPacific = array(
            'labels' => array('A tiempo', 'Retrasado', 'Tarde'),
            'datasets' => array(array(
                "backgroundColor" => array('#8DE971', '#FFF36D', '#FF7276'),
                "borderWidth" => 0,
                "data" => array($count_green_p, $count_yellow_p, $count_red_p)
            ))
        );

        $dataCentral = array(
            'labels' => array('A tiempo', 'Retrasado', 'Tarde'),
            'datasets' => array(array(
                "backgroundColor" => array('#8DE971', '#FFF36D', '#FF7276'),
                "borderWidth" => 0,
                "data" => array($count_green_c, $count_yellow_c, $count_red_c)
            ))
        );

        $dataBajio = array(
            'labels' => array('A tiempo', 'Retrasado', 'Tarde'),
            'datasets' => array(array(
                "backgroundColor" => array('#8DE971', '#FFF36D', '#FF7276'),
                "borderWidth" => 0,
                "data" => array($count_green_b, $count_yellow_b, $count_red_b)
            ))
        );


        die(json_encode(array("datasetsTotal" => $dataTotal, "datasetsPacific" => $dataPacific, "datasetsCentral" => $dataCentral, "datasetsBajio" => $dataBajio, "data"=>$res)));
       
    }

    //Functions for operations
    private static function group_by($key, $data) {
        $result = array();
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
        return $result;
    }

    private static function getIntervalMinutes($umbral)
    {
        switch ($umbral->time) {
            case 'min':
                return cumplimientoTR1Model::createIntervalMinutes(0, 0, $umbral->comparator);
            case 'hrs':
                return cumplimientoTR1Model::createIntervalMinutes(0, $umbral->comparator, 0);
            case 'dias':
                return cumplimientoTR1Model::createIntervalMinutes($umbral->comparator, 0, 0);
            default:
                return 0;
        }
    }

    private static function runCondition($delay, $operator, $comparator)
    {
        switch ($operator) {
            case '=':
                return $delay == $comparator;
            case '<':
                return $delay < $comparator;
            case '>':
                return $delay > $comparator;
            case '>=':
                return $delay >= $comparator;
            case '<=':
                return $delay <= $comparator;

            default:
                return false;
        }
    }

}
