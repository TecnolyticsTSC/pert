<?php


namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Models\dimUsuariosModel;
use Slim\Views\Twig as View;
use Respect\Validation\Validator as v;

class AuthController extends BaseController

{

    public function getLogout($request, $response)

    {
        $this->container->auth->logout();
        return $response->withRedirect($this->container->router->pathFor('landing'));
    }

    public function getLogin($request, $response)

    {
        return $this->container->view->render($response, 'Auth/login.twig',array('page_title'=>'Iniciar Sesion'));
    }


    public function postLogin($request, $response)

    {

        $auth = $this->container->auth->attempt(
            $request->getParam('correo'),
            $request->getParam('contrasena')
        );

        if (!$auth){
            $this->container->flash->addMessage('warning','Verifique su correo y contraseña');
            return $response->withRedirect($this->container->router->pathFor('login'));
        }

        return $response->withRedirect($this->container->router->pathFor('home'));

    }


    public function getSignUp($request, $response)

    {
        return $this->container->view->render($response, 'Auth/signup.twig',array('page_title'=>'Regsitro'));
    }


    public function postSignUp($request, $response)

    {
        $validation = $this->container->validator->validate($request,[
            'correo'=>v::emailAvailable(),
            'contrasena'=> v::notEmpty(),
            'contrasena2'=> v::equals($request->getParam('contrasena2'))
        ]);

        if ($validation->failed()){
            //redireccionar hacia tars
            return $response->withRedirect($this->container->router->pathFor('signup'));
        }

        $datos =
            [
                'correo'=>$request->getParam('correo'),
                'contrasena'=> password_hash($request->getParam('contrasena'), PASSWORD_DEFAULT)
            ];
        $usuario = dimUsuariosModel::create($datos);
        //$usuario->save();


        $this->container->flash->addMessage('info','Registro exitoso');


        $this->container->auth->attempt(
            $usuario->correo,
            $request->getParam('contrasena')
        );
        return $response->withRedirect($this->container->router->pathFor('dashboard'));
    }

}