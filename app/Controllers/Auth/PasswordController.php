<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 22/01/2019
 * Time: 13:14
 */

namespace App\Controllers\Auth;


use App\Controllers\BaseController;
use Respect\Validation\Validator as v;

class PasswordController extends BaseController
{

    public function getChangePassword($request, $response)

    {
        return $this->container->view->render($response, '/Auth/password/change.twig',array('page_title'=>'Cambiar Contraseña'));
    }


    public function postChangePassword($request, $response)

    {
        $validation = $this->container->validator->validate($request,[
            'contrasena_old'=> v::notEmpty()->noWhitespace()->matchesPassword($this->container->auth->user()->contrasena),
            'contrasena'=> v::notEmpty()->noWhitespace()
        ]);

        if ($validation->failed()){
            return $response->withRedirect($this->container->router->pathFor('pwchange'));
        }

        $this->container->auth->user()->cambiarContrasena($request->getParam('contrasena'));

        $this->container->flash->addMessage('success','Se ha actualizado su contraseña');

        return $response->withRedirect($this->container->router->pathFor('home'));

    }

}