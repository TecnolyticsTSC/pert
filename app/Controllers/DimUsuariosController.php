<?php


namespace App\Controllers;

use App\Models\dimUsuariosModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Respect\Validation\Validator as v;

class DimUsuariosController extends BaseController

{

    public function index(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/list.twig',
            array(
                'page_title'=>'Usuarios',
                'name'=>'Usuarios',
                'usuarios'=>dimUsuariosModel::all()
            )
            /*'usuarios'=>dimUsuariosModel::where('admin','=',0)->get()*/
        );
    }

    public function getAddPlazas(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/plazas.twig',
            array(
                'page_title'=>'Plazas Para El Usuario',
                'name'=>'Plazas',
                'usuario'=>dimUsuariosModel::find($args['id'])
            )
        );
    }
    
    public function getAddPantallas(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/pantallas.twig',
            array(
                'page_title'=>'Pantallas Para El Usuario',
                'name'=>'Pantallas',
                'usuario'=>dimUsuariosModel::find($args['id'])
            )
        );
    }

    public function getCreate(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/create.twig',
            array(
                'page_title'=>'Crear Usuario',
                'name'=>'Usuarios',
                /*'ramo'=>array(
                    "nombre"=>($request->getParam("nombre"))?$request->getParam("nombre"):""
                )*/
            )
        );
    }

    public function postCreate(ServerRequestInterface $request, ResponseInterface $response,$args)

    {
        $validation = $this->container->validator->validate($request,[
            'nombres'=>v::notEmpty()->alpha(),
            'apellidopat'=>v::notEmpty()->alpha(),
            'apellidomat'=>v::notEmpty()->alpha(),
            'correo'=>v::emailAvailable(),
            'contrasena'=> v::notEmpty(),
            'contrasena2'=> v::equals($request->getParam('contrasena'))
        ]);
        if ($validation->failed()){
            return $response->withRedirect($this->container->router->pathFor('usuario.create'));
            // die("invalid field name");
        }
        $r =  new dimUsuariosModel;

        $r->nombres = $request->getParam('nombres');
        $r->apellidopat = $request->getParam('apellidopat');
        $r->apellidomat = $request->getParam('apellidomat');
        $r->correo = $request->getParam('correo');
        $r->contrasena = password_hash($request->getParam('contrasena'), PASSWORD_DEFAULT);
        $r->save();

        $this->container->flash->addMessage('success','Se ha registrado con éxito');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));

    }

    public function getEdit(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/edit.twig',
            array(
                'page_title'=>'Editar Usuario',
                'name'=>'Usuarios',
                'usuario'=>dimUsuariosModel::find($args['id'])
            )
        );
    }

    public function postEdit(ServerRequestInterface $request, ResponseInterface $response, $args)
    {

        $dataToValidate = [
            'nombres'=>v::notEmpty()->alpha(),
            'apellidopat'=>v::notEmpty()->alpha(),
            'apellidomat'=>v::notEmpty()->alpha(),
            'correo'=>v::emailAvailable()
        ];

        $r =  dimUsuariosModel::find($args['id']);
        if (!$r){
            $this->container->flash->addMessage('danger','El registro no existe');
            return $response->withRedirect($this->container->router->pathFor('usuario',['id'=>$args['id']]));
        }

        //Validar que el correo sea el perteneciente, sino, validar que no esté registrado previamente
        $dataToValidate['correo'] = v::notEmpty();
        if ($r->correo != $request->getParam('correo')){
            $dataToValidate['correo'] = v::emailAvailable();
        }

        //Validar que los campos de contraseña a cambiar sean iguales
        if ( !empty($request->getParam('contrasena')) ){
            $dataToValidate['contrasena'] = v::notEmpty();
            $dataToValidate['contrasena2'] =  v::equals($request->getParam('contrasena'));
        }

        $validation = $this->container->validator->validate($request,$dataToValidate);

        if ($validation->failed()){
            return $response->withRedirect($this->container->router->pathFor('usuario',['id'=>$args['id']]));
        }

        $r->nombres = $request->getParam('nombres');
        $r->apellidopat = $request->getParam('apellidopat');
        $r->apellidomat = $request->getParam('apellidomat');
        $r->correo = $request->getParam('correo');
        $r->fecha_cambio = new \DateTime();

        if (!empty($request->getParam('contrasena'))){
            $r->contrasena = password_hash($request->getParam('contrasena'), PASSWORD_DEFAULT);
        }
        $r->save();

        $this->container->flash->addMessage('success','Se ha actualizado');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));
    }

    public function postDelete(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $_IDS = explode(",",$request->getParam('usuarios_delete'));
        foreach ($_IDS as $Id) {

            $r = dimUsuariosModel::find($Id);

            $r->desactivar();

        }
        $this->container->flash->addMessage('success','Se ha actualizado');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));
    }


    /*Devoluciones JSON*/
    public function plazas(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        return $response->withJson([
            "success" => true,
            "data" => dimUsuariosModel::find($args['id'])->plazas
        ], 200,
            JSON_PRETTY_PRINT);
    }
    
    public function admin(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        return $this->container->view->render($response, 'plataforma/administrar.twig',
            array()
        );
    }

}