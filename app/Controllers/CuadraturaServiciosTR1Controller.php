<?php
namespace App\Controllers;


class CuadraturaServiciosTR1Controller extends BaseController

{
    public function CuadraturaServiciosTR1($request, $response, $urlparams)
    {
        $this->container->logger->info("/CuadraturaServiciosTR1"); 

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }

        return $this->container->view->render($response, 'plataforma/cumplimiento-tr1/CuadraturaServiciosTR1.twig',
            array(
                'titulo' => 'Cuadratura de Servicios',
                'titulo_principal' => 'Cuadratura de Servicios en Rutas TR1',
                'callback' => $callback
            ));
    }
}