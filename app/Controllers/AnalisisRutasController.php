<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class AnalisisRutasController extends BaseController {

    public function analisisRutas($request, $response, $urlparams) {
        $this->container->logger->info("/analisisrutas");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/analisis_rutas_tr2.twig', array(
                    'titulo' => 'Análisis de Rutas TR2',
                    'titulo_principal' => 'Análisis de Rutas TR2',
                    'callback' => $callback
        ));
    }

}
