<?php


namespace App\Controllers;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class SalidasController extends BaseController

{

    public function salidas($request, $response, $urlparams)

    {
        $this->container->logger->info("/salidas");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/salidas.twig',array(
            'titulo'=>'Salidas TR1',
            'titulo_principal'=>'Salidas TR1',
            'callback'=>$callback
        ));
    }

    public function chart($request, $response, $urlparams)

    {
        $this->container->logger->info("/chart");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/chart.twig',array(
            'titulo'=>'chart',
            'titulo_principal'=>'Ejemplos de gráficas',
            'callback'=>$callback
        ));
    }

}