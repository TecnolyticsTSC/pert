<?php


namespace App\Controllers;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class CargaProcesadaController extends BaseController

{

    public function cargaprocesada($request, $response, $urlparams)

    {
        $this->container->logger->info("/cargaprocesada");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/carga-procesada.twig',array(
            'titulo'=>'Carga Procesada y Eventos de Demora',
            'titulo_principal'=>'Carga Procesada y Eventos de Demora',
            'callback'=>$callback
        ));
    }

}