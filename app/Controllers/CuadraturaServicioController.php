<?php


namespace App\Controllers;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class cuadraturaServicioController extends BaseController

{

    public function cuadraturaServicio($request, $response, $urlparams)

    {
        $this->container->logger->info("/cuadraturaservicio");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/cuadratura_servicio.twig',array(
            'titulo'=>'Identificacion de carga ordenada para entrega y recolección',
            'titulo_principal'=>'Identificacion de carga ordenada para entrega y recolección',
            'callback'=>$callback
        ));
    }


}