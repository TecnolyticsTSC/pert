<?php


namespace App\Controllers;

use App\Models\dimGeocercaModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Respect\Validation\Validator as v;

class DimGeocercasController extends BaseController

{

    public function index(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Geocercas/listGeocercas.twig',
            array(
                'page_title'=>'Localidad-Geocerca',
                'name'=>'Geocercas',
                'GeocercasSelect'=>dimGeocercaModel::selectGeocerca()
            )
        );
             
    }
    public function getCreate(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Geocercas/createGeocerca.twig',
            array(
                'page_title'=>'Crear Geocerca-Localidad',
                'name'=>'Geocerca',
                /*'ramo'=>array(
                    "nombre"=>($request->getParam("nombre"))?$request->getParam("nombre"):""
                )*/
            )
        );
    }



}