<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class NoLocalizadosController extends BaseController {

    public function noLocalizados($request, $response, $urlparams) {
        $this->container->logger->info("/nolocalizados");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/items_no_localizados.twig', array(
                    'titulo' => 'Items No Localizados',
                    'titulo_principal' => 'Items No Localizados',
                    'callback' => $callback
        ));
    }

}
