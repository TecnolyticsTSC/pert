<?php
namespace App\Controllers;
class TendenciaCargaG2Controller extends BaseController

{
    public function TendenciaCargaG2($request, $response, $urlparams)

    {
        // Registrar en el log que se accedió a esta url
        $this->container->logger->info("/AnalisisTendenciaCarga");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        //                                     Ruta donde se encuentra la página a renderizar
        return $this->container->view->render($response, 'plataforma/AnalisisTendenciaCarga.twig',
            /*Parámetros que se le pasarán a la página*/
            array(
                'titulo' => 'Analisis de Tendencia de Carga',
                'titulo_principal' => 'Analisis de Tendencia de Carga, del 02/03/2020 al 13/03/2020 (Pantalla 2 de 2)',
                'callback' => $callback
            ));
    }
}
