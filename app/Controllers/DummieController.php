<?php
namespace App\Controllers;
class DummieController extends BaseController

{
    public function dummie($request, $response, $urlparams)

    {
        // Registrar en el log que se accedió a esta url
        $this->container->logger->info("/nuevapagina");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        //                                     Ruta donde se encuentra la página a renderizar
        return $this->container->view->render($response, 'plataforma/dummie.twig',
            /*Parámetros que se le pasarán a la página*/
            array(
            'titulo'=>'hola titulo',
            'titulo_principal'=>'Hola Twig!',
            'callback'=>$callback
        ));
    }

}