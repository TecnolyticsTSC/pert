<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class DestinosRecoleccionesController extends BaseController {

    public function destinosRecolecciones($request, $response, $urlparams) {
        $this->container->logger->info("/destinosRecolecciones");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/destinos_recolecciones.twig', array(
                    'titulo' => 'Destinos de Recolecciones',
                    'titulo_principal' => 'Destinos de Recolecciones',
                    'callback' => $callback
        ));
    }

}
