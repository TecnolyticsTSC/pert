<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;


class LlegadasTr1Controller extends BaseController {

    public function llegadas($request, $response, $urlparams) {
        $this->container->logger->info("/llegadas");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        $id = $this->container->auth->user()->id;
        return $this->container->view->render($response, 'plataforma/ci/llegadastr1.twig', array(
                    'titulo' => 'Llegadas TR1',
                    'titulo_principal' => 'Llegadas TR1',
                    'callback' => $callback,
//                    "plazas" => dimDiasGarantia::obtenerPlazasUsuario($id),
//                    "tipo_vehiculo" => entradasTr1::obtenerTipoVehiculo(),
//                    "estatus_ruta" => entradasTr1::obtenerEstatusRuta(),
//                    "id_ruta" => entradasTr1::obtenerIdRuta(),
//                    "tramo" => entradasTr1::obtenerTramo(),
//                    "numero_eco" => entradasTr1::obtenerNumeroEco()
        ));
    }

    /*
     * LLena KPIs principal y por filtro
     * */

    public function entradasFiltro($request, $response, $args) {
        $this->container->logger->info("/entradasFiltro");
        $data = ($request->getParsedBody());
        //Aqui lleguen los filtros 
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $estatus_ruta = !empty($data['estatus_ruta']) ? $data['estatus_ruta'] : [];
        $id_ruta = !empty($data['id_ruta']) ? $data['id_ruta'] : [];
        $tramo = !empty($data['tramo']) ? $data['tramo'] : [];
        $cadena_cliente = !isset($data['cliente']) ? null : $data['cliente'];
        //conversion cadena to array
        $cliente = explode(",", $cadena_cliente);
        $tipo_vehiculo = !empty($data['tipo_vehiculo']) ? $data['tipo_vehiculo'] : [];
        $numero_eco = !isset($data['numero_eco']) ? null : $data['numero_eco'];
        $fecha_inicial =  !isset($data['fecha_inicial']) ? null : $data['fecha_inicial'];
        //variables para Avance de Llegadas
        $countLlegadas = entradasTr1::getLlegadas($estatus_ruta,$fecha_inicial,$plazas_filtro);
        $llegadas = 0;
        foreach ($countLlegadas as $lleg) {
            $llegadas = $lleg->llegada;
        }
        $countPendientes = entradasTr1::getPendientes($estatus_ruta,$fecha_inicial,$plazas_filtro);
        $pendientes = 0;
        foreach ($countPendientes as $pend) {
            $pendientes = $pend->pendiente;
        }
        $totalesAvance = $llegadas + $pendientes;
        if ($totalesAvance != 0) {
            $llegadas = ($llegadas * 100) / $totalesAvance;
            $pendientes = ($pendientes * 100) / $totalesAvance;
        } else {
            $llegadas = 80;
            $pendientes = 20;
        }
        $lista_porcentaje = ["Llegadas " . round($llegadas,2) . "%", "Pendientes " . round($pendientes,2) . "%"];
        $datasets = [$llegadas, $pendientes];
        //variables para Garantìas de Llegadas
        $countMayores3 = entradasTr1::getMayorTresDias();
        $countMayor3 = 0;
        foreach ($countMayores3 as $cm3) {
            $countMayor3 = $cm3->mayortres;
        }
        $countMayores2 = entradasTr1::getMayorDosDias();
        $countMayor2 = 0;
        foreach ($countMayores2 as $cm2) {
            $countMayor2 = $cm2->mayordos;
        }
        $countMenoresHoy = entradasTr1::getMenorHoy();
        $countMenorHoy = 0;
        foreach ($countMenoresHoy as $cmh) {
            $countMenorHoy = $cmh->menorhoy;
        }
        $totalesLlegadas = $countMayor3 + $countMayor2 + $countMenorHoy;
        if ($totalesLlegadas != 0) {
            $countMayor3 = ($countMayor3 * 100) / $totalesLlegadas;
            $countMayor2 = ($countMayor2 * 100) / $totalesLlegadas;
            $countMenorHoy = ($countMenorHoy * 100) / $totalesLlegadas;
        } else {
            $countMayor3 = 70;
            $countMayor2 = 20;
            $countMenorHoy = 10;
        }
        $lista_porcentaje2 = ["PTA > 3 dias " . $countMayor3 . "%", "PTA > 2 dias " . $countMayor2 . "%", "PTA < hoy " . $countMenorHoy . "%"];
        $datasets2 = [$countMayor3, $countMayor2, $countMenorHoy];
        //variables para Garantías de Pendientes
        $countMayores3P = entradasTr1::getMayorTresDiasPendientes();
        $countMayor3P = 0;
        foreach ($countMayores3P as $cm3P) {
            $countMayor3P = $cm3P->mayortres;
        }
        $countMayores2P = entradasTr1::getMayorDosDiasPendientes();
        $countMayor2P = 0;
        foreach ($countMayores2P as $cm2P) {
            $countMayor2P = $cm2P->mayordos;
        }
        $countMenoresHoyP = entradasTr1::getMenorHoyPendientes();
        $countMenorHoyP = 0;
        foreach ($countMenoresHoyP as $cmhP) {
            $countMenorHoyP = $cmhP->menorhoy;
        }
        $totalesPendientes = $countMayor3P + $countMayor2P + $countMenorHoyP;
        if ($totalesPendientes != 0) {
            $countMayor3P = ($countMayor3P * 100) / $totalesPendientes;
            $countMayor2P = ($countMayor2P * 100) / $totalesPendientes;
            $countMenorHoyP = ($countMenorHoyP * 100) / $totalesPendientes;
        } else {
            $countMayor3P = 90.5;
            $countMayor2P = 5.5;
            $countMenorHoyP = 4;
        }
        $lista_porcentaje3 = ["PTA > 3 dias " . $countMayor3P . "%", "PTA > 2 dias " . $countMayor2P . "%", "PTA < hoy " . $countMenorHoyP . "%"];
        $datasets3 = [$countMayor3P, $countMayor2P, $countMenorHoyP];
        //LlegadasTr1
        $LlegadasTr1 = entradasTr1::getLlegadasTr1();
        die(json_encode(array("datasets" => $datasets, "labels" => $lista_porcentaje, "datasets2" => $datasets2, "labels2" => $lista_porcentaje2, "datasets3" => $datasets3, "labels3" => $lista_porcentaje3, "data"=>$LlegadasTr1)));
    }

    public function cargadescarga($request, $response, $urlparams) {
        $this->container->logger->info("/entradas");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/analisis_carga_descarga_rutastr1.twig', array(
                    'titulo' => 'Salidas TR1',
                    'titulo_principal' => 'Entradas TR1',
                    'callback' => $callback
        ));
    }

    public function chart($request, $response, $urlparams) {
        $this->container->logger->info("/chart");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/chart.twig', array(
                    'titulo' => 'chart',
                    'titulo_principal' => 'Ejemplos de gráficas',
                    'callback' => $callback
        ));
    }

}
