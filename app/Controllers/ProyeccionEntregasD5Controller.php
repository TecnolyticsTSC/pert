<?php

/**
 * Created by PhpStorm.
 * User: Blue_Beats
 * Date: 20/03/2020
 * Time: 11:22:55
 */

namespace App\Controllers;

use App\Models\dimDiasGarantia;
use App\Models\dimGarantias;
use App\Models\dimUsuariosModel;
use App\Models\factGuias;
use Illuminate\Database\QueryException;

class ProyeccionEntregasD5Controller extends BaseController {

    private $_LISTA_GARANTIAS = [
            [
            "id" => "A",
            "id_alter" => "3",
            "name" => "9:30",
            "backgroundColor" => "#8d83cf"/* Morado */
        ],
            [
            "id" => "3",
            "name" => "9:30",
            "backgroundColor" => "#8d83cf"/* Morado */
        ],
            [
            "id" => "5",
            "name" => "11:30",
            "backgroundColor" => "#e8a97d",
        ],
            [
            "id" => "6",
            "name" => "Dia Siguiente",
            "backgroundColor" => "#aeaca4",
        ],
            [
            "id" => "T",
            "name" => "Dia Siguiente Terrestre",
            "backgroundColor" => "#ddda69",
        ],
            [
            "id" => "D",
            "name" => "2 Dias",
            "backgroundColor" => "#9dcfa5",
        ],
            [
            "id" => "7",
            "name" => "Terrestre",
            "backgroundColor" => "#91aec4",
        ],
            [
            "id" => "F",
            "name" => "Paquetería Aérea",
            "backgroundColor" => "#ffdbad",
        ]
    ];

    public function index($request, $response, $urlparams) {

 

        $this->container->logger->info("/proyeccion/entregas/d5");

 

        $params = $request->getParams();

 

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        $id = $this->container->auth->user()->id;
        //die(json_encode($this->container->auth->user()->id));
        //die(json_encode(factGuias::getCodigosClientes()));
        return $this->container->view->render($response, 'plataforma/proyeccion-d+5/proyeccion_entregas_d5.twig', array(
                    'title' => 'Proyección TR2 D+5',
                    'callback' => $callback,
                    'titulo_principal' => "Proyección TR2 D+5",
//            "plazas" => $this->getPlazas(),
                    "plazas" => dimDiasGarantia::obtenerPlazasUsuario($id),
                    "garantias" => dimDiasGarantia::obtenerGarantias(),
                    "clientes" => []/* factGuias::getCodigosClientes() */
        ));
    }

    public function listarFiltrosPlazas($request, $response, $urlparams) {
        $filtroPlazas = $this->getPlazas();
        die(json_encode($filtroPlazas));
    }

    public function getPlazas() {
        $filtroPlazas = dimUsuariosModel::find($this->container->auth->user()->id)->plazas;
        $banderas = [];
        $plzas = [];

        $listaPlazas = [];

        foreach ($filtroPlazas as $d) {
            if (!in_array($d->locationid, $banderas)) {
                $banderas[] = $d->locationid;
                $plzas[] = $d;
            }
        }


        foreach ($plzas as $plaza) {
            $listaPlazas[] = [
                "locationid" => $plaza->locationid,
                "locationcode" => $plaza->locationcode,
            ];

            if ($plaza->locationcode == 'MEX') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'MX1',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'MX3',
                ];
            }

            if ($plaza->locationcode == 'CVA') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'CTL',
                ];
            }
            if ($plaza->locationcode == 'PUE') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'PU2',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'PU3',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'PU4',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'PU5',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TZN',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TXL',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'APO',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'SMT',
                ];
            }

            if ($plaza->locationcode == 'COR') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'ORI',
                ];
            }
            if ($plaza->locationcode == 'PAZ') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TXP',
                ];
            }

            if ($plaza->locationcode == 'TXC') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TBA',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'CPN',
                ];
            }
            if ($plaza->locationcode == 'XAL') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TIN',
                ];
            }
            if ($plaza->locationcode == 'GDL') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'GD1',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'GD2',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'GD3',
                ];
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'GD4',
                ];
            }
            if ($plaza->locationcode == 'TIJ') {
                //MX1 MX3
                $listaPlazas[] = [
                    "locationid" => rand(100, 1000),
                    "locationcode" => 'TKT',
                ];
            }
            //die($plaza->locationcode);
        }

        return $listaPlazas;
    }

    //Sin uso/obsoleto
    public function filtrarTR1($request, $response, $args) {
        $data = ($request->getParsedBody());

        $fecha_filtro = !isset($data['fecha_filtro']) ? new \DateTime() : $data['fecha_filtro'];
        $plaza_filtro = !isset($data['plaza_filtro']) ? null : $data['plaza_filtro'];
        $garantia_filtro = !isset($data['garantia_filtro']) ? null : $data['garantia_filtro'];
        $cliente_filtro = !isset($data['cliente_filtro']) ? null : $data['cliente_filtro'];
        $guia_filtro = !isset($data['guia_filtro']) ? null : $data['guia_filtro'];


        $entregas = factGuias::getEntregasTR1($fecha_filtro, $plaza_filtro, $garantia_filtro, $cliente_filtro, $guia_filtro);
        //$recoleccion = factGuias::getRecoleccionTR1($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);
        //die(json_encode($entregas));
        return $response->withJson([
                    "success" => true,
                    "data" => [$entregas]
                        ], 200, JSON_PRETTY_PRINT);
    }

    /* public function filtrarTR1(ServerRequestInterface $request, ResponseInterface $response,$args)
      {
      $data = ($request->getParsedBody());

      $fecha_filtro = !isset($data['fecha_filtro'])?new \DateTime():$data['fecha_filtro'];
      $plaza_filtro = !isset($data['plaza_filtro'])?null:$data['plaza_filtro'];
      $garantia_filtro = !isset($data['garantia_filtro'])?null:$data['garantia_filtro'];
      $cliente_filtro = !isset($data['cliente_filtro'])?null:$data['cliente_filtro'];
      $guia_filtro = !isset($data['guia_filtro'])?null:$data['guia_filtro'];


      $entregas = factGuias::getEntregasTR1($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);
      $recoleccion = factGuias::getRecoleccionTR1($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);

      return $response->withJson([
      "success" => true,
      "data" => [$entregas,$recoleccion]
      ], 200,
      JSON_PRETTY_PRINT);
      } */

    /*
      public function dias($request, $response,$args){
      $data = ($request->getParsedBody());
      //die(json_encode($data));
      $fecha_filtro = !isset($data['fecha_filtro'])?new \DateTime():$data['fecha_filtro'];
      $plaza_filtro = !isset($data['plaza_filtro'])?null:$data['plaza_filtro'];
      $garantia_filtro = !isset($data['garantia_filtro'])?null:$data['garantia_filtro'];
      $cliente_filtro = !isset($data['cliente_filtro'])?null:$data['cliente_filtro'];
      $guia_filtro = !isset($data['guia_filtro'])?null:$data['guia_filtro'];


      $lista_garantias = [
      [
      "id"=>"A",
      "id_alter"=>"3",
      "name"=>"9:30",
      "backgroundColor" =>"#8d83cf"
      ],[
      "id"=>"5",
      "name"=>"11:30",
      "backgroundColor" =>"#e8a97d",
      ],
      [
      "id"=>"6",
      "name"=>"Dia Siguiente",
      "backgroundColor" => "#aeaca4",
      ],
      [
      "id"=>"T",
      "name"=>"Dia Siguiente Terrestre",
      "backgroundColor" => "#ddda69",
      ],
      [
      "id"=>"D",
      "name"=>"2 Dias",
      "backgroundColor" => "#9dcfa5",
      ],
      [
      "id"=>"9",
      "name"=>"Apoyo a OT",
      "backgroundColor" => "#c3af85",
      ],
      [
      "id"=>"H",
      "name"=>"Mismo Día",
      "backgroundColor" => "#25c3db",
      ],
      [
      "id"=>"C",
      "name"=>"SITI",
      "backgroundColor" => "#7e8687",
      ]
      ,
      [
      "id"=>"7",
      "name"=>"Terrestre",
      "backgroundColor" => "#38a935",
      ],
      [
      "id"=>"8",
      "name"=>"Internacional",
      "backgroundColor" => "#684d3c",
      ],
      [
      "id"=>"J",
      "name"=>"Cruce de Anden",
      "backgroundColor" => "#FA8072",
      ],
      [
      "id"=>"F",
      "name"=>"Paquetería Aérea",
      "backgroundColor" => "#B22222",
      ],
      [
      "id"=>"F",
      "name"=>"Paquetería Aérea",
      "backgroundColor" => "#FF6347",
      ],
      [
      "id"=>"L",
      "name"=>"LTL Guía Enbarque",
      "backgroundColor" => "#FFA500",
      ],
      [
      "id"=>"9",
      "name"=>"Apoyo a Operaciones",
      "backgroundColor" => "#BDB76B",
      ],
      [
      "id"=>"G",
      "name"=>"USA Canadá Estandard",
      "backgroundColor" => "#9370DB",
      ],
      [
      "id"=>"B",
      "name"=>"Baja Temporal",
      "backgroundColor" => "#7B68EE ",
      ],[
      "id"=>"E",
      "name"=>"Carga Express",
      "backgroundColor" => "#34495e",
      ]

      ];



      $errors = [];

      $data = [];
      if (!$errors) {
      try {

      $reco = $this->recolecciones($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);
      //die(json_encode($reco));

      $res = factGuias::getEntregasTR1($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);


      $grouped = null;
      $dias =[];
      $labels = [];


      foreach ($res as $guia){

      if(!is_null($guia->garantia_usar)) {
      $dias[$guia->dia][] =$guia;
      $labels[] = $guia->dia;
      }

      }

      $datasets =[];
      $baseItem =null;

      foreach ($dias as $index => $entregas) {

      foreach ($entregas as $idxd => $garantias_) {
      foreach ($lista_garantias as $idx => $garantia) {
      // $datasets[$idx] = $baseItem;
      $datasets[$idx]["backgroundColor"] = $garantia["backgroundColor"];
      $datasets[$idx]["id"] = $garantia["id"];
      $datasets[$idx]["label"] = $garantia["name"];
      $datasets[$idx]["stack"] ='Stack 0';

      if((int)$garantias_->garantia_usar === (int) $datasets[$idx]["id"]) {
      $datasets[$idx]["data"][] = (int)$garantias_->cantguias;
      // $datasets[$idx]["d"][] = $garantias_;
      } else{
      $datasets[$idx]["data"][] = 0;
      }
      }
      }

      }

      //$grouped =  $datasets;

      $grouped =array_merge($datasets,$reco);

      //die(json_encode(array("datasets"=>$grouped,"labels"=>$labels)));
      $data=["datasets"=>$grouped,"labels"=>$labels];

      }catch (QueryException $e){
      $this->container->logger->error("Falló el registro".$e->getMessage());
      $errors[] = "Ha ocurrido un error interno";
      }catch (\Exception $e){
      $this->container->logger->error("Falló el registro".$e->getMessage());
      $errors[] = "Ha ocurrido un error interno";
      }

      return $response->withJson([
      "success" => true,
      "data" => $data
      ], 200,
      JSON_PRETTY_PRINT);
      } else {
      // Error occured
      return $response->withJson([
      'success' => false,
      'errors' => $errors
      ], 400,
      JSON_PRETTY_PRINT);
      }



      }

      public function recolecciones($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro){

      $lista_garantias = [
      [
      "id"=>"A",
      "id_alter"=>"3",
      "name"=>"9:30",
      "backgroundColor" =>"#8d83cf"
      ],[
      "id"=>"5",
      "name"=>"11:30",
      "backgroundColor" =>"#e8a97d",
      ],
      [
      "id"=>"6",
      "name"=>"Dia Siguiente",
      "backgroundColor" => "#aeaca4",
      ],
      [
      "id"=>"T",
      "name"=>"Dia Siguiente Terrestre",
      "backgroundColor" => "#ddda69",
      ],
      [
      "id"=>"D",
      "name"=>"2 Dias",
      "backgroundColor" => "#9dcfa5",
      ],
      [
      "id"=>"9",
      "name"=>"Apoyo a OT",
      "backgroundColor" => "#c3af85",
      ],
      [
      "id"=>"H",
      "name"=>"Mismo Día",
      "backgroundColor" => "#25c3db",
      ],
      [
      "id"=>"C",
      "name"=>"SITI",
      "backgroundColor" => "#7e8687",
      ]
      ,
      [
      "id"=>"7",
      "name"=>"Terrestre",
      "backgroundColor" => "#38a935",
      ],
      [
      "id"=>"8",
      "name"=>"Internacional",
      "backgroundColor" => "#684d3c",
      ],
      [
      "id"=>"J",
      "name"=>"Cruce de Anden",
      "backgroundColor" => "#FA8072",
      ],
      [
      "id"=>"F",
      "name"=>"Paquetería Aérea",
      "backgroundColor" => "#B22222",
      ],
      [
      "id"=>"F",
      "name"=>"Paquetería Aérea",
      "backgroundColor" => "#FF6347",
      ],
      [
      "id"=>"L",
      "name"=>"LTL Guía Enbarque",
      "backgroundColor" => "#FFA500",
      ],
      [
      "id"=>"9",
      "name"=>"Apoyo a Operaciones",
      "backgroundColor" => "#BDB76B",
      ],
      [
      "id"=>"G",
      "name"=>"USA Canadá Estandard",
      "backgroundColor" => "#9370DB",
      ],
      [
      "id"=>"B",
      "name"=>"Baja Temporal",
      "backgroundColor" => "#7B68EE ",
      ],[
      "id"=>"E",
      "name"=>"Carga Express",
      "backgroundColor" => "#34495e",
      ]

      ];

      try {

      $res = factGuias::getRecoleccionTR1($fecha_filtro,$plaza_filtro,$garantia_filtro,$cliente_filtro,$guia_filtro);


      //die(json_encode($res));


      $grouped = null;
      $dias =[];
      $labels = [];


      foreach ($res as $guia){

      if(!is_null($guia->garantia_usar)) {
      $dias[$guia->dia][] =$guia;
      $labels[] = $guia->dia;
      }

      }

      $datasets =[];
      $baseItem =null;

      foreach ($dias as $index => $entregas) {

      foreach ($entregas as $idxd => $garantias_) {
      foreach ($lista_garantias as $idx => $garantia) {
      // $datasets[$idx] = $baseItem;
      $datasets[$idx]["backgroundColor"] = $garantia["backgroundColor"];
      $datasets[$idx]["id"] = $garantia["id"];
      $datasets[$idx]["label"] = $garantia["name"];
      $datasets[$idx]["stack"] ='Stack 1';

      if((int)$garantias_->garantia_usar === (int) $datasets[$idx]["id"]) {
      $datasets[$idx]["data"][] = (int)$garantias_->cantguias;
      // $datasets[$idx]["d"][] = $garantias_;
      } else{
      $datasets[$idx]["data"][] = 0;
      }
      }
      }

      }

      return $datasets;

      $grouped =  $datasets;

      //die(json_encode(array("datasets"=>$grouped,"labels"=>$labels)));
      $data=["datasets"=>$grouped,"labels"=>$labels];

      }catch (QueryException $e){
      $this->container->logger->error("Falló el registro".$e->getMessage());
      $errors[] = "Ha ocurrido un error interno";
      }catch (\Exception $e){
      $this->container->logger->error("Falló el registro".$e->getMessage());
      $errors[] = "Ha ocurrido un error interno";
      }



      } */



    /* Grafica principal */
    /*
     * Estatus:Estable
     * RESPALDO; BUG EN UN ITEM CON IDGARANTIA A
     * */

    public function diasssx3($request, $response, $args) {
        $this->container->logger->info("/API/days");
        $data = ($request->getParsedBody());
        //die(json_encode($data));
        $fecha_filtro = !isset($data['fecha_filtro']) ? new \DateTime() : $data['fecha_filtro'];
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $garantias_filtro = !empty($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $cliente_filtro = !isset($data['cliente_filtro']) ? null : $data['cliente_filtro'];
        $guia_filtro = !isset($data['guia_filtro']) ? null : $data['guia_filtro'];

        $res = factGuias::getEntregasTR1($fecha_filtro, $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro);
//die(json_encode($res));

        $resReco = factGuias::getRecoleccionTR1($fecha_filtro, $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro);



        //die(json_encode($resReco));

        $grouped = null;
        //Servira para almacenar los dias y por cada día, la lista de garantías
        $dias_agrupadas_por_dia = []; //Contiene los items agrupados por día, cada item tiene la garantía a usar
        $dias_agrupadas_por_garantia = [];
        $idsgarantias = [];
        $diasRec = [];
        $labels = [];
        $datasets = [];
        $datasets1 = [];




        //Sacar la lista todal de garantias
        foreach ($res as $guia) {
            if (!is_null($guia->garantia_usar)) {
                //if(!in_array($guia->garantia_usar,$idsgarantias)){
                $idsgarantias[$guia->dia][] = $guia;
                //}
            }
        }

        //die(json_encode($idsgarantias));
        //Agrupa los valores por día
        /* foreach ($res as $guia){
          foreach ($idsgarantias as $idgarantia){}
          //die(json_encode($guia));
          //Por cada día, las garantías pertenecientes
          if(!is_null($guia->garantia_usar)) {
          $dias_agrupadas_por_dia[$guia->dia][] =$guia;
          if(!in_array($guia->dia,$labels)){
          $labels[] = $guia->dia;
          }
          }

          } */



        //die(json_encode($dias_agrupadas_por_dia));
        //Agrupa los valores por garantias
        /* foreach ($res as $guia){
          if(!is_null($guia->garantia_usar)) {
          $dias_agrupadas_por_garantia[$guia->garantia_usar][] =$guia;
          }

          } */
        //die(json_encode($dias_agrupadas_por_garantia));


        $baseItem = null;
        $contDia = 0;
        //Para tomar las garantías por idgarantía, se debera tener
        //un getbyid que devuelva el nombre y el color de la garantia para asignarlo al item

        /* for($i=0;$i<count($dias_agrupadas_por_dia);$i++){
          die($dias_agrupadas_por_dia['']);
          } */

        foreach ($idsgarantias as $index => $days) {
            $labels[] = $index;


            foreach ($days as $idxd => $garantias_d) {
                //die(json_encode($garantias_d));
                //if ( $garantias_d->garantia_usar ==  $datasets[$idxd]["idgarantia"] ){
                $datasets[$idxd]["stack"] = 'Stack 0';
                $datasets[$idxd]["idgarantia"] = $garantias_d->garantia_usar;
                $datasets[$idxd]["label"] = self::findObjectField($this->_LISTA_GARANTIAS, 'id', $garantias_d->garantia_usar)['name'];
                $datasets[$idxd]["backgroundColor"] = self::findObjectField($this->_LISTA_GARANTIAS, 'id', $garantias_d->garantia_usar)['backgroundColor'];
                $datasets[$idxd]["data"][] = (int) $garantias_d->cantguias;
                $datasets[$idxd]["d"][] = $garantias_d;
                $datasets[$idxd]["objs"][] = $days;
                /* if ((int)$garantias_d->cantguias===205){
                  $datasets[$idxd]["backgroundColor"]="#FF0000";
                  $datasets[$idxd]["label"]="Terrestre";
                  echo json_encode($datasets[$idxd])."\n";
                  //die(json_encode($datasets[$idxd]));
                  } */
                /* if ($garantias_d->dia =="D+3" && $garantias_d->garantia_usar=='7'){
                  $datasets[$idxd]["backgroundColor"]="#FF0000";
                  $datasets[$idxd]["label"]="Terrestre";
                  //die(json_encode($datasets[$idxd]));
                  } */
                /* if (count($days) == $idxd+1)
                  break; */
                //}
            }
        }

        //die(json_encode($datasets));
        //recolecciones
        foreach ($resReco as $guia) {

            if (!is_null($guia->garantia_usar)) {
                $diasRec[$guia->dia][] = $guia;
                if (!in_array($guia->dia, $labels)) {
                    $labels[] = $guia->dia;
                }
            }
        }


        foreach ($diasRec as $index => $days) {
            $labels[] = $index;
            foreach ($days as $idxd => $garantias_d) {
                //die(json_encode($garantias_d));
                $datasets1[$idxd]["stack"] = 'Stack 1';
                $datasets1[$idxd]["idgarantia"] = $garantias_d->garantia_usar;
                $datasets1[$idxd]["label"] = self::findObjectField($this->_LISTA_GARANTIAS, 'id', $garantias_d->garantia_usar)['name'];
                $datasets1[$idxd]["backgroundColor"] = self::findObjectField($this->_LISTA_GARANTIAS, 'id', $garantias_d->garantia_usar)['backgroundColor'];
                $datasets1[$idxd]["data"][] = (int) $garantias_d->cantguias;
                $datasets1[$idxd]["d"][] = $garantias_d;
                $datasets1[$idxd]["objs"][] = $days;
            }
        }

        //die(json_encode([$datasets,$datasets1]));

        /* die(json_encode($datasets1));
          $labels = array_unique($labels); */
        //$datasets = array_merge($datasets,$datasets1);

        foreach ($datasets1 as $ds) {
            $datasets[] = $ds;
        }



        $grouped = $datasets;

        die(json_encode(array("datasets" => $grouped, "labels" => $labels)));
    }

    /*
     * ESTATUS:FUNCIONAL 100%
     * */

    public function dias($request, $response, $args) {
        $this->container->logger->info("/API/ordes");
        $data = ($request->getParsedBody());
        //die(json_encode($data));

        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"/* Morado */
            ],
                [
                "id" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"/* Morado */
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "L",
                "name" => "LTL Guia Embarque",
                "backgroundColor" => "#a6c16f",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];
        //$plaza_filtro = $data['plaza_filtro'] ? $data['plaza_filtro']: 0 ;
        $fecha_filtro = !isset($data['fecha_filtro']) ? new \DateTime() : $data['fecha_filtro'];
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $garantias_filtro = !empty($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $cliente_filtro = !isset($data['cliente_filtro']) ? null : $data['cliente_filtro'];
        $guia_filtro = !isset($data['guia_filtro']) ? null : $data['guia_filtro'];
        $fecha_entrega = !isset($data['fecha_entrega']) ? null : $data['fecha_entrega'];
        $codigo_postalNull = !isset($data['codigo_postal']) ? null : $data['codigo_postal'];

        $plazas_agrup = factGuias::getPlazasAgrupadas($plazas_filtro);
        $agrupadas = [];
        $bandera = 0;
        foreach ($plazas_agrup as $grup) {
            if (!is_null($grup->sector)) {
                $agrupadas[$bandera] = $grup->sector;
                $bandera ++;
            }
        }
        //Start: Manual insertion to the array 
        foreach ($plazas_filtro as $item) {
            array_push($agrupadas, $item);
        }

        $agrupadas = array_unique($agrupadas);
        //End: Manual insertion to the array

        $res = factGuias::getEntregasTR1($fecha_filtro, $agrupadas, $garantias_filtro, $cliente_filtro, $guia_filtro, $fecha_entrega, $codigo_postalNull);
//die(json_encode($res));

        $resReco = factGuias::getRecoleccionTR1($fecha_filtro, $agrupadas, $garantias_filtro, $cliente_filtro, $guia_filtro);
        $resReco = [];
        //die(json_encode($resReco));
        //INICIO DE LA CONFIGURACION
        $labels = [];
        $plazas_agrupadas = [];
        $codigospostales1 = [];
        foreach ($res as $guia) {
            if (!is_null($guia->garantia_usar)) {
                $plazas_agrupadas[$guia->dia][] = $guia;

                if (!in_array($guia->dia, $labels)) {
                    $labels[] = $guia->dia;
                    $labels = array_values(array_unique($labels));
                }
            }
        }
//        foreach ($resReco as $guia) {
//
//            if (!is_null($guia->garantia_usar)) {
//                $codigospostales1[$guia->dia][] = $guia;
//                if (!in_array($guia->dia, $labels)) {
//                    $labels = array_values(array_unique($labels));
//                    $labels[] = $guia->dia;
//                }
//            }
//        }
        //die(json_encode($plazas_agrupadas));
        $datasets = [];
        $datasets1 = [];

        foreach ($lista_garantias as $idx => $garantia) {
            $datasets[$idx]["backgroundColor"] = $garantia["backgroundColor"];
            $datasets[$idx]["idx"] = $garantia["id"];
            $datasets[$idx]["stack"] = 'Stack 0';
            $datasets[$idx]["label"] = $garantia["name"];


            foreach ($plazas_agrupadas as $name_plaza => $plaza) {
                if (!in_array($name_plaza, $labels)) {
                    $labels[] = $name_plaza;
                    array_values(array_unique($labels));
                }
                foreach ($plaza as $idxp => $garantias_plaza) {
                    if ($garantias_plaza->garantia_usar == $datasets[$idx]["idx"] &&
                            $garantias_plaza->garantia_usar == $garantia['id']
                    ) {
                        $datasets[$idx]["data"][self::getLabelIndex($labels, $garantias_plaza->dia)] = (int) $garantias_plaza->cantguias;

                        $garantias_plaza->position_need = self::getLabelIndex($labels, $garantias_plaza->dia);
                        $datasets[$idx]["d"][] = $garantias_plaza;
                    } else {
                        //  $datasets[$idx]["data"][$idxp] = 0;
                    }
                }
            }
        }


        foreach ($datasets as $idx => $dataset) {
            foreach ($labels as $index => $label) {
                if (!isset($datasets[$idx]["data"][$index])) {
                    $datasets[$idx]["data"][$index] = 0;
                }
            }
        }

        //convertir a arreglo todo
        foreach ($datasets as $idx => $dataset) {
            $tempV = $datasets[$idx]["data"];
            $datasets[$idx]["data"] = [];
            for ($e = 0; $e < count($labels); $e++) {
                $datasets[$idx]["data"][] = $tempV[$e];
            }
        }




        //stack 1

        foreach ($lista_garantias as $idx => $garantia) {
            $datasets1[$idx]["backgroundColor"] = $garantia["backgroundColor"];
            $datasets1[$idx]["idx"] = $garantia["id"];
            $datasets1[$idx]["stack"] = 'Stack 1';
            $datasets1[$idx]["label"] = $garantia["name"];
            foreach ($codigospostales1 as $name_plaza => $plaza) {
                if (!in_array($name_plaza, $labels)) {
                    $labels[] = $name_plaza;
                    array_values(array_unique($labels));
                }
                foreach ($plaza as $idxp => $garantias_plaza) {
                    if ($garantias_plaza->garantia_usar == $datasets1[$idx]["idx"] &&
                            $garantias_plaza->garantia_usar == $garantia['id']
                    ) {
                        $datasets1[$idx]["data"][self::getLabelIndex($labels, $garantias_plaza->dia)] = (int) $garantias_plaza->cantguias;


                        $garantias_plaza->position_need = self::getLabelIndex($labels, $garantias_plaza->dia);
                        $datasets1[$idx]["d"][] = $garantias_plaza;
                    } else {
                        //  $datasets[$idx]["data"][$idxp] = 0;
                    }
                }
            }
        }

//        foreach ($datasets1 as $idx => $dataset) {
//            foreach ($labels as $index => $label) {
//                if (!isset($datasets1[$idx]["data"][$index])) {
//                    $datasets1[$idx]["data"][$index] = 0;
//                    //$datasets1[$idx]["data"] =array_values($datasets1[$idx]["data"]);
//                }
//            }
//        }
        //convertir a arreglo todo
//        foreach ($datasets1 as $idx => $dataset) {
//            $tempV = $datasets1[$idx]["data"];
//            $datasets1[$idx]["data"] = [];
//            for ($e = 0; $e < count($labels); $e++) {
//                $datasets1[$idx]["data"][] = $tempV[$e];
//            }
//        }
        //die(json_encode($datasets1));
        //$labels = array_values(array_unique($labels));
        //FIN DE LA  CONFIGURACION
        //$labels = array_values(array_unique($labels));
//        $datasets = array_merge($datasets, $datasets1);




        $grouped = $datasets;

        die(json_encode(array("datasets" => $grouped, "labels" => $labels, "tamano" => count($labels))));
    }

    /* Grafica TR1 */

    /*
     * ESTATUS: CORREGIDO; ESTABLE
     * */

    public function origenAnteriorDestinoSigiente($request, $response, $args) {
        $this->container->logger->info("/API/ordes");
        $data = ($request->getParsedBody());
        //die(json_encode($data));

        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "L",
                "name" => "LTL Guia Embarque",
                "backgroundColor" => "#a6c16f",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];
        //$plaza_filtro = $data['plaza_filtro'] ? $data['plaza_filtro']: 0 ;
        $fecha_filtro = !isset($data['fecha_filtro']) ? new \DateTime() : $data['fecha_filtro'];
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $garantias_filtro = !empty($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $cliente_filtro = !isset($data['cliente_filtro']) ? null : $data['cliente_filtro'];
        $guia_filtro = !isset($data['guia_filtro']) ? null : $data['guia_filtro'];
        $dia_seleccionado = !isset($data['dia']) ? 0 : $data['dia'];

        //die($plaza_filtro);
        //die(json_encode(factGuias::getInfoEntregasByCodigoPostal($plaza_filtro ,$fecha_filtro,$cliente_filtro ,$guia_filtro,$garantia_filtro)));

        $res = factGuias::getOrigenAnterior($fecha_filtro, $plazas_filtro, $garantias_filtro, $cliente_filtro, $guia_filtro, $dia_seleccionado);



        $resReco = []; //= factGuias::getDestinoSiguiente($fecha_filtro,$plazas_filtro,$garantias_filtro,$cliente_filtro,$guia_filtro,$dia_seleccionado);
        //die(json_encode($resReco));
        //INICIO DE LA CONFIGURACION

        /* $res ='[
          {
          "origen_anterior": "Plaza >=2",
          "garantia_usar": 7,
          "cantguias": 1364
          },
          {
          "origen_anterior": "CWM",
          "garantia_usar": "L",
          "cantguias": 18
          },
          {
          "origen_anterior": "CWG",
          "garantia_usar": "L",
          "cantguias": 5
          },
          {
          "origen_anterior": "CWM",
          "garantia_usar": 7,
          "cantguias": 2
          },


          {
          "origen_anterior": "Plaza >=2",
          "garantia_usar": "L",
          "cantguias": 31
          }
          ]';

          $resReco= '[
          {
          "destino_siguiente": "GDL",
          "garantia_usar": "L",
          "cantguias": 554
          },
          {
          "destino_siguiente": "MEX",
          "garantia_usar": 7,
          "cantguias": 900
          },
          {
          "destino_siguiente": "MEX",
          "garantia_usar": "L",
          "cantguias": 800
          },
          {
          "destino_siguiente": "Plaza >=2",
          "garantia_usar": 7,
          "cantguias": 3627
          },
          {
          "destino_siguiente": "Plaza >=2",
          "garantia_usar": "L",
          "cantguias": 465
          }
          ]';
          $resReco = json_decode($resReco);
          $res = json_decode($res); */


        /*
         * Para cada tipo de garantia que se representará con barras de colores apiladas,
         *  Se debe tener la lista de plazas con sus diferentes tipos de garantías
         *
         * Para facilitar el proceso primero, las plazas deben ser agrupadas por id de garantia
         *
         * for each $lista_garantias as $i => $ItemGarantia{
         *      for each $listaPlazas as plaza{
         *          si plaza.idgarantia == $ItemGarantia.idgarantia{
         *              $dataset[$i]=[
         *                  'idgarantia'=>plaza.idgarantia,
         *                  'nombre'=>plaza.idgarantia,
         *                  'data'[]=>plaza.cantgarantia,
         *              ]else{
         *                  $dataset[$i]['data']=0;
         *              }
         *          }
         *      }
         * }
         *      si $ItemGarantia
         *
         *
         *
         * */
        $labels = [];
        $plazas_agrupadas = [];
        $codigospostales1 = [];
        foreach ($res as $guia) {
            if (!is_null($guia->garantia_usar)) {
                $plazas_agrupadas[$guia->origen_anterior][] = $guia;

                if (!in_array($guia->origen_anterior, $labels)) {
                    $labels[] = $guia->origen_anterior;
                    $labels = array_values(array_unique($labels));
                }
            }
        }
        foreach ($resReco as $guia) {

            if (!is_null($guia->garantia_usar)) {
                $codigospostales1[$guia->destino_siguiente][] = $guia;
                if (!in_array($guia->destino_siguiente, $labels)) {
                    $labels = array_values(array_unique($labels));
                    $labels[] = $guia->destino_siguiente;
                }
            }
        }

        //die(json_encode($plazas_agrupadas));
        $datasets = [];
        $datasets1 = [];

        foreach ($lista_garantias as $idx => $garantia) {
            $datasets[$idx]["backgroundColor"] = $garantia["backgroundColor"];
            $datasets[$idx]["idx"] = $garantia["id"];
            $datasets[$idx]["stack"] = 'Stack 0';
            $datasets[$idx]["label"] = $garantia["name"];


            foreach ($plazas_agrupadas as $name_plaza => $plaza) {
                if (!in_array($name_plaza, $labels)) {
                    $labels[] = $name_plaza;
                    array_values(array_unique($labels));
                }
                foreach ($plaza as $idxp => $garantias_plaza) {
                    if ($garantias_plaza->garantia_usar == $datasets[$idx]["idx"] &&
                            $garantias_plaza->garantia_usar == $garantia['id']
                    ) {
                        $datasets[$idx]["data"][self::getLabelIndex($labels, $garantias_plaza->origen_anterior)] = (int) $garantias_plaza->cantguias;

                        $garantias_plaza->position_need = self::getLabelIndex($labels, $garantias_plaza->origen_anterior);
                        $datasets[$idx]["d"][] = $garantias_plaza;
                    } else {
                        //  $datasets[$idx]["data"][$idxp] = 0;
                    }
                }
            }
        }


        foreach ($datasets as $idx => $dataset) {
            foreach ($labels as $index => $label) {
                if (!isset($datasets[$idx]["data"][$index])) {
                    $datasets[$idx]["data"][$index] = 0;
                }
            }
        }

        //convertir a arreglo todo
        foreach ($datasets as $idx => $dataset) {
            $tempV = $datasets[$idx]["data"];
            $datasets[$idx]["data"] = [];
            for ($e = 0; $e < count($labels); $e++) {
                $datasets[$idx]["data"][] = $tempV[$e];
            }
        }




        //stack 1

        foreach ($lista_garantias as $idx => $garantia) {
            $datasets1[$idx]["backgroundColor"] = $garantia["backgroundColor"];
            $datasets1[$idx]["idx"] = $garantia["id"];
            $datasets1[$idx]["stack"] = 'Stack 1';
            $datasets1[$idx]["label"] = $garantia["name"];
            foreach ($codigospostales1 as $name_plaza => $plaza) {
                if (!in_array($name_plaza, $labels)) {
                    $labels[] = $name_plaza;
                    array_values(array_unique($labels));
                }
                foreach ($plaza as $idxp => $garantias_plaza) {
                    if ($garantias_plaza->garantia_usar == $datasets1[$idx]["idx"] &&
                            $garantias_plaza->garantia_usar == $garantia['id']
                    ) {
                        $datasets1[$idx]["data"][self::getLabelIndex($labels, $garantias_plaza->destino_siguiente)] = (int) $garantias_plaza->cantguias;


                        $garantias_plaza->position_need = self::getLabelIndex($labels, $garantias_plaza->destino_siguiente);
                        $datasets1[$idx]["d"][] = $garantias_plaza;
                    } else {
                        //  $datasets[$idx]["data"][$idxp] = 0;
                    }
                }
            }
        }

        foreach ($datasets1 as $idx => $dataset) {
            foreach ($labels as $index => $label) {
                if (!isset($datasets1[$idx]["data"][$index])) {
                    $datasets1[$idx]["data"][$index] = 0;
                    //$datasets1[$idx]["data"] =array_values($datasets1[$idx]["data"]);
                }
            }
        }

        //convertir a arreglo todo
        foreach ($datasets1 as $idx => $dataset) {
            $tempV = $datasets1[$idx]["data"];
            $datasets1[$idx]["data"] = [];
            for ($e = 0; $e < count($labels); $e++) {
                $datasets1[$idx]["data"][] = $tempV[$e];
            }
        }




        //die(json_encode($datasets1));
        //$labels = array_values(array_unique($labels));
        //FIN DE LA  CONFIGURACION
        //$labels = array_values(array_unique($labels));
        //$datasets = array_merge($datasets,$datasets1);




        $grouped = $datasets;

        die(json_encode(array("datasets" => $grouped, "labels" => $labels, "tamano" => count($labels))));
    }

//Detalle de guias por CODIGO POSTAL
    /*
     * Estatus:Estable
     * Detalles: Cuando se filtra co la sigla MEX no se encuentra en la tabla dimFrecuenciasMP
     */
    public function codigoPostal($request, $response, $args) {
        $this->container->logger->info("/API/cp");
        $data = ($request->getParsedBody());
        //die(json_encode($data));
        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "L",
                "name" => "LTL Guia Embarque",
                "backgroundColor" => "#a6c16f",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];
        $fecha_filtro = $data['fecha_filtro'] ? $data['fecha_filtro'] : null;
        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0;
        $guia_filtro = $data['guia_filtro'] ? $data['guia_filtro'] : 0;
        $dia_filtro = $data['dia'] ? ((int) $data['dia'] - 1) : 0;
        $dia_filtro = $data['dia'] == 0 ? -30 : $dia_filtro;
        $fecha_entrega = !isset($data['fecha_entrega']) ? null : $data['fecha_entrega'];
        $codigo_postalNull = !isset($data['codigo_postal']) ? null : $data['codigo_postal'];
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $garantias_filtro = !empty($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $plazas_agrup = factGuias::getPlazasAgrupadas($plazas_filtro);
        $agrupadas = [];
        $bandera = 0;
        foreach ($plazas_agrup as $grup) {
            if (!is_null($grup->sector)) {
                $agrupadas[$bandera] = $grup->sector;
                $bandera ++;
            }
        }
        //Start: Manual insertion to the array 
        foreach ($plazas_filtro as $item) {
            array_push($agrupadas, $item);
        }
        $agrupadas = array_unique($agrupadas);
        //End: Manual insertion to the array
        /*
         * STATUS: Estable
         * TODO:
         * Si se filtra por plaza 'MEX', no dará resultados pue sla tabla de origen bq caFrecuenciasMP no tiene ese registro solo caFrecuenciasLTL
         */
        $res = factGuias::getInfoEntregasByCodigoPostal($agrupadas, $fecha_filtro, $cliente_filtro, $guia_filtro, $garantias_filtro, $dia_filtro, $fecha_entrega, $codigo_postalNull);
        //die(json_encode($res));
        /*
         * Estatus:Estable
         */
        $resReco = []; // factGuias::getInfoRecoleccionesByCodigoPostal($plazas_filtro ,$fecha_filtro,$cliente_filtro ,$guia_filtro,$garantias_filtro,$dia_filtro);
        $grouped = null;
        $codigospostales = [];
        $codigospostales1 = [];
        $labels = [];
        $datasets = [];
        $datasets1 = [];
        foreach ($res as $guia) {
            if (!is_null($guia->garantia_usar)) {
                $codigospostales[$guia->codigopostal][] = $guia;
                if (!in_array($guia->codigopostal, $labels)) {
                    $labels[] = $guia->codigopostal;
                }
            }
        }
        $baseItem = null;
        foreach ($codigospostales as $index => $cp) {
            $labels[] = $index;
            //base del item
            $baseItem = [
                "label" => 'Internacional',
                //"backgroundColor" => "#" . $this->random_color(),
                "stack" => 'Stack 0',
                "data" => [
                ]
            ];
            foreach ($cp as $idxd => $garantias_cp) {
                foreach ($lista_garantias as $idx => $garantia) {
                    //$datasets[$idx] = $baseItem;
                    $datasets[$idx]["backgroundColor"] = $garantia["backgroundColor"];
                    $datasets[$idx]["idx"] = $garantia["id"];
                    $datasets[$idx]["stack"] = 'Stack 0';
                    $datasets[$idx]["label"] = $garantia["name"];
                    if (is_numeric($garantias_cp->garantia_usar)) {
                        if ((int) $garantias_cp->garantia_usar === (int) $datasets[$idx]["idx"]) {
                            $datasets[$idx]["data"][self::getLabelIndex($labels, $garantias_cp->codigopostal)] = (int) $garantias_cp->cantguias;
                            $datasets[$idx]["d"][] = $garantias_cp;
                        } else {
                            $datasets[$idx]["data"][] = 0;
                        }
                    } else {
                        if ($garantias_cp->garantia_usar == $datasets[$idx]["idx"]) {
                            $datasets[$idx]["data"][self::getLabelIndex($labels, $garantias_cp->codigopostal)] = (int) $garantias_cp->cantguias;
                            $datasets[$idx]["d"][] = $garantias_cp;
                        } else {
                            $datasets[$idx]["data"][] = 0;
                        }
                    }
                }
            }
        }
//        //recolecciones
//        foreach ($resReco as $guia) {
//            if (!is_null($guia->garantia_usar)) {
//                $codigospostales1[$guia->codigopostal][] = $guia;
//                if (!in_array($guia->codigopostal, $labels)) {
//                    $labels[] = $guia->codigopostal;
//                }
//            }
//        }
//        foreach ($codigospostales1 as $index => $cp) {
//            $labels[] = $index;
//            //base del item
//            $baseItem = [
//                "label" => 'Internacional',
//                "backgroundColor" => "#" . $this->random_color(),
//                "stack" => 'Stack 1',
//                "data" => [
//                ]
//            ];
//            foreach ($cp as $idxd => $garantias_cp) {
//                foreach ($lista_garantias as $idx => $garantia) {
//                    $datasets1[$idx]["backgroundColor"] = $garantia["backgroundColor"];
//                    $datasets1[$idx]["idx"] = $garantia["id"];
//                    $datasets1[$idx]["stack"] = 'Stack 1';
//                    $datasets1[$idx]["label"] = $garantia["name"];
//                    if ((int) $garantias_cp->garantia_usar === (int) $datasets1[$idx]["idx"]) {
//                        $datasets1[$idx]["data"][] = (int) $garantias_cp->cantguias;
//                    } else {
//                        $datasets1[$idx]["data"][] = 0;
//                    }
//                }
//            }
//        }
        $labels = array_values(array_unique($labels));
//        $datasets = /* $datasets1;// */array_merge($datasets, $datasets1);
        $grouped = $datasets;
        $idVacio = array_search(null, $labels);
        if (!empty($idVacio)) {
            $labels[$idVacio] = "CP NULO";
        }
        die(json_encode(array("datasets" => $grouped, "labels" => $labels, "tamano" => count($labels))));
    }

//testFactGuias RUTA TR2
    /*
     * ESTATUS:ESTABLE
     * */
    public function rutaTr2($request, $response, $args) {
        $this->container->logger->info("/API/tr2");
        $data = ($request->getParsedBody());

        $plazas_filtro = isset($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $fecha_filtro = $data['fecha_filtro'] ? $data['fecha_filtro'] : null;
        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0;
        $guia_filtro = $data['guia_filtro'] ? $data['guia_filtro'] : 0;
        $garantias_filtro = isset($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $dia_filtro = $data['dia'] ? ((int) $data['dia'] - 1) : 0;
        $dia_filtro = $data['dia'] == 0 ? -30 : $dia_filtro;

        $fecha_entrega = !isset($data['fecha_entrega']) ? null : $data['fecha_entrega'];
        $codigo_postalNull = !isset($data['codigo_postal']) ? null : $data['codigo_postal'];

        $plazas_agrup = factGuias::getPlazasAgrupadas($plazas_filtro);
        $agrupadas = [];
        $bandera = 0;
        foreach ($plazas_agrup as $grup) {
            if (!is_null($grup->sector)) {
                $agrupadas[$bandera] = $grup->sector;
                $bandera ++;
            }
        }
        //Start: Manual insertion to the array 
        foreach ($plazas_filtro as $item) {
            array_push($agrupadas, $item);
        }
        $agrupadas = array_unique($agrupadas);
        //End: Manual insertion to the array 

        $res = factGuias::getInfoEntregasByTr2($agrupadas, $fecha_filtro, $cliente_filtro, $guia_filtro, $garantias_filtro, $dia_filtro, $fecha_entrega, $codigo_postalNull);

        die(json_encode(array("data" => $res)));
    }

    /*
     * ESTATUS:ESTABLE
     * */

//    public function detalleGuias( $request,  $response,$args){
//
//        $this->container->logger->info("/API/detalle/guias");
//        $data = ($request->getParsedBody());
//
//
//        $lista_garantias = [
//            [
//                "id"=>"A",
//                "id_alter"=>"3",
//                "name"=>"9:30",
//                "backgroundColor" =>"#8d83cf"
//            ],
//            [
//                "id"=>"5",
//                "name"=>"11:30",
//                "backgroundColor" =>"#e8a97d",
//            ],
//            [
//                "id"=>"6",
//                "name"=>"Dia Siguiente",
//                "backgroundColor" => "#aeaca4",
//            ],
//            [
//                "id"=>"T",
//                "name"=>"Dia Siguiente Terrestre",
//                "backgroundColor" => "#ddda69",
//            ],
//            [
//                "id"=>"D",
//                "name"=>"2 Dias",
//                "backgroundColor" => "#9dcfa5",
//            ],
//            [
//                "id"=>"7",
//                "name"=>"Terrestre",
//                "backgroundColor" => "#91aec4",
//            ],
//            [
//                "id"=>"F",
//                "name"=>"Paquetería Aérea",
//                "backgroundColor" => "#ffdbad",
//            ]
//        ];
//
//
//        /*
//
//          select
//        --COALESCE(vwdestino_codigopostal, vwopenvio_codigopos) as codigopostal,
//        TRIM( COALESCE( vwopenvio_codigopos,vwdestino_codigopostal))
//        || case when tr2cp.sob_ruta = tr2cp.paq_ruta then ' | ' || TRIM(tr2cp.sob_ruta)
//           else ' | ' ||  TRIM(tr2cp.sob_ruta)  || ' | ' ||  TRIM(tr2cp.paq_ruta)
//           end
//          as codigopostal,
//
//
//        */
//
//        //$codigopostal = $data['codigopostal'] ? substr($data['codigopostal'],0,5): 0 ;
//        $codigopostal = $data['codigopostal'] ? $data['codigopostal']: 0 ;
//        $plazas_filtro = isset($data['plazas_filtro']) ? $data['plazas_filtro']: [] ;
//        $fecha_filtro = $data['fecha_filtro'] ? $data['fecha_filtro']  : null  ;
//        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0 ;
//        $guia_filtro= $data['guia_filtro'] ? $data['guia_filtro'] : 0 ;
//        $garantias_filtro= isset($data['garantias_filtro']) ?  $data['garantias_filtro'] : [] ;
//        $dia_filtro= $data['dia'] ? $data['dia'] : 0 ;
//
//        //die(json_encode($plazas_filtro));
//        $detalle =  factGuias::getDetalleGuias($plazas_filtro ,$fecha_filtro,$cliente_filtro ,$guia_filtro,$garantias_filtro,$dia_filtro,$codigopostal);
//
//
//        /*$banderas=[];
//        $cp=[];
//        foreach ($detalle as $d){
//            if (!in_array($d->vwguias_numeroguia,$banderas)){
//                $banderas[]=$d->vwguias_numeroguia;
//                $cp[]=$d;
//            }
//        }*/
//        //verificar las garantias permitidas
//
//        die(json_encode($detalle));
//    }
    public function detalleGuias($request, $response, $args) {
        $this->container->logger->info("/API/detalle/guias");
        $data = ($request->getParsedBody());
        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "L",
                "name" => "LTL Guia Embarque",
                "backgroundColor" => "#a6c16f",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];
        /*
          select
          --COALESCE(vwdestino_codigopostal, vwopenvio_codigopos) as codigopostal,
          TRIM( COALESCE( vwopenvio_codigopos,vwdestino_codigopostal))
          || case when tr2cp.sob_ruta = tr2cp.paq_ruta then ' | ' || TRIM(tr2cp.sob_ruta)
          else ' | ' ||  TRIM(tr2cp.sob_ruta)  || ' | ' ||  TRIM(tr2cp.paq_ruta)
          end
          as codigopostal,
         */
        //$codigopostal = $data['codigopostal'] ? substr($data['codigopostal'],0,5): 0 ;
        $codigopostal = $data['codigopostal'] ? $data['codigopostal'] : 0;
        $plazas_filtro = isset($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $fecha_filtro = $data['fecha_filtro'] ? $data['fecha_filtro'] : null;
        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0;
        $guia_filtro = $data['guia_filtro'] ? $data['guia_filtro'] : 0;
        $garantias_filtro = isset($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        //$dia_filtro = $data['dia'] ? $data['dia'] : 0;
        $dia_filtro = !isset($data['dia']) ? 0 : ((int) $data['dia'] - 1);
        $dia_filtro = $data['dia'] == 0 ? -30 : $dia_filtro;
        $fecha_entrega = !isset($data['fecha_entrega']) ? null : $data['fecha_entrega'];
        $codigo_postalNull = !isset($data['codigo_postal']) ? null : $data['codigo_postal'];
        $plazas_agrup = factGuias::getPlazasAgrupadas($plazas_filtro);
        $agrupadas = [];
        $bandera = 0;
        foreach ($plazas_agrup as $grup) {
            if (!is_null($grup->sector)) {
                $agrupadas[$bandera] = $grup->sector;
                $bandera ++;
            }
        }
        //Start: Manual insertion to the array 
        foreach ($plazas_filtro as $item) {
            array_push($agrupadas, $item);
        }
        $agrupadas = array_unique($agrupadas);
        //End: Manual insertion to the array
        //die(json_encode($plazas_filtro));
        $detalle = factGuias::getDetalleGuias($agrupadas, $fecha_filtro, $cliente_filtro, $guia_filtro, $garantias_filtro, $dia_filtro, $codigopostal, $fecha_entrega, $codigo_postalNull);
        /* $banderas=[];
          $cp=[];
          foreach ($detalle as $d){
          if (!in_array($d->vwguias_numeroguia,$banderas)){
          $banderas[]=$d->vwguias_numeroguia;
          $cp[]=$d;
          }
          } */
        //verificar las garantias permitidas
        die(json_encode($detalle));
    }

    /*
     * ESTATUS:ESTABLE
     * */

    public function detalleGuiasTr2($request, $response, $args) {

        $this->container->logger->info("/API/detalle/guias/tr2");
        $data = ($request->getParsedBody());


        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];

        $ruta = $data['ruta'] ? $data['ruta'] : 0;
        $plazas_filtro = isset($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $fecha_filtro = isset($data['fecha_filtro']) ? $data['fecha_filtro'] : null;
        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0;
        $guia_filtro = $data['guia_filtro'] ? $data['guia_filtro'] : 0;
        $garantias_filtro = isset($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $dia_filtro = $data['dia'] ? ((int) $data['dia'] - 1) : 0;
        $dia_filtro = $data['dia'] == 0 ? -30 : $dia_filtro;

        $fecha_entrega = !isset($data['fecha_entrega']) ? null : $data['fecha_entrega'];
        $codigo_postalNull = !isset($data['codigo_postal']) ? null : $data['codigo_postal'];

        $plazas_agrup = factGuias::getPlazasAgrupadas($plazas_filtro);
        $agrupadas = [];
        $bandera = 0;
        foreach ($plazas_agrup as $grup) {
            if (!is_null($grup->sector)) {
                $agrupadas[$bandera] = $grup->sector;
                $bandera ++;
            }
        }
        //Start: Manual insertion to the array 
        foreach ($plazas_filtro as $item) {
            array_push($agrupadas, $item);
        }
        $agrupadas = array_unique($agrupadas);
        //End: Manual insertion to the array 

        $detalle = factGuias::getDetalleGuiasTr2($agrupadas, $fecha_filtro, $cliente_filtro, $guia_filtro, $garantias_filtro, $dia_filtro, $ruta, $fecha_entrega, $codigo_postalNull);


        die(json_encode($detalle));
    }

    /*
     * ESTATUS: TERMINADO SIN PROBAR
     * Falta ajustar la optimizacion que Leo pasó
     * */

    public function detalleGuiasTr1($request, $response, $args) {

        $this->container->logger->info("/API/detalle/guias/tr2");
        $data = ($request->getParsedBody());


        $lista_garantias = [
                [
                "id" => "A",
                "id_alter" => "3",
                "name" => "9:30",
                "backgroundColor" => "#8d83cf"
            ],
                [
                "id" => "5",
                "name" => "11:30",
                "backgroundColor" => "#e8a97d",
            ],
                [
                "id" => "6",
                "name" => "Dia Siguiente",
                "backgroundColor" => "#aeaca4",
            ],
                [
                "id" => "T",
                "name" => "Dia Siguiente Terrestre",
                "backgroundColor" => "#ddda69",
            ],
                [
                "id" => "D",
                "name" => "2 Dias",
                "backgroundColor" => "#9dcfa5",
            ],
                [
                "id" => "7",
                "name" => "Terrestre",
                "backgroundColor" => "#91aec4",
            ],
                [
                "id" => "F",
                "name" => "Paquetería Aérea",
                "backgroundColor" => "#ffdbad",
            ]
        ];

        $plazas_filtro = isset($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $fecha_filtro = isset($data['fecha_filtro']) ? $data['fecha_filtro'] : new \DateTime();
        $cliente_filtro = $data['cliente_filtro'] ? $data['cliente_filtro'] : 0;
        $guia_filtro = $data['guia_filtro'] ? $data['guia_filtro'] : 0;
        $garantias_filtro = isset($data['garantias_filtro']) ? $data['garantias_filtro'] : [];
        $plaza_filtro_seleccionado = isset($data['plaza_filtro_seleccionado']) ? $data['plaza_filtro_seleccionado'] : '';
        $dia_seleccionado = isset($data['dia']) ? $data['dia'] : 0;
        /*
         * ESTATUS: TERMINADO SIN PROBAR
         * Optimizado
         * FALTA PROBAR FILTROS BASICOS
         * */
        $detalle = factGuias::getDetalleGuiasTr1($plazas_filtro, $fecha_filtro, $cliente_filtro, $guia_filtro, $garantias_filtro, $plaza_filtro_seleccionado, $dia_seleccionado);

        $banderas = [];
        $tr1 = [];
        foreach ($detalle as $d) {
            if (!in_array($d->vwguias_numeroguia, $banderas)) {
                $banderas[] = $d->vwguias_numeroguia;
                $tr1[] = $d;
            }
        }


        //verificar las garantias permitidas

        die(json_encode($tr1));
    }

    public function random_color_part() {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public static function findObjectField($fieldsArray, $attrib, $value) {
        $item = NULL;
        foreach ($fieldsArray as $obj) {
            //$obj["SD"] = "asdsaddas";
            if ($value == $obj[$attrib]) {
                $item = $obj;
                break;
            }
        }

        return $item;
    }

    public static function getLabelIndex($array, $valueLabel) {
        $index = 0;
        foreach ($array as $idx => $label) {

            if ($valueLabel == $label) {
                $index = $idx;
                break;
            }
        }
        return $index;
    }

}
