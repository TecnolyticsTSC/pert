<?php


namespace App\Controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class HomeController extends BaseController

{

    public function index($request, $response, $urlparams)

    {
        $this->container->logger->info("/home");

        $params = $request->getParams();
        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'home.twig',array(
            'title'=>'Home',
            'callback'=>$callback
        ));
    }

    public function landing($request, $response, $urlparams)

    {
        $this->container->logger->info("/");
        //revisar si el usuario tiene sesión
        if($this->container->auth->check()){
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }

        return $this->container->view->render($response, 'landing/index.twig',array(
            'title'=>'PERT',
            'callback'=>$callback
        ));
    }

    public function proyeccion_entregas($request, $response, $urlparams)

    {
        $this->container->logger->info("/home");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/proyeccion-d+5/proyeccion-entregas.twig',array(
            'title'=>'Home',
            'callback'=>$callback
        ));
    }


    public function tables($request, $response, $urlparams)

    {
        try {
            /*$tables = DB::select(//WHERE table_schema = 'public'
                "SELECT table_schema, table_name FROM information_schema.tables
            WHERE table_schema NOT IN ('pg_catalog','gp_toolkit','information_schema')
            GROUP BY table_schema,table_name
            ORDER BY table_schema, table_name"
            );*/

            /*"INSERT INTO admin.dimusuarios VALUES (1 , 'Fred', 'C', 'G', 'prtdw@gmail.com', $pw, '1', '0', '2020-04-03 17:00:45', '2020-04-04 23:21:14');"*/

            /*"SELECT * from admin.dimusuarios"*/
            /*"SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_NAME = 'dimusuarios';"*/

            /*"ALTER TABLE admin.dimusuarios  ADD COLUMN admin BOOL DEFAULT FALSE ;"*/

            /*"UPDATE admin.dimusuarios SET admin = TRUE WHERE id = 2;"*/
            /*$tables = DB::select(
                " select * from tr1.dimlocations limit 10;"
            );*/

            $tables = DB::select(
                "set optimizer=off;"
            );

            echo (json_encode($tables));
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die($e);
            //die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }

    public function schemas($request, $response, $urlparams)

    {
        try {
            /*$tables = DB::select(//WHERE table_schema = 'public'
                "SELECT table_name FROM information_schema.tables  ORDER BY table_name"
            );*/
            $tables = DB::select(//WHERE table_schema = 'public'
                "SELECT schema_name FROM information_schema.schemata"
            );

            echo (json_encode($tables));
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die($e);
            //die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }

    public function js($request, $response, $urlparams)

    {
        return $this->container->view->render($response, 'dummie.twig',array(
            'title'=>'Home',
        ));
    }


    public function zonasmp($request, $response, $urlparams)

    {
        try {

            //$query = "INSERT INTO vwopEnvio VALUES ('DUMM0017358997655380006600',1.000000000,7358997,655,3,8,1.000000000,'2020-04-08 17:42:00 UTC','GDL','MTY',NULL,NULL,NULL,66266,NULL,NULL,NULL,NULL,'2020-04-09 01:45:27.25',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'2020-04-09 06:51:15.552718','2020-04-09 10:18:14.660768');";
            $query = "ELETE from vwopEnvio WHERE IDGUIA = 'DUMM0017358997655380006600' limit 1";
            $tables = DB::select(
                $query
            );
            /*$tables = DB::select(
                "select count(*) from caZonaPlaza;"
            );*/



            echo json_encode($tables);
            die();
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die($e);
            //die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }


}