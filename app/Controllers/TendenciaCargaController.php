<?php
namespace App\Controllers;
class TendenciaCargaController extends BaseController

{
    public function TendenciaCarga($request, $response, $urlparams)

    {
        // Registrar en el log que se accedió a esta url
        $this->container->logger->info("/TendenciaCarga");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        //                                     Ruta donde se encuentra la página a renderizar
        return $this->container->view->render($response, 'plataforma/TendenciaCarga.twig',
            /*Parámetros que se le pasarán a la página*/
            array(
                'titulo' => 'Analisis de Tendencia de Cargas',
                'titulo_principal' => 'Analisis de Tendencia de Cargas',
                'callback' => $callback
            ));
    }
}
