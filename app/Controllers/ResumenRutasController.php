<?php


namespace App\Controllers;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class ResumenRutasController extends BaseController

{

    public function resumenrutas($request, $response, $urlparams)

    {
        $this->container->logger->info("/resumenrutas");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/resumen-rutas.twig',array(
            'titulo'=>'Resumen de Rutas y Items Operados',
            'titulo_principal'=>'Resumen de Rutas y Items Operados',
            'callback'=>$callback
        ));
    }

}