<?php


namespace App\Controllers;

use App\Models\PermisoModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig as View;

use Respect\Validation\Validator as v;

class CatalogoPermisosController extends BaseController

{

    public function index(ServerRequestInterface $request, ResponseInterface $response,$args)
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Permisos/list.twig',
            array(
                'page_title'=>'Permisos',
                'name'=>'Permisos',
                'permisos'=>PermisoModel::all()
            )
        );
    }

    public function getCreate(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        return $this->container->view->render($response, 'plataforma/Catalogos/Permisos/create.twig',
            array(
                'page_title'=>'Crear Permiso',
                'name'=>'Permisos',
                'ramo'=>array(
                    "nombre"=>($request->getParam("nombre"))?$request->getParam("nombre"):""
                )
            )
        );
    }

    public function postCreate(ServerRequestInterface $request, ResponseInterface $response,$args)

    {

        $validation = $this->container->validator->validate($request,[
            'nombre'=>v::notEmpty()->alpha()
        ]);
        if ($validation->failed()){
            return $response->withRedirect($this->container->router->pathFor('permiso.create',[],array("nombre"=>$request->getParam("nombre"))));
            // die("invalid field name");
        }

        $r =  new PermisoModel;

        $r->nombre = $request->getParam('nombre');
        $r->save();

        $this->container->flash->addMessage('success','Se ha registrado con éxito');
        return $response->withRedirect($this->container->router->pathFor('permisos'));

    }

    public function getEdit(ServerRequestInterface $request, ResponseInterface $response, $args )
    {

        return $this->container->view->render($response, 'plataforma/Catalogos/Permisos/edit.twig',
            array(
                'page_title'=>'Editar permiso',
                'name'=>'Permisos',
                'permiso'=>PermisoModel::find($args['id'])
            )
        );
    }

    public function postEdit(ServerRequestInterface $request, ResponseInterface $response, $args )
    {
        $validation = $this->container->validator->validate($request,[
            'nombre'=>v::notEmpty()->alpha()
        ]);
        if ($validation->failed()){
            return $response->withRedirect($this->container->router->pathFor('permiso',['id'=>$args['id']]));
        }

        $r =  PermisoModel::find($request->getParam('id'));

        if (!$r){
            $this->container->flash->addMessage('danger','El registro no existe');
            return $response->withRedirect($this->container->router->pathFor('permisos'));
        }

        $r->nombre = $request->getParam('nombre');
        $r->save();

        $this->container->flash->addMessage('success','Se ha actualizado');
        return $response->withRedirect($this->container->router->pathFor('permisos'));
    }

    public function postDelete(ServerRequestInterface $request, ResponseInterface $response, $args)
    {

        $_IDS = explode(",",$request->getParam('permisos_delete'));
        foreach ($_IDS as $Id) {

            $r = PermisoModel::find($Id);

            $r->delete();

        }
        return $response->withRedirect($this->container->router->pathFor('permisos'));
    }


    /*Devoluciones JSON*/
    public function permisos(ServerRequestInterface $request, ResponseInterface $response,$args)
    {

        return $response->withJson([
            "success" => true,
            "data" => PermisoModel::all()
        ], 200,
            JSON_PRETTY_PRINT);
    }




}