<?php
namespace App\Controllers;

use App\Models\umbralesModel;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Respect\Validation\Validator as v;

class UmbralesTR1Controller extends BaseController

{
    public function routes($request, $response, $urlparams) {
        $routesUmbrals = umbralesModel::getRoutesUmbrals();

        //Convert stdClass in array
        $stdRoutes = json_decode(json_encode($routesUmbrals), true);

        //Delete repeats
        $uniques = self::unique_multidim_array($stdRoutes,'route');

        //Group by route
        $byGroup = self::group_by("route", $stdRoutes);

        //Transform data
        $formatData = array();
        $position = 0;
        foreach ($uniques as $item){
            $day1 =''; $day2 =''; $day3 =''; $day4 =''; $day5 =''; $day6 =''; $day7 ='';
            for($group=0; $group<count($byGroup[$item['route']]); $group++){
                if($byGroup[$item['route']][$group]['startweekday'] == 'LUNES'){
                    $day1 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'MARTES'){
                    $day2 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'MIERCOLES'){
                    $day3 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'JUEVES'){
                    $day4 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'VIERNES'){
                    $day5 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'SABADO'){
                    $day6 =  $byGroup[$item['route']][$group]['startweekday'];
                }
                else if($byGroup[$item['route']][$group]['startweekday'] == 'DOMINGO'){
                    $day7 =  $byGroup[$item['route']][$group]['startweekday'];
                }
            }

            $umbrales = json_decode($item['umbralesjson']);

            $formatData[$position] = (object) [
                'idroute' => $item['idroute'],
                'route' => $item['route'],
                'description' => $item['description'],
                'origen' => $item['origen'],
                'destino' => $item['destino'],
                'ptasalida' => $item['ptasalida'],
                'ptallegada' => $item['ptallegada'],
                'hours' => $item['hours'],
                'minutes' => $item['minutes'],
                'mon' => $day1,
                'tues' => $day2,
                'wednes' => $day3,
                'thurs' => $day4,
                'fri' => $day5,
                'sun' => $day6,
                'satur' => $day7,
                'green' => !isset($umbrales) ? '' : self::create_umbrales_str($umbrales->green),	
                'yellow' => !isset($umbrales) ? '' : self::create_umbrales_str($umbrales->yellow),
                'red' => !isset($umbrales) ? '' : self::create_umbrales_str($umbrales->red)
            ];         
            $position++;
        }

        return $this->container->view->render($response, 'plataforma/cumplimiento-tr1/umbrales/UmbralesTR1.twig',
            array(
                'page_title' => 'Administrar Umbrales TR1',
                'routes'=> $formatData
            )
        );
    }
    
    public function route($request, $response, $args) {
        $data = ($request->getParsedBody());
        $id_route = !isset($data['id']) ? null : $data['id'];

        $route = umbralesModel::getRouteUmbral($id_route);
        die(json_encode(array($route[0])));
    }

    public function saveUmbrales(ServerRequestInterface $request, ResponseInterface $response,$args) {
        $jsonUmbral = self::process_json_umbral($request);
        $result = umbralesModel::updateOrCreate([
            'idroute'=>$request->getParam('route'),
        ],[
            'umbralesjson'=>$jsonUmbral,
            'green'=>$request->getParam('operator_green_a').$request->getParam('number_green_a').$request->getParam('time_green_a'). ' y '.
                    $request->getParam('operator_green_b').$request->getParam('number_green_b').$request->getParam('time_green_b'),
	        'yellow'=>$request->getParam('operator_yellow_a').$request->getParam('number_yellow_a').$request->getParam('time_yellow_a'). ' y '.
                    $request->getParam('operator_yellow_b').$request->getParam('number_yellow_b').$request->getParam('time_yellow_b'),
	        'red' => $request->getParam('operator_red_a').$request->getParam('number_red_a').$request->getParam('time_red_a'). ' y '.
                    $request->getParam('operator_red_b').$request->getParam('number_red_b').$request->getParam('time_red_b')
        ]);

        if ($result){
            $this->container->flash->addMessage('success','Se han agregado exitosamente los umbrales');
            return $response->withRedirect($this->container->router->pathFor('routes_umbrales'));
        }
    }

    //Functions for operations
    private function group_by($key, $data) {
        $result = array();
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
        return $result;
    }

    private function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
       
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    private function create_umbrales_str($data){
        if(is_null($data)){
            return '';
        }
        return $data->a->operator.$data->a->comparator.$data->a->time. ' y ' .$data->b->operator.$data->b->comparator.$data->b->time;
    }

    private function process_json_umbral($request){
       return json_encode(array (
            'green' => 
            array (
              'a' => 
              array (
                'operator' => $request->getParam('operator_green_a'),
                'comparator' => $request->getParam('number_green_a'),
                'time' => $request->getParam('time_green_a'),
              ),
              'b' => 
              array (
                'operator' => $request->getParam('operator_green_b'),
                'comparator' => $request->getParam('number_green_b'),
                'time' => $request->getParam('time_green_b'),
              ),
            ),
            'yellow' => 
            array (
              'a' => 
              array (
                'operator' => $request->getParam('operator_yellow_a'),
                'comparator' => $request->getParam('number_yellow_a'),
                'time' => $request->getParam('time_yellow_a'),
              ),
              'b' => 
              array (
                'operator' => $request->getParam('operator_yellow_b'),
                'comparator' => $request->getParam('number_yellow_b'),
                'time' => $request->getParam('time_yellow_b'),
              ),
            ),
            'red' => 
            array (
              'a' => 
              array (
                'operator' => $request->getParam('operator_red_a'),
                'comparator' => $request->getParam('number_red_a'),
                'time' => $request->getParam('time_red_a'),
              ),
              'b' => 
              array (
                'operator' => $request->getParam('operator_red_b'),
                'comparator' => $request->getParam('number_red_b'),
                'time' => $request->getParam('time_red_b'),
              ),
            ),
        ));
    }

}
