<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;
use App\Models\dimDiasGarantia;

class ProcesosController extends BaseController {

    public function procesos($request, $response, $urlparams) {
        $this->container->logger->info("/procesos");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        $id = $this->container->auth->user()->id;
        return $this->container->view->render($response, 'plataforma/tr2/procesos.twig', array(
                    'titulo' => 'Procesos',
                    'titulo_principal' => 'Proceso en Plataforma',
                    'callback' => $callback,
                    "plazas" => dimDiasGarantia::obtenerPlazasUsuario($id)
        ));
    }

    public function chart($request, $response, $urlparams) {
        $this->container->logger->info("/chart");

        $params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/chart.twig', array(
                    'titulo' => 'chart',
                    'titulo_principal' => 'Ejemplos de gráficas',
                    'callback' => $callback
        ));
    }
    public function procesosFiltro($request, $response, $args) {
        $this->container->logger->info("/entradasFiltro");
        $data = ($request->getParsedBody());
        //Aqui lleguen los filtros 
        $plazas_filtro = !empty($data['plazas_filtro']) ? $data['plazas_filtro'] : [];
        $edo_garantia = !empty($data['edo_garantia']) ? $data['edo_garantia'] : [];
        $tipo_garantia = !empty($data['tipo_garantia']) ? $data['tipo_garantia'] : [];
        $tipo_empaque = !empty($data['tipo_empaque']) ? $data['tipo_empaque'] : [];
        $cliente = !isset($data['cliente']) ? null : $data['cliente'];
        $cod_razon = !empty($data['cod_razon']) ? $data['cod_razon'] : [];
        $guia = !isset($data['guia']) ? null : $data['guia'];
        $prioridad_entr = !empty($data['prioridad_entr']) ? $data['prioridad_entr'] : [];
    }

}
