<?php

namespace App\Controllers;

use App\Models\dimUsuariosModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Respect\Validation\Validator as v;

class ItemsPerdidosController extends BaseController {

    public function inicio(ServerRequestInterface $request, ResponseInterface $response, $args) {
        return $this->container->view->render($response, 'plataforma/Catalogos/ItemsPerdidos/listG.twig', array(
                    'page_title' => 'Distribución de Items Perdidos',
                    'name' => 'Distribución de Items Perdidos',
                    'usuarios' => dimUsuariosModel::all()
                        )
        );
    }

    public function getAddPlazas(ServerRequestInterface $request, ResponseInterface $response, $args) {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/plazas.twig', array(
                    'page_title' => 'Plazas Para El Usuario',
                    'name' => 'Plazas',
                    'usuario' => dimUsuariosModel::find($args['id'])
                        )
        );
    }

    public function getCreate(ServerRequestInterface $request, ResponseInterface $response, $args) {
        return $this->container->view->render($response, 'plataforma/Catalogos/ItemsPerdidos/create.twig', array(
                    'page_title' => 'Crear Distribución de Items Perdidos',
                    'name' => 'Items Perdidos',
                        )
        );
    }

    public function postCreate(ServerRequestInterface $request, ResponseInterface $response, $args) {
        $validation = $this->container->validator->validate($request, [
            'id' => v::notEmpty(),
            'nomdistrib' => v::notEmpty()->alpha(),
            'diainicio' => v::notEmpty(),
            'diafin' => v::notEmpty()
        ]);
        if ($validation->failed()) {
            return $response->withRedirect($this->container->router->pathFor('dist.create'));
            // die("invalid field name");
        }
        $r = new dimUsuariosModel;

        $r->nombres = $request->getParam('nombres');
        $r->apellidopat = $request->getParam('apellidopat');
        $r->apellidomat = $request->getParam('apellidomat');
        $r->correo = $request->getParam('correo');
        $r->contrasena = password_hash($request->getParam('contrasena'), PASSWORD_DEFAULT);
        $r->save();

        $this->container->flash->addMessage('success', 'Se ha registrado con éxito');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));
    }

    public function getEdit(ServerRequestInterface $request, ResponseInterface $response, $args) {
        return $this->container->view->render($response, 'plataforma/Catalogos/Usuarios/edit.twig', array(
                    'page_title' => 'Editar Usuario',
                    'name' => 'Usuarios',
                    'usuario' => dimUsuariosModel::find($args['id'])
                        )
        );
    }

    public function postEdit(ServerRequestInterface $request, ResponseInterface $response, $args) {

        $dataToValidate = [
            'nombres' => v::notEmpty()->alpha(),
            'apellidopat' => v::notEmpty()->alpha(),
            'apellidomat' => v::notEmpty()->alpha(),
            'correo' => v::emailAvailable()
        ];

        $r = dimUsuariosModel::find($args['id']);
        if (!$r) {
            $this->container->flash->addMessage('danger', 'El registro no existe');
            return $response->withRedirect($this->container->router->pathFor('usuario', ['id' => $args['id']]));
        }

        //Validar que el correo sea el perteneciente, sino, validar que no esté registrado previamente
        $dataToValidate['correo'] = v::notEmpty();
        if ($r->correo != $request->getParam('correo')) {
            $dataToValidate['correo'] = v::emailAvailable();
        }

        //Validar que los campos de contraseña a cambiar sean iguales
        if (!empty($request->getParam('contrasena'))) {
            $dataToValidate['contrasena'] = v::notEmpty();
            $dataToValidate['contrasena2'] = v::equals($request->getParam('contrasena'));
        }

        $validation = $this->container->validator->validate($request, $dataToValidate);

        if ($validation->failed()) {
            return $response->withRedirect($this->container->router->pathFor('usuario', ['id' => $args['id']]));
        }

        $r->nombres = $request->getParam('nombres');
        $r->apellidopat = $request->getParam('apellidopat');
        $r->apellidomat = $request->getParam('apellidomat');
        $r->correo = $request->getParam('correo');
        $r->fecha_cambio = new \DateTime();

        if (!empty($request->getParam('contrasena'))) {
            $r->contrasena = password_hash($request->getParam('contrasena'), PASSWORD_DEFAULT);
        }
        $r->save();

        $this->container->flash->addMessage('success', 'Se ha actualizado');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));
    }

    public function postDelete(ServerRequestInterface $request, ResponseInterface $response, $args) {
        $_IDS = explode(",", $request->getParam('usuarios_delete'));
        foreach ($_IDS as $Id) {

            $r = dimUsuariosModel::find($Id);

            $r->desactivar();
        }
        $this->container->flash->addMessage('success', 'Se ha actualizado');
        return $response->withRedirect($this->container->router->pathFor('usuarios'));
    }

    /* Devoluciones JSON */

    public function plazas(ServerRequestInterface $request, ResponseInterface $response, $args) {
        return $response->withJson([
                    "success" => true,
                    "data" => dimUsuariosModel::find($args['id'])->plazas
                        ], 200, JSON_PRETTY_PRINT);
    }

}
