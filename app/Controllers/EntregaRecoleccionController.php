<?php


namespace App\Controllers;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig as View;

class entregaRecoleccionController extends BaseController

{

    public function entregasRecoleccion($request, $response, $urlparams)

    {
        $this->container->logger->info("/entregasrecoleccion");

        $params = $request->getParams();

        $callback='';
        if (isset($params['callback'])){
            $callback = $params['callback'];
        }
        return $this->container->view->render($response, 'plataforma/tr2/entrega_recoleccion.twig',array(
            'titulo'=>'Identificacion de carga ordenada para entrega y recolección',
            'titulo_principal'=>'Identificacion de carga ordenada para entrega y recolección',
            'callback'=>$callback
        ));
    }


}