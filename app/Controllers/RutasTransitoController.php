<?php
namespace App\Controllers;

use App\Models\RutasTransitoModel;
use Illuminate\Database\QueryException;


class RutasTransitoController extends BaseController
{
    public function index($request, $response, $urlparams) {

        $this->container->logger->info("/indexRutasTransito");

        return $this->container->view->render($response, 'plataforma/RutasTransito.twig', array(
                    'title' => 'Analisis de Rutas en Tránsito',
                    'titulo_principal' => "Analisis de Rutas en Tránsito",
                    "DatGraficaPTAvsRTA" => RutasTransitoModel::getGraficaPTAvsRTA(),
                    "MovimTransito" => RutasTransitoModel::getDetalleMovimTransito()
        ));
    }    
    
    
    
    
    public function indexRutasTransito($request, $response, $urlparams)
    {
        // Registrar en el log que se accedió a esta url
        $this->container->logger->info("/indexRutasTransito");
        
        //die(json_encode(RutasTransitoModel::getDetalleMovimTransito()));
        
        return $this->container->view->render($response, 'plataforma/RutasTransito.twig',
            array(
                'page_title' => 'Analisis de Rutas en Tránsito',
                'MovimTransito'=> RutasTransitoModel::getDetalleMovimTransito()
            )
        );        

        /*$params = $request->getParams();

        $callback = '';
        if (isset($params['callback'])) {
            $callback = $params['callback'];
        }
        // Ruta donde se encuentra la página a renderizar
        return $this->container->view->render($response, 'plataforma/RutasTransito.twig',
            ///Parámetros que se le pasarán a la página
            array(
                'titulo' => 'Analisis de Rutas en Tránsito',
                'titulo_principal' => 'Analisis de Rutas en Tránsito'
                'callback' => $callback,
                'chartPTAvsETA' => RutasTransitoModel::getGraficaPTAvsETA(),
                'chartPTAvsRTA' => RutasTransitoModel::getGraficaPTAvsRTA()
            ));
            */
    }
    
    public function indexTabPTAvsRTA($request, $response, $urlparams) {
        return $this->container->view->render($response, 'plataforma/RutasTransito.twig',
            array(
                'page_title' => 'Analisis de Rutas en Tránsito',
                'DatTabPTAvsRTA'=> RutasTransitoModel::getGraficaPTAvsRTA()
            )
        );
    }
    
    
    
    public function detalleRutasTransitoG1($request, $response, $args) 
    {
        $this->container->logger->info("/API/RutasTransitoG11");
        $data = ($request->getParsedBody());
        //$arDetalle = RutasTransitoModel::getGraficaPTAvsETA(); 
        $colorVerde = RutasTransitoModel::getGraficaPTAvsETAVerde(); 
        $colorAma = RutasTransitoModel::getGraficaPTAvsETAAmarillo(); 
        $colorRojo = RutasTransitoModel::getGraficaPTAvsETARojo(); 
   
        //die(var_dump($colorVerde,$colorAma,$colorRojo));
        
       // $datasets = [$colorVerde,$colorAma,$colorRojo];
       $datasets = [40,0,0];
        
        $lista_porcentaje = ["A tiempo","Retrasado","Tarde"];
        
        //die(json_encode($datasets));
        
        /* return $response->withJson([
                    "success" => true,
                    "datasets" => [$datasets],
                    "labels" => $lista_porcentaje   ], 200, JSON_PRETTY_PRINT);*/
       
         die(json_encode(array("datasets" => $datasets, "labels" => $lista_porcentaje)));
             
    }    
    public function detalleRutasTransitoG2($request, $response, $args) 
    {
        $this->container->logger->info("/API/RutasTransitoG2");
        //$data = ($request->getParsedBody());        
        
        $detalle = RutasTransitoModel::getGraficaPTAvsRTA();

        die(json_encode($detalle));
    }     
}
