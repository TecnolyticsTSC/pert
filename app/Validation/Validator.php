<?php

namespace App\Validation;
use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $errors;


    public function validate($request, array  $rules)
    {

        foreach ($rules as $field => $rule){
            try {
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            } catch (NestedValidationException $e){



                /*$errors = $e->findMessages([
                    'usernameAvailable' => '{{name}} ya existe en la base de datos',
                    'emailAvailable' => '{{name}} ya existe en la base de datos',
                    'notEmpty' => '{{name}} no puede estar vacío',
                    'noWhitespace' => '{{name}} no puede contener espacios',
                    'email' => '{{name}} debe contener un e-mail válido'
                ]);
                $filteredErrors = array_filter($errors); // Ensure the array is not containing empty values
                $this->errors[$field] = $filteredErrors;*/

                $this->errors[$field] = $e->getFullMessage();
                //$this->errors["sent"] = $request->getParams();
            }
        }

        $_SESSION['errors'] = $this->errors;

        return $this;

    }




    public function failed()
    {
        return !empty($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }

}