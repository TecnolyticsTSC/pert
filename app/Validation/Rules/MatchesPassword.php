<?php
/**
 * Created by PhpStorm.
 * User: joaquin
 * Date: 22/01/2019
 * Time: 13:44
 */

namespace App\Validation\Rules;


use Respect\Validation\Rules\AbstractRule;

class MatchesPassword extends AbstractRule
{

    protected $contrasena;

    /**
     * MatchesPassword constructor.
     * @param $contrasena
     */
    public function __construct($contrasena)
    {
        $this->contrasena = $contrasena;
    }


    public function validate($input)
    {
        return password_verify($input,$this->contrasena);
    }
}