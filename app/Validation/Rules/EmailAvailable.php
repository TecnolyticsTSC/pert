<?php

namespace App\Validation\Rules;


use App\Models\dimUsuariosModel;
use Respect\Validation\Rules\AbstractRule;

class EmailAvailable extends AbstractRule
{

    public function validate($input)
    {
        return dimUsuariosModel::where('correo',$input)->count()===0;
    }
}