<?php
require __DIR__ . '../../vendor/autoload.php';
session_start();
// Create Slim app

use Respect\Validation\Validator as v;
use App\Custom\Extensions\AppExtension;

$container = require('config/container.php');

$app = new \Slim\App($container);


// Fetch DI Container
$container = $app->getContainer();

$app->add(new \App\Middleware\ValidationErrorsMiddleware($container));
$app->add(new \App\Middleware\OldInputMiddleware($container));
//AGREGAR la clase FILTRO CSRF AL APP
//$app->add(new \App\Middleware\CsrfViewMiddleware($container));

v::with('App\\Validation\\Rules');

//AGREGAR EL FILTRO CSRF AL APP
//$app->add($container->csrf);

//Auth
$container['auth'] = function ($container){
    return new App\Auth\Auth;
};


// Register flash provider
$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages;
};

// Register component on container

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('resources/templates/views', [
        'cache' => false
    ]);

    $view->getEnvironment()->addGlobal( 'asset_base_url', '/PR');
    //$view->getEnvironment()->addGlobal( 'asset_base_url', '/manitas');

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));
    $view->addExtension(new Twig_Extension_Optimizer());


    //Controladores de sesion
    $view->getEnvironment()->addGlobal('auth', [
        'check'=> $container->auth->check(),
        'user'=> $container->auth->user(),
    ]);

    //Mensajes flash
    $view->getEnvironment()->addGlobal('flash', $container->flash);

    return $view;
};


//CORS
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, token')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});
/*$container['HomeController'] = function ($container) {
    return new \App\Controllers\HomeController($container);
};*/



require_once __DIR__ . '../../app/routes.php';