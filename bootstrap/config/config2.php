<?php

return [
"settings" =>[
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false,
    'logger' => [
        'name' => 'PERT-APP',
        'path' => __DIR__ . '/../logs/'.date("d-m-Y").'.log',
        'level' => \Monolog\Logger::DEBUG,
    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'pert_dw',
        'username' => 'root',
        'password' => '',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => '',
    ],
]
 ];