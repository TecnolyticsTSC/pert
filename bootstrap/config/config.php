<?php

return [
"settings" =>[
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false,
    'logger' => [
        'name' => 'PERT-APP',
        'path' => __DIR__ . '/../logs/'.date("d-m-Y").'.log',
        'level' => \Monolog\Logger::DEBUG,
    ],/*BD Postgres*/
    'db' =>
    /*[

        'driver' => 'pgsql',
        'host' => '172.16.89.86',
        'port' => '5432',
        'database' => 'PERT_Staging',
        'username' => 'pert',
        'password' => 'Est4f3t4PERT',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => '',
    ],*/
    /*BD greenplum*/
     [

        'driver' => 'pgsql',
        'host' => '172.16.89.95',
        'port' => '5432',
        'database' => 'PERT_DW',
        'username' => 'pert',
        'password' => 'Est4f3t4PERT',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => '',
    ],
     /*Local*/
     /*[

        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'PERT_DW',
        'username' => 'root',
        'password' => '',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => '',
    ],*/
]
 ];