

// Initialize and add the map
function initMap() {
    // The location of Uluru
    var uluru = {lat: 20.976642, lng:  -89.625549};
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('mapCargaDescarga'), {zoom: 14, center: uluru, mapTypeId: 'hybrid',});//roadmap, satellite, hybrid and terrain



    // The marker, positioned at Uluru
    var icon = {
        url: "http://192.168.1.77/PERT/dist/assets/icons/transporte_verde.svg", // url
        scaledSize: new google.maps.Size(30, 30), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(15, 15), // anchor
        labelOrigin: new google.maps.Point(15, 40)

    };

    var label = {
        text :"COP_MEX",
        color:"#fff",
        fontWeight:"500",
        fontSize: "18px",

    };
    var marker = new google.maps.Marker({
        icon:icon,
        position: uluru,
        map: map,
        label: label,

    });

    var contentStr ='<span class="text-muted font-weight-bold">ADC00198-253ER8</span>';
    var iw = new google.maps.InfoWindow({
        content: contentStr
    });
    iw.open(map, marker);
    google.maps.event.addListener(marker, "click", function(e) {
        iw.open(map, this);
    });
}

initMap();
$(document).ready(function () {

    getInfoTableDetallePorEstatus(objInfoEstatusTipoCarga);
    getInfoTableDetalleCarga(objInfoDetalleCarga);

});


var objInfoEstatusTipoCarga = [
    {
        ruta: "ID Ruta. 1500",
        vehiculo_destino:"REMOLQUE RADC0152",
        p_ocupacion: "30%",
        estatus: "TRANSITO",
        is_pendiente:false,
        fecha_hora:"4/16/2019 9:11 PM MEX",
        sobres: 2,
        garantia: 200,
        terrestre:15,
        total: 217,
        detalle_ruta: [
            {
                remolque: "Plaza Destino XX1",
                sobres: 0,
                garantia: 5,
                terrestre: 3
            },
            {
                remolque: "Plaza Destino XX2",
                sobres: 0,
                garantia: 10,
                terrestre: 2
            },
            {
                remolque: "Plaza Destino XX3",
                sobres: 1,
                garantia: 50,
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX4",
                sobres: 0,
                garantia: 15,
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX5",
                sobres: 1,
                garantia: 20,
                terrestre: 10
            },
            {
                remolque: "Plaza Destino XX6",
                sobres: 0,
                garantia: 100,
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX7",
                sobres: 0,
                garantia: 60,
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX8",
                sobres: 0,
                garantia: 40,
                terrestre: 0
            },
        ]

    },
    {
        ruta: "ID Ruta. 1072",
        vehiculo_destino:"REMOLQUE TTE01662",
        p_ocupacion: "90%",
        estatus: "PENDIENTE",
        is_pendiente:true,
        fecha_hora:"4/16/2019 9:11 PM LEN",
        sobres: "1/1",
        garantia: "110/166",
        terrestre:"2/5",
        total: "113/172, 66%",
        detalle_ruta: [
            {
                remolque: "Plaza Destino XX1",
                sobres: 0,
                garantia: 0,
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX2",
                sobres: 0,
                garantia: "80/80",
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX3",
                sobres: "1/1",
                garantia: 0,
                terrestre: "1/2"
            },
            {
                remolque: "Plaza Destino XX4",
                sobres: 0,
                garantia: "20/30",
                terrestre: 0
            },
            {
                remolque: "Plaza Destino XX5",
                sobres: 0,
                garantia: "10/56",
                terrestre: 0
            },
        ]

    }

];
/**
 * funcion para obtener infromacion de la tabla
 * Detalle por Estatus y Tipo de Carga
 */
function getInfoTableDetallePorEstatus(objectRows) {
    var e ='';
    var arrayRowsCollapse = [];//arreglo de grupos
    $.each(objectRows,function (idx,obj) {

        var groupRoute = (obj.detalle_ruta.length>0)?"rutacollapse"+(idx+1):"";

        e+="<tr>" +
            "<td style='text-decoration: underline;white-space: nowrap;' class='"+groupRoute+"'>"+obj.ruta+"</td>" +
            "<td style='text-decoration: underline;'>"+obj.vehiculo_destino+"</td>" +
            "<td>"+obj.p_ocupacion+"</td>" +
            "<td><div class='"+(obj.is_pendiente?"badge badge-danger-pendiente":"")+"'>"+obj.estatus+"</div></td>" +
            "<td>"+obj.fecha_hora+"</td>" +
            "<td>"+obj.sobres+"</td>" +
            "<td>"+obj.garantia+"</td>" +
            "<td>"+obj.terrestre+"</td>" +
            "<td>"+obj.total+"</td>" +
            "</tr>";
        /*<div "badge badge-primary text-wrap" style="width: 6rem;">
         This text should wrap.
         </div>*/


        $.each(obj.detalle_ruta, function (idx, objChilds) {
            e += "<tr class='" + groupRoute + "'>" +
                "<td></td>" +//espacio en blanco
                "<td>" + objChilds.remolque + "</td>" +
                "<td colspan='3'></td>" + //espacio en blanco
                "<td>" + objChilds.sobres + "</td>" +
                "<td>" + objChilds.garantia + "</td>" +
                "<td>" + objChilds.terrestre + "</td>" +
                "<td></td>" +
                "</tr>";
        });

        //$("#table-collapse>tbody").html(e);
        if(obj.detalle_ruta.length>0) {
            arrayRowsCollapse.push(groupRoute);
            //CreateGroup(groupRoute);
        }


    });


    $("#table-collapse>tbody").html(e);

    //instance columns ar colapsed
    $.each(arrayRowsCollapse,function (idx,group) {
        CreateGroup(group);
    });

    console.error(arrayRowsCollapse)


    //console.log("Working this function getInfoTableDetallePorEstatus()");
}


var objInfoDetalleCarga = [
    {
        guia: "1015063317612681075393",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Guadalajara",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "3015063317612681075392",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Guadalajara",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "9015063317612681075394",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Monterrey",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "7015063317612681075395",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Durango",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "7015063317612681075390",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Guadalajara",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "3015063317612681075397",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Hermosillo",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "1015063317612681075398",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Monterrey",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "5015063317612681075391",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Guadalajara",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "2015063317612681075388",
        garantia:"1 día",
        cliente:"5063317",
        razon_social: "AMORE BELLEZA S.A DE C.V.",
        origen:"Tijuana",
        destino: "Guadalajara",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "401501555324A503520001",
        garantia:"11:30",
        cliente:"5015553",
        razon_social: "CONSEJO DE LA JUDICATURA FEDERAL",
        origen:"Tijuana",
        destino: "MEXICO D.F.",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "3018703471612600837185",
        garantia:"1 día",
        cliente:"8703471",
        razon_social: "COMERCIALIZADORA TAKUS DE RL DE CV",
        origen:"ESTACION AEREA TIJ",
        destino: "PUEBLA",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "501871001612600943253",
        garantia:"1 día",
        cliente:"8710001",
        razon_social: "BARSTONG SA DE CV",
        origen:"Tijuana",
        destino: "MEXICO D.F.",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "1015535018612600956820",
        garantia:"1 día",
        cliente:"5535018",
        razon_social: "CSI TACTICAL AND BALISTICA DE CV",
        origen:"Tijuana",
        destino: "MEXICO D.F.",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "9018703471612600837187",
        garantia:"1 día",
        cliente:"8703471",
        razon_social: "COMERCIALIZADORA TAKUS DE RL DE CV",
        origen:"ESTACION AEREA TIJ",
        destino: "PUEBLA",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "9055836563612620100090",
        garantia:"1 día",
        cliente:"5836563",
        razon_social: "CARLOS ALBERTO CASTRO",
        origen:"Tijuana",
        destino: "Puerto Vallarta",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "5055003333612681080328",
        garantia:"1 día",
        cliente:"5003333",
        razon_social: "LETICIA PAZ URIAS",
        origen:"Sn Luis Rio Colorado",
        destino: "Nogales",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "3055003333612681080329",
        garantia:"1 día",
        cliente:"5003333",
        razon_social: "LETICIA PAZ URIAS",
        origen:"Sn Luis Rio Colorado",
        destino: "Nogales",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "4018674774620600127747",
        garantia:"1 día",
        cliente:"8674774",
        razon_social: "COMERCIALIZADORA RCBEAUTYS DE RL DE CV",
        origen:"Cd. Juárez",
        destino: "Piedras Negras",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "0048703471612690365587",
        garantia:"1 día",
        cliente:"8703471",
        razon_social: "COMERCIALIZADORA TAKUS DE RL DE CV",
        origen:"ESTACION AEREA TIJ",
        destino: "Nogales",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
    {
        guia: "6018666722018600430633",
        garantia:"1 día",
        cliente:"8666722",
        razon_social: "ROVEEDORA DE SERVICIOS HUMANOS Y LOGISTICOS AXP SA DE C",
        origen:"Ensenada",
        destino: "Cancún",
        tipo_item:"PAQUETE",
        operacion: "05/12/2019",
        ut_bolsa:"NA",
        status:""
    },
];

/**
 * funcion para generar informacion de la tabla
 * Detalla de Carga
 * @param objectRows
 */
function getInfoTableDetalleCarga(objectRows) {
    var e ='';

    $.each(objectRows,function (idx,obj){

        e+='<tr>' +
            '<td>'+(idx+1)+'</td>' +
            '<td>'+obj.guia+'</td>' +
            '<td>'+obj.garantia+'</td>' +
            '<td>'+obj.cliente+'</td>' +
            '<td>'+obj.razon_social+'</td>' +
            '<td>'+obj.origen+'</td>' +
            '<td>'+obj.destino+'</td>' +
            '<td>'+obj.tipo_item+'</td>' +
            '<td>'+obj.operacion+'</td>' +
            '<td>'+obj.ut_bolsa+'</td>' +
            '<td>'+obj.status+'</td>' +
            '</tr>';

    });


    $("#detalle_carga>tbody").html(e);
}

