/**
 * Created by Blue_Beats on 20/03/2020.
 */



function  COLLAPSE_METHODS ()  {

    this.groupNameSplited= null;
    this.groupNameTotal= null;
    this.groupNameButton= null;
    this.groupNameButtonContent= null;

    this.setGroupNameClass= function (groupNameClassSplited) {
        this.groupNameSplited = groupNameClassSplited;
        this.groupNameTotal = groupNameClassSplited +"-total";
        this.groupNameButton = groupNameClassSplited +"-btn";
        this.groupNameButtonContent = groupNameClassSplited +"-content";

        //iniciar total colapsado
        this.init();
    };
    this.doShowSplited= function (state) {
        $(  "."+this.groupNameSplited).removeClass("hideGroupGarantiaTotal").addClass(state?"":"hideGroupGarantiaTotal")//css({"display":state?"":"none"})//si es true mostrar
    };
    this.doShowTotal= function (state) {
        $( "."+this.groupNameTotal).removeClass("hideGroupGarantiaTotal").addClass(state?"":"hideGroupGarantiaTotal")//css({"display":state?"":"none"})//si es true mostrar
    };
    this.hideColumns = function () {
        this.doShowSplited(false);
        this.doShowTotal(true);
    };
    this.showColumns = function () {
        this.doShowSplited(true);
        this.doShowTotal(false);
    };
    this.toggleColumns = function () {
        if ($("."+this.groupNameSplited).is(":visible")){
            this.hideColumns();
        }else{
            this.showColumns();
        }
    };

    this.createButtonToggled = function () {
        $('td.' + this.groupNameButtonContent).prepend("<img class='" + this.groupNameButton + " button_closed '> ");
        this.initAction();
    };

    this.initAction = function () {
        var self =this;
        $('.' + this.groupNameButton).click( function(e){
            e.preventDefault();
            self.toggleColumns();
            self.toggleButton();
            console.log("click button")
        });


    };

    this.toggleButton =  function () {
        $('.' + this.groupNameButton).toggleClass('button_open');
        $('.' + this.groupNameButton).toggleClass('button_closed');
    };

    this.firstInit= function () {
        this.hideColumns();
        this.toggleButton();
    };

    this.init = function () {
        this.firstInit();
        this.createButtonToggled();
    }


}

function COLLAPSE_COLUMN() {

    COLLAPSE_METHODS.call(this);


}


COLLAPSE_COLUMN.prototype.constructor = COLLAPSE_COLUMN;


/*COLLAPSE_COLUMN.prototype.constructor = function COLLAPSE_COLUMN() {
    this.groupNameTotal = "asdasd";
};*/
