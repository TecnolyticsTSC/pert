    (function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
            if (this.href === path) {
                $(this).addClass("active");
            }
        });

    // Toggle the side navigation
    $("#sidebarToggle,#cerrarSidebar").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
        hideOverflow();
    });


    var hideOverflow = function () {
      if(
          $("body").hasClass("sb-sidenav-toggled") &&
              $('#cerrarSidebar').is(":visible")

      ){
          $("body").css({"overflow-y":"hidden"});
      }else{
          $("body").css({"overflow-y":"auto"});
      }
    };
})(jQuery);
