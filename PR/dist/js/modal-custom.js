/**
 * Created by Blue_Beats on 23/03/2020.
 */

/*filtros*/
var today = new Date();
var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
var yyyy = today.getFullYear();

today = dd + "/" + mm + "/" + yyyy;
/*filtros={
 fecha_filtro: today,
 fecha_fin_filtro: 0,
 plaza_filtro:0,
 garantia_filtro:0,
 plazas_filtro:[],
 garantias_filtro:[],
 cliente_filtro:0,
 guia_filtro:'',
 dia:-1
 };*/
function filtrar() {
  /*return $.ajax({
     type: "POST",

     dataType: "json",
     success: function (data) {
     console.log("filtrando",data);
     },
     data: filtros
     });*/
}

function generarExcel() {
  /*if (nombre){

    }*/

  $(".table2excel").table2excel({
    exclude: ".noExl",
    name: "Detalle de guías",
    filename:
      "Detalle guias " +
      new Date().toISOString().replace(/[\-\:\.]/g, "") +
      ".xls",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true,
    preserveColors: false,
  });
}
/*$('#filtro_plaza').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 var value = this.value;
 if ( id === 0 || id ==='0'){
 value = 0;
 }
 console.log("filtrando","filtro_plaza",value);
 filtros.plaza_filtro =value;
 filtrar();
 });
 $('#filtro_garantia').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 console.log("filtrando","filtro_garantia",id,this.value);
 filtros.garantia_filtro =id;
 filtrar();
 });
 $('#filtro_cliente').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 var value = this.value;
 //Para anular el filtro en caso que no tenga valor
 if ( id === 0 || id ==='0'){
 value = 0;
 }
 console.log("filtrando","filtro_cliente",value);
 filtros.cliente_filtro =value;
 filtrar();
 });
 $('#filtro_guia').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 var value = this.value;
 //Para anular el filtro en caso que no tenga valor
 if ( value.length===0 ){
 value = 0;
 }
 console.log("filtrando","filtro_guia",value);
 filtros.guia_filtro =value;
 filtrar();
 });
 $('#fechaInicio').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 var value = this.value;
 //Para anular el filtro en caso que no tenga valor
 if ( value.length===0 ){
 value = today;
 }
 console.log("filtrando","fechaInicio",value);
 filtros.fecha_filtro =value;
 filtrar();
 });
 $('#fechaFin').on('change', function() {
 var id = $(this).children(":selected").attr("id");
 var value = this.value;
 //Para anular el filtro en caso que no tenga valor
 if ( value.length===0 ){
 value = today;
 }
 console.log("filtrando","fechaFin",value);
 filtros.fecha_fin_filtro =value;
 filtrar();
 });*/

$(function () {
  /*$(".export").click(function(e){


    });*/
});

/**
 * Funcion para modal y mostrar el detalle de cuadratura
 * @param objectInfo
 */
function showDetalleGuias(objectInfo) {
  $.alert({
    title:
      "" +
      objectInfo.list_guias.length +
      " Guías No Localizadas de la " +
      objectInfo.route_name,
    titleClass: "custom-title-modal",
    content: generateTableDetalleGuiasStructure(objectInfo.list_guias),
    columnClass:
      "col-md-10 col-md-offset-2  col-sm-10 col-sm-offset-2 col-8  custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.7,
    //useBootstrap: false,
    //boxWidth: '500px',
    bootstrapClasses: {
      //    container: 'container',
      //    containerFluid: 'container-fluid',
      // row: 'row justify-content-center',
    },
  });
}

/**
 *
 * funcion para generar la estructura de la tabla a mostrar en el modal
 *
 * @param rows
 * @returns {string}
 */
function generateTableDetalleGuiasStructure(rows) {
  var e = "";
  e +=
    '<div class="table-responsive" > ' +
    '<div class="row">' +
    '<div class="col-3 pb-2">' +
    '<button type="button" class="btn btn-secondary btn-sm export" onclick="generarExcel(""detalle Guias"")">Exportar XLS</button>' +
    "</div>" +
    "</div>" +
    '<table class="table2excel table table-borderless custom-fontsize-table mb-0 table-striped table-sm table-hover table-light table-dark table-black" ' +
    "border=0 cellpadding=0 cellspacing=0  " +
    'style="text-align: center;" >' +
    "<thead >" +
    '<tr class="header-divider-table"><th class="text-left">Guía</th>' +
    "<th>Plaza Destino</th>" +
    '<th class="text-left">Cliente</th>' +
    '<th class="text-left">Contenido</th>' +
    "<th>Plaza de Escaneo</th>" +
    "<th>Código de Razón</th>" +
    "</tr>" +
    "</thead>" +
    "<tbody>" +
    '<colgroup><col class="text-left"><col><col><col><col><col></colgroup>';

  $.each(rows, function (idx, obj) {
    e +=
      "<tr>" +
      "<td class='text-left'>" +
      obj.guia +
      "</td>" +
      "<td>" +
      obj.plaza_destino +
      "</td>" +
      "<td class='text-left'>" +
      obj.cliente +
      "</td>" +
      "<td class='text-left'>" +
      obj.contenido +
      "</td>" +
      "<td>" +
      obj.plaza_escaneo +
      "</td>" +
      "<td class='text-left'>" +
      obj.codigo_razon +
      "</td>" +
      "</tr>";
  });

  e +=
    "</tbody>" +
    '<tfoot ><tr class="footer-divider-table pb-5"><td colspan="6">&nbsp;</td></tr></tfoot>' +
    "</table></div>";

  return e;
}

var object = {
  route_name: "Ruta AE-002",
  tramo: "ACA-MEX C1",
  list_items: [
    {
      plaza_destino: "MEX",
      items_totales: "200",
      sobre: "120",
      paquete: "60",
      palet: "-",
      c10: "20",
    },
    {
      plaza_destino: "GDL",
      items_totales: "100",
      sobre: "50",
      paquete: "40",
      palet: "-",
      c10: "10",
    },
    {
      plaza_destino: "QRO",
      items_totales: "50",
      sobre: "-",
      paquete: "25",
      palet: "20",
      c10: "5",
    },
    {
      plaza_destino: "TIJ",
      items_totales: "450",
      sobre: "300",
      paquete: "100",
      palet: "-",
      c10: "50",
    },
    {
      plaza_destino: "Total",
      items_totales: "800",
      sobre: "470",
      paquete: "225",
      palet: "20",
      c10: "85",
    },
  ],
};

/**
 * funcion para el modal y mostrar el detalle de destino de los items
 * @param objectInfo
 */
function showDetalleDestinosItems(objectInfo) {
  $.alert({
    title:
      "Destino de Items de la Ruta " +
      objectInfo.route_name +
      " Tramo " +
      objectInfo.tramo,
    titleClass: "custom-title-modal",
    content: generateTableDetalleDestinosItems(objectInfo.list_items),
    columnClass: "col-md-8  col-sm-8 col-xs-8 custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.7,
  });
}

/**
 * funcion para generar la estructura de la tabla
 * @param rows
 * @returns {string}
 */
function generateTableDetalleDestinosItems(rows) {
  var e = "";

  e +=
    '<table class="table table-borderless custom-fontsize-table mb-0 text-center table-striped table-sm table-hover table-light table-dark table-black" style="text-align: center;">' +
    '<thead class="header-divider-table">' +
    "<tr>" +
    '<th rowspan="2" style="vertical-align: middle">Plaza<br>Destino</th>' +
    '<th rowspan="2" style="vertical-align: middle">Items<br>Totales</th>' +
    '<th class="header-divider-table" colspan="4">Empaque</th>' +
    "</tr>" +
    "<tr>" +
    "<th>Sobre</th>" +
    "<th>Paquete</th>" +
    "<th>Palet</th>" +
    "<th>C10</th>" +
    "</tr>" +
    "</thead>" +
    '<tbody class=""">';

  $.each(rows, function (idx, obj) {
    e +=
      "<tr " +
      (rows.length === idx + 1 ? 'class="text-total-color"' : "") +
      ">" +
      "<td>" +
      obj.plaza_destino +
      "</td>" +
      "<td>" +
      obj.items_totales +
      "</td>" +
      "<td>" +
      obj.sobre +
      "</td>" +
      "<td>" +
      obj.paquete +
      "</td>" +
      "<td>" +
      obj.palet +
      "</td>" +
      "<td>" +
      obj.c10 +
      "</td>" +
      "</tr>";
  });

  e +=
    "</tbody>" +
    '<tfoot ><tr class="footer-divider-table pb-5"><td colspan="6">&nbsp;</td></tr></tfoot>' +
    "</table>";
  return e;
}

var object_garantias = {
  tipo_garantia: "09:30 am",
  total_garantias: 400,
  list_garantias: [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ],
};

/**
 * funcion para el modal y mostrar el detalle de las garantias
 * @param objectInfo
 */
function showDetalleGarantias(objectInfo) {
  $.alert({
    title:
      "Detalle de: Garantías " +
      objectInfo.tipo_garantia +
      " Total: " +
      objectInfo.total_garantias,
    titleClass: "custom-title-modal",
    content: generateTableDetalleGarantias(objectInfo.list_garantias),
    columnClass:
      "col-lg-12 col-md-12  col-sm-12 col-xs-garantias custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.7,
    onContentReady: function () {
      var self = this;
      var el2 = self.$content.closest(".jconfirm-content");
      //el2.addClass("hide-overflow-x");//css({"overflow-x":"hidden!important"});

      setTimeout(function () {
        var el = self.$content.closest(".jconfirm-content-pane");
        // el.addClass("hide-overflow-x");//.css({"overflow-x":"auto!important"});
        // console.log(el);
      }, 5000);
    },
  });
}

/**
 * funcion para generar la estructura de la tabla
 * @param rows
 * @returns {string}
 */
function generateTableDetalleGarantias(rows) {
  var e = "";

  e +=
    '<div class="table-responsive" >' +
    '<div class="row">' +
    '<div class="col-3 pb-2">' +
    '<button type="button" class="btn btn-secondary btn-sm export" onclick="generarExcel()">Exportar XLS</button>' +
    "</div>" +
    "</div>" +
    '<table class="table2excel table table-borderless custom-fontsize-table mb-0 text-center table-striped table-sm table-hover table-light table-dark table-black" style="text-align: center;">' +
    '<thead class="">' +
    "<tr>" +
    "<th></th>" +
    "<th></th>" +
    '<th colspan="7" class="color_border_botom_white">Gu&iacute;a</th>' +
    "<th >&nbsp;</th>" +
    '<th colspan="3" class="color_border_botom_white">Cliente</th>' +
    "<th >&nbsp;</th>" +
    '<th colspan="7" class="color_border_botom_white">Ubicaci&oacute;n</th>' +
    "<th >&nbsp;</th>" +
    '<th colspan="9" class="color_border_botom_white">Último Estatus</th>' +
    "</tr>" +
    "<tr >" +
    '<th class="color_border_botom_white">Consec</th>' +
    "<th></th>" +
    '<th class="color_border_botom_white">Número</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Tipo de empaque</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Garantía</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Contenido</th>' +
    "<th></th>" +
    '<th class="color_border_botom_white">Número</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Razón Social</th>' +
    "<th></th>" +
    '<th class="color_border_botom_white">Distrito</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Regional</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Plaza</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">CD</th>' +
    "<th></th>" +
    '<th class="color_border_botom_white">Movimiento</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Código de Razon</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">No. Empleado</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Ruta</th>' +
    '<th class="color_border_botom_white"></th>' +
    '<th class="color_border_botom_white">Fecha del Evento</th>' +
    "</tr>" +
    "</thead>" +
    "<tbody>";

  $.each(rows, function (idx, obj) {
    e +=
      "<tr>" +
      "<td>" +
      (idx + 1) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.vwguias_numeroguia === null ? "" : obj.vwguias_numeroguia) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.vwinfoenvio_nomtipoenvio === null
        ? ""
        : obj.vwinfoenvio_nomtipoenvio) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.garantia === null ? "" : obj.garantia) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.vwinfoenvio_contenido === null ? "" : obj.vwinfoenvio_contenido) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.numero_cliente === null ? "" : obj.numero_cliente) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.vwdestino_razonsocial === null ? "" : obj.vwdestino_razonsocial) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.distrito === null ? "" : obj.distrito) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.regional === null ? "" : obj.regional) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.plaza === null ? "" : obj.plaza) +
      "</td>" +
      "<td></td>" + //separador
      "<td>" +
      (obj.codigo_postal === null ? "" : obj.codigo_postal) +
      "</td>" +
      "<td>" +
      (obj.direccion === null ? "" : obj.direccion) +
      "</td>" + //separador
      "</tr>";

    /*e+="<tr>" +
            "<td>"+(idx+1)+"</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "</tr>"*/
  });

  e += "</tbody></table></div>";

  return e;
}

var objectVehicle = {
  vehicle_name: "T001245",
  route_name: "ACA 3316",
  tramo: "ACA-MEX",
  position: {
    latitud: 20.96974,
    longitud: -89.621489,
  },
};

/**
 * funcion para el modal y mostrar el detalle de geoposicion
 * @param objectInfo
 */
function showGeopositionVehicle(objectInfo) {
  $.alert({
    title:
      "Posición de Vehículo " +
      objectInfo.vehicle_name +
      " | Ruta " +
      objectInfo.route_name +
      " | Tramo " +
      objectInfo.tramo,
    titleClass: "custom-title-modal",
    content: generateMapContent(),
    columnClass: "col-md-10  col-sm-10 col-xs-10 custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.7,
    onContentReady: function () {
      var self = this;
      var el2 = self.$content.closest("#mapModalLlegadasTr1");
      el2; //css({"overflow-x":"hidden!important"});

      /* setTimeout(function () {

             var el = self.$content.closest(".jconfirm-content-pane");
             // el.addClass("hide-overflow-x");//.css({"overflow-x":"auto!important"});
             // console.log(el);
             }, 5000);*/

      var mapa = function initMap() {
        // Initialize and add the map
        // The location of Uluru
        var uluru = {
          lat: objectInfo.position.latitud,
          lng: objectInfo.position.longitud,
        };
        // The map, centered at Uluru
        var map = new google.maps.Map(
          document.getElementById("mapModalLlegadasTr1"),
          { zoom: 14, center: uluru, mapTypeId: "hybrid" }
        );
        // The marker, positioned at Uluru

        var icon = {
          url: "http://192.168.1.77/PERT/dist/assets/icons/avion_verde.svg", // url
          scaledSize: new google.maps.Size(30, 30), // scaled size
          origin: new google.maps.Point(0, 0), // origin
          anchor: new google.maps.Point(15, 15), // anchor
          labelOrigin: new google.maps.Point(15, 40),
        };

        var label = {
          text: "COP_MEX",
          color: "#fff",
          fontWeight: "500",
          fontSize: "18px",
        };
        var marker = new google.maps.Marker({
          icon: icon,
          position: uluru,
          map: map,
          label: label,
        });
        var contentStr =
          '<span class="text-muted font-weight-bold">ADC00198-253ER8</span>';
        var iw = new google.maps.InfoWindow({
          content: contentStr,
        });
        iw.open(map, marker);
        google.maps.event.addListener(marker, "click", function (e) {
          iw.open(map, this);
        });
      };

      mapa();
    },
  });
}

/**
 * funcion para generar el contenedor del mapa
 * @returns {string}
 */
function generateMapContent() {
  var e = "";

  e += '<div id="mapModalLlegadasTr1">' + "</div>";

  return e;
}

var objectRastreo = {
  numero_guia: "278158263651L00120523",
  codigo_rastreo_guia: "1898313797",
  numero_cliente: "8158269 VIP",
  razon_social: "Mercado Libre",
  plaza_origen: "CDMX",
  recoleccion: "02/05/2019 18:06",
  plaza_destino: "Cancún",
  codigo_postal: 77527,
  servicio: "LTL",
  estatus: "Entregado",
  entrega: "07/05/2019 11:58",
  recibio: "PDV3: Andrea Bazán",

  lista_rastreo: [
    {
      fecha_hora: "07/05/2019 11:58",
      lugar_movimiento: "Recolección en Oficina por Ruta Local Chilpancingo",
      ruta: "P000000",
      comentarios:
        "C13 - Envío ocurre direccionado a oficina Av. Guerrero 54 Centro",
      vehiculo: "V00A0000",
      empleado: 0,
    },
    {
      fecha_hora: "07/05/2019 11:52",
      lugar_movimiento: "Entrada de Envío a Contenedor",
      ruta: "P777777",
      comentarios:
        "Chilpancingo de los Bravos" +
        "<br>C13 - Envío ocurre direccionado a oficina Av. Guerrero 54 Centro",
      vehiculo: "M01EM1111",
      empleado: 28168,
    },
    {
      fecha_hora: "07/05/2019 09:58",
      lugar_movimiento: "Envío recibido en oficina Acapulco",
      ruta: "R139300",
      comentarios: "Chilpancingo de los Bravos",
      vehiculo: "R26TT0801",
      empleado: 19733,
    },
    {
      fecha_hora: "07/05/2019 09:55",
      lugar_movimiento: "Salida de Centro Operativo",
      ruta: "R139300",
      comentarios: "",
      vehiculo: "R26TT0801",
      empleado: 19733,
    },
  ],
};
function showRatreoGuias(objectInfo) {
  $.alert({
    title: "Rastreo de Guías",
    titleClass: "custom-title-modal",
    content: generateTableRastreoGuias(objectInfo),
    columnClass:
      "col-lg-12 col-md-12  col-sm-12 col-xs-rastreo-guias custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.7,
  });
}

function generateTableRastreoGuias(rows) {
  var e = "";

  //tabla principal
  e +=
    '<div class="table-responsive" >' +
    '<div class="row">' +
    '<div class="col-3 pb-2">' +
    '<button type="button" class="btn btn-secondary btn-sm export" onclick="generarExcel()">Exportar XLS</button>' +
    "</div>" +
    "</div>" +
    '<table class="table2excel table table-borderless custom-fontsize-table mb-0 text-center  table-sm table-hover table-light table-dark table-black" style="text-align: center;">' +
    "<thead>" +
    "<tr>" +
    '<th colspan="2">Guía</th>' +
    "<th></th>" + //separador
    '<th colspan="2">Cliente</th>' +
    //'<th></th>' + //separador
    '<th colspan="2">Origen</th>' +
    "<th></th>" + //separador
    '<th colspan="2">Destino</th>' +
    "<th></th>" + //separador
    '<th rowspan="2" class="color_border_botom_white align-text-top" >Servicio</th>' +
    '<th rowspan="2" class="color_border_botom_white align-text-top">Estatus</th>' +
    '<th rowspan="2" class="color_border_botom_white align-text-top">Entrega</th>' +
    '<th rowspan="2" class="color_border_botom_white align-text-top">Recibió</th>' +
    "</tr>" +
    "<tr>" +
    '<th class="color_border_botom_white">Número</th>' +
    '<th class="color_border_botom_white">Código de Rastreo</th>' +
    "<th></th>" + //separador
    '<th class="color_border_botom_white">Número</th>' +
    '<th class="color_border_botom_white">Razón Social</th>' +
    //'<th></th>' + //separador
    '<th class="color_border_botom_white">Plaza</th>' +
    '<th class="color_border_botom_white">Recolección</th>' +
    "<th></th>" + //separador
    '<th class="color_border_botom_white">Plaza</th>' +
    '<th class="color_border_botom_white">CP</th>' +
    "<th></th>" + //separador
    "</tr>" +
    "</thead>" +
    "<tbody>";

  e +=
    "<tr>" +
    "<td>" +
    rows.numero_guia +
    "</td>" +
    "<td>" +
    rows.codigo_rastreo_guia +
    "</td>" +
    "<td></td>" + //separador
    "<td>" +
    rows.numero_cliente +
    "</td>" +
    "<td>" +
    rows.razon_social +
    "</td>" +
    //"<td></td>" + //separador
    "<td>" +
    rows.plaza_origen +
    "</td>" +
    "<td>" +
    rows.recoleccion +
    "</td>" +
    "<td></td>" + //separador
    "<td>" +
    rows.plaza_destino +
    "</td>" +
    "<td>" +
    rows.codigo_postal +
    "</td>" +
    "<td></td>" + //separador
    "<td>" +
    rows.servicio +
    "</td>" +
    "<td>" +
    rows.estatus +
    "</td>" +
    "<td>" +
    rows.entrega +
    "</td>" +
    "<td>" +
    rows.recibio +
    "</td>" +
    "</tr>";

  e += "</tbody></table></div>";

  //tabla secundaria
  e +=
    '<div class="table-responsive" >' +
    '<div class="row">' +
    '<div class="col-3 pb-2">' +
    '<button type="button" class="btn btn-secondary btn-sm export" onclick="generarExcel()">Exportar XLS</button>' +
    "</div>" +
    "</div>" +
    '<table class="table2excel table table-borderless custom-fontsize-table mb-0 table-striped table-sm table-hover table-light table-dark table-black" >' +
    '<thead class="header-divider-table-top">' +
    "<tr>" +
    '<th class="text-left">Fecha - Hora</th>' +
    '<th class="text-left w-25">Lugar - Movimiento</th>' +
    '<th class="text-left">Ruta</th>' +
    '<th class="text-left">Comentarios</th>' +
    '<th class="text-left">Vehículo</th>' +
    '<th class="text-left">Empleado</th>' +
    "</tr>" +
    "</thead>" +
    "<tbody>";

  $.each(rows.lista_rastreo, function (idx, obj) {
    e +=
      "<tr >" +
      "<td class='text-right'>" +
      obj.fecha_hora +
      "</td>" +
      "<td>" +
      obj.lugar_movimiento +
      "</td>" +
      "<td>" +
      obj.ruta +
      "</td>" +
      "<td>" +
      obj.comentarios +
      "</td>" +
      "<td>" +
      obj.vehiculo +
      "</td>" +
      "<td class='text-right'>" +
      obj.empleado +
      "</td>" +
      "</tr>";
  });

  e += "</tbody></table></div>";

  return e;
} /*datasets: [
 {
 label: '9:30',
 backgroundColor: "#56170c",
 stack: 'Stack 0',
 data: [
 66,
 93,
 31,
 76,
 39,
 75
 ]
 },
 {
 label: '11:30',
 backgroundColor: "#922714",
 stack: 'Stack 0',
 data: [
 76,
 203,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: 'Día Sig.',
 backgroundColor: "#bf331a",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: 'Día Sig. Terr.',
 backgroundColor: "#b02f18",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: '2 Días',
 backgroundColor: "#e86d57",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: 'Apoyo a OT',
 backgroundColor: "#f0a193",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: 'LTL',
 backgroundColor: "#f6c8bf",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: 'Internacional',
 backgroundColor: "#fcefec",
 stack: 'Stack 0',
 data: [
 76,
 103,
 41,
 86,
 49,
 85
 ]
 },
 {
 label: '9:30',
 backgroundColor: "#56170c",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: '11:30',
 backgroundColor: "#922714",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: 'Día Sig.',
 backgroundColor: "#bf331a",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: 'Día Sig. Terr.',
 backgroundColor: "#b02f18",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: '2 Días',
 backgroundColor: "#e86d57",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: 'Apoyo a OT',
 backgroundColor: "#f0a193",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: 'LTL',
 backgroundColor: "#f6c8bf",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 },
 {
 label: 'Internacional',
 backgroundColor: "#fcefec",
 stack: 'Stack 1',
 data: [
 56,
 56,
 83,
 21,
 66,
 29
 ]
 }
 ],*/

/************************************************Menu  RUTA TR2***********************************/

/**
 * funcion para generar la grafica de detalle de plaza por ruta tr2
 */ function showDetallePlazaTr2(valueLabel, datasource) {
  $.alert({
    title: "Detalle - " + valueLabel + " por Ruta TR2",
    titleClass: "custom-title-modal",
    content: generateContentGraphPlazaTr2(),
    columnClass:
      "col-lg-12 col-md-12  col-sm-12 col-xs-rastreo-guias custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.9,
    onContentReady: function () {
      console.log("Esta grafica TR2: modal-custom.js - showDetallePlazaTr2");

      // logic to get new data
      var getData = function () {
        $.ajax({
          type: "POST",
          url: datasource.tr2,
          dataType: "json",
          data: filtros,
          success: function (data) {
            // console.log(data);

            var barChartData_tr2 = {
              labels: data.labels, //['97000', '97060', '97099', '97117', '97134', '97147'],
              datasets: data.datasets,
              tamano: data.tamano,
            };

            //Start: Code to reorder the graph
            barChartData_tr2 = ordering(barChartData_tr2);
            //Delete registers undefined
            var indices = [];
            var idx = barChartData_tr2.labels.indexOf(undefined);
            while (idx != -1) {
              indices.push(idx);
              idx = barChartData_tr2.labels.indexOf(undefined, idx + 1);
            }
            for (var i = 0; i < indices.length; i++) {
              if (i > 0) indices[i] = indices[i] - i;
              barChartData_tr2.labels.splice(indices[i], 1);
              for (var d = 0; d < barChartData_tr2.datasets.length; d++) {
                barChartData_tr2.datasets[d].data.splice(indices[i], 1);
              }
            }
            //End: Code to reorder the graph

            var ancho = data.tamano * 3;
            var ctx_tr2 = document
              .getElementById("myChart_tr2")
              .getContext("2d");

            var myBar_tr2 = new Chart(ctx_tr2, {
              type: "bar",
              data: barChartData_tr2,
              options: {
                maintainAspectRatio: false,
                legendCallback: function (chart) {
                  var text = [];
                  text.push('<ul class="0-legend">');
                  var ds = chart.data.datasets[0];
                  var sum = ds.data.reduce(function add(a, b) {
                    return a + b;
                  }, 0);
                  for (var i = 0; i < ds.data.length; i++) {
                    text.push("<li>");
                    var perc = Math.round((100 * ds.data[i]) / sum, 0);
                    //console.log("DATASET", chart.data.datasets[i]);
                    text.push(
                      '<span style="background-color:' +
                        chart.data.datasets[i]?.backgroundColor +
                        '">' +
                        "</span>" +
                        chart.data.datasets[i]?.label
                    );
                    text.push("</li>");
                  }
                  text.push("</ul>");
                  return text.join("");
                },
                onClick: function (clickEvt, activeElems) {
                  //console.log("CLICK", clickEvt, activeElems);
                  //alert("asdasddsasda");
                  //if click was on a bar, we don't care (we want clicks on labels)
                  if (activeElems && activeElems.length) return;
                  var mousePoint = Chart.helpers.getRelativePosition(
                    clickEvt,
                    myBar_tr2
                  );
                  var indexPosition = myBar_tr2.chart.scales[
                    "xAxis1"
                  ].getValueForPixel(mousePoint.x);
                  var valueDetail =
                    myBar_tr2.chart.scales["xAxis1"].ticks[indexPosition];
                  //var valueLabel = myFCH.chart.scales["xAxis1"]._labelItems[indexPosition].label;
                  // console.log(
                  //   indexPosition,
                  //   activeElems,
                  //   myBar_tr2.chart.scales["xAxis1"]
                  // );
                  showProyeccionDetalleGuiasTr2(
                    valueLabel,
                    " Ruta " + valueDetail,
                    datasource,
                    valueDetail
                  );
                },
                layout: {
                  padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 10,
                  },
                },
                title: {
                  display: false,
                  text: "Wisdom Application My To Do",
                },
                legend: {
                  onClick: null,
                  display: false,
                  position: "bottom",
                  labels: {
                    fontColor: "white",
                    fontSize: 14,
                    generateLabels: function (chart) {
                      return Chart.defaults.global.legend.labels.generateLabels
                        .apply(this, [chart])
                        .filter(function (item, i) {
                          return i <= 7;
                        });
                    },
                  },
                },
                tooltips: {
                  enabled: true,
                  custom: function (tooltipModel) {},
                },
                responsive: true,
                scales: {
                  xAxes: [
                    {
                      id: "xAxis1",
                      type: "category",

                      stacked: true,
                      ticks: {
                        autoSkip: false,
                        maxRotation: 0,
                        minRotation: 0,
                        fontColor: "white",
                        fontSize: 15,
                        padding: 15,

                        //  beginAtZero: true,
                        /*callback: function(value, index, values) {
                                             return 'Entrega  /  Recoleccion' ;
                                             }*/
                      },
                      gridLines: {
                        color: "white",
                        drawTicks: true,
                        tickMarkLength: 90,
                        zeroLineColor: "rgba(255, 255, 255, 1)",
                      },
                    },
                  ],
                  yAxes: [
                    {
                      stacked: true,
                      scaleLabel: {
                        display: true,
                        //labelString: 'Task Count'
                      },
                      gridLines: {
                        color: "white",
                        // borderDash: [2, 5],
                      },
                      ticks: {
                        fontColor: "white",
                        fontSize: 15,
                        padding: 5,
                        beginAtZero: true,
                        userCallback: function (label, index, labels) {
                          // when the floored value is the same as the value we have a whole number
                          if (Math.floor(label) === label) {
                            return label
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            //return label;
                          }
                        },

                        //  beginAtZero: true,
                        /*callback: function(value, index, values) {
                                             return 'Entrega  /  Recoleccion' ;
                                             }*/
                      },
                    },
                  ],
                },
                plugins: {
                  datalabels: {
                    // display:false,
                    anchor: "start",
                    align: "start",
                    //backgroundColor:"white",
                    //borderRadius: 5,
                    //rotation: -90,
                    color: "white",
                    font: {
                      // weight:'bold'
                    },
                    formatter: function (value, ctx_local) {
                      //console.log('ctx :', ctx_local);
                      //console.log('ctx.chart.data :', ctx_local.chart.data);
                      var chartInstance = ctx_local.chart;
                      var textSShow = null;

                      var stack0 = chartInstance.data.datasets.filter(function (
                        itm
                      ) {
                        return itm.stack === "Stack 0";
                      });

                      var stack1 = chartInstance.data.datasets.filter(function (
                        itm
                      ) {
                        return itm.stack === "Stack 1";
                      });

                      // console.log("stak0", stack0);
                      // console.log("stak1", stack1);
                      // console.log("value", value + "");

                      var datasets = null;
                      if (stack0.indexOf(ctx_local.dataset) === 0) {
                        datasets = ctx_local.chart.data.datasets.filter(
                          function (ds) {
                            var indice = 0;
                            $.each(ds._meta, function (v, a) {
                              indice = v;
                              return v;
                            });
                            return !ds._meta[indice].hidden;
                          }
                        );
                        var sum = 0;
                        stack0.map(function (dataset) {
                          sum += dataset.data[ctx_local.dataIndex];
                          // console.log("SUM", sum);
                        });
                        textSShow = "Entrega \n  " + sum;
                      } else if (stack1.indexOf(ctx_local.dataset) === 0) {
                        datasets = ctx_local.chart.data.datasets.filter(
                          function (ds) {
                            var indice = 0;
                            $.each(ds._meta, function (v, a) {
                              indice = v;
                              return v;
                            });
                            return !ds._meta[indice].hidden;
                          }
                        );
                        var sum2 = 0;
                        stack1.map(function (dataset) {
                          sum2 += dataset.data[ctx_local.dataIndex];
                          // console.log("SUM", sum2);
                        });
                        textSShow = "Recolección \n     " + sum2;
                      }
                      return textSShow;
                    },
                  },
                  /*zoom: {
                                     // Container for pan options
                                     pan: {
                                     // Boolean to enable panning
                                     enabled: true,



                                     // Panning directions. Remove the appropriate direction to disable
                                     // Eg. 'y' would only allow panning in the y direction
                                     mode: 'x'
                                     },



                                     // Container for zoom options
                                     zoom: {
                                     // Boolean to enable zooming
                                     enabled: true,



                                     // Zooming directions. Remove the appropriate direction to disable
                                     // Eg. 'y' would only allow zooming in the y direction
                                     mode: 'x',
                                     }
                                     }*/
                },
              },
            });

            var newwidth =
              $(".chartAreaWrapperTR2").width() + parseInt(ancho) * 20;
            $(".chartAreaWrapperTR2").width(newwidth);

            // document.getElementById('legends_tr2').innerHTML = myBar_tr2.generateLegend();
            $("#spinner").hide();
          },
        });
      };

      getData();
    },
  });
}


/**
 * funcion para generar contenido en la funcion
 * showDetallePlazaTr2(valueLabel)
 *
 * @returns {string}
 */
function generateContentGraphPlazaTr2() {
  var e = "";

  e +=
    '<div class="col-12 col-sm-12 col-md-12 col-lg-12 justify-content-center pl-0 pr-0">' +
    '<div style="width: 100%; overflow-x: auto;">' +
    '<div id="canvas-holder " class="chartAreaWrapperTR2" style="background-color:rgb(0, 0, 0);position: relative; height:70vh; width:100vw;">' + //
    /*'<h2 id="spinner" class="text-white text-center">Cargando gráfica...</h2>'+*/
    '<canvas id="myChart_tr2"  ></canvas>' +
    "</div>" +
    "</div>" +
    '<div class="text-center" id="legends_tr2">' +
    generateLegends() +
    "</div>" +
    "</div>";

  return e;
}

/*************************************legends default ************************/
function generateLegends() {
  var leg =
    '<div class="text-center" id="legends" style="background-color: black">' +
    '<ul class="0-legend">' +
    '<li><span style="background-color:#8d83cf"></span>9:30</li>' +
    '<li><span style="background-color:#e8a97d"></span>11:30</li>' +
    '<li><span style="background-color:#aeaca4"></span>Dia Siguiente</li>' +
    '<li><span style="background-color:#ddda69"></span>Dia Siguiente Terrestre</li>' +
    '<li><span style="background-color:#9dcfa5"></span>2 Dias</li>' +
    '<li><span style="background-color: #91aec4;"></span>Terrestre</li>' +
    '<li><span style="background-color: #ffdbad;"></span>LTL</li>' +
    "</ul>" +
    "</div>";
  return leg;
}
/*************************************fin legends default ************************/

/************************************************Submenu  RUTA TR2 -> DETALLE GUIAS***********************************/
/**
 * funcion para mostrar la tabla de detalle para las Rutas TR2
 */
/*function showProyeccionDetalleGuias(valueLabel,valueDetail) {

 var proyeccion_guias =[
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 {},
 ];
 $.alert({
 title: 'Detalle de Guías - '+valueLabel+ ' Ruta '+valueDetail,
 titleClass: 'custom-title-modal',
 content: generateContentProyeccionDetalleGuias(proyeccion_guias),
 columnClass: 'col-md-10 col-md-offset-2  col-sm-10 col-sm-offset-2 col-8  custom-modal-closeicon',
 closeIcon: true,
 buttons: false,
 bgOpacity:.7,
 //useBootstrap: false,
 //boxWidth: '500px',
 bootstrapClasses: {
 //    container: 'container',
 //    containerFluid: 'container-fluid',
 // row: 'row justify-content-center',
 }
 });
 }*/

function showProyeccionDetalleGuiasOrigenDestino(
  valueLabel,
  valueDetail,
  datasource,
  plaza
) {
  var proyeccion_guias = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
  //filtros.ruta = ruta;
  filtros.plaza_filtro_seleccionado = plaza;
  var getGuias = function () {
    $.ajax({
      type: "POST",
      url: datasource.tr1_detalle,
      dataType: "json",
      data: filtros,
      success: function (data) {
        proyeccion_guias = data;

        $.alert({
          title: "Detalle de Guías - " + valueLabel + " " + valueDetail,
          titleClass: "custom-title-modal",
          content: generateContentProyeccionDetalleGuias(proyeccion_guias),
          columnClass:
            "col-12  col-sm-12 col-md-12  col-lg-12   custom-modal-closeicon",
          closeIcon: true,
          buttons: false,
          bgOpacity: 0.7,
          //useBootstrap: false,
          //boxWidth: '500px',
          bootstrapClasses: {
            //    container: 'container',
            //    containerFluid: 'container-fluid',
            // row: 'row justify-content-center',
          },
        });
      },
    });
  };
  getGuias();
}
function showProyeccionDetalleGuiasTr2(
  valueLabel,
  valueDetail,
  datasource,
  ruta
) {
  var proyeccion_guias = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
  filtros.ruta = ruta;
  var getGuias = function () {
    $.ajax({
      type: "POST",
      url: datasource.tr2_detalle,
      dataType: "json",
      data: filtros,
      success: function (data) {
        proyeccion_guias = data;

        $.alert({
          title: "Detalle de Guías - " + valueLabel + " " + valueDetail,
          titleClass: "custom-title-modal",
          content: generateContentProyeccionDetalleGuias(proyeccion_guias),
          columnClass:
            "col-12  col-sm-12 col-md-12  col-lg-12   custom-modal-closeicon",
          closeIcon: true,
          buttons: false,
          bgOpacity: 0.7,
          //useBootstrap: false,
          //boxWidth: '500px',
          bootstrapClasses: {
            //    container: 'container',
            //    containerFluid: 'container-fluid',
            // row: 'row justify-content-center',
          },
        });
      },
    });
  };
  getGuias();
}
function showProyeccionDetalleGuias2(
  valueLabel,
  valueDetail,
  datasource,
  codpos
) {
  var proyeccion_guias = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
  filtros.codigopostal = codpos;
  var getGuias = function () {
    $.ajax({
      type: "POST",
      url: datasource.codigopos_detalle,
      dataType: "json",
      data: filtros,
      success: function (data) {
        proyeccion_guias = data;

        $.alert({
          title: "Detalle de Guías - " + valueLabel + " " + valueDetail,
          titleClass: "custom-title-modal",
          content: generateContentProyeccionDetalleGuias(proyeccion_guias),
          columnClass:
            "col-12  col-sm-12 col-md-12  col-lg-12   custom-modal-closeicon",
          closeIcon: true,
          buttons: false,
          bgOpacity: 0.7,
          //useBootstrap: false,
          //boxWidth: '500px',
          bootstrapClasses: {
            //    container: 'container',
            //    containerFluid: 'container-fluid',
            // row: 'row justify-content-center',
          },
        });
      },
    });
  };
  getGuias();
}

/**
 * funcion para generar contenido en la funcion de
 * showProyeccionDetalleGuias(valueLabel,valueDetail)
 * @returns {string}
 */
function generateContentProyeccionDetalleGuias(rows) {
  var e = "";

  // console.log("ROWSSSSS",rows);

  e +=
    '<div class="table-responsive" >' +
    '<div class="row">' +
    '<div class="col-3 pb-2">' +
    '<button type="button" class="btn btn-secondary btn-sm export" onclick="generarExcel()">Exportar XLS</button>' +
    "</div>" +
    "</div>" +
    '<table class="table2excel table table-borderless custom-fontsize-table mb-0 text-center table-striped table-sm table-hover table-light table-dark table-black" style="text-align: center;">' +
    '<thead class="">' +
    "<tr>" +
    /*'<th></th>'+*/
    "<th></th>" +
    /*'<th colspan="7" class="color_border_botom_white">Gu&iacute;a</th>'+*/
    '<th colspan="2" class="color_border_botom_white">Gu&iacute;a</th>' +
    /*'<th >&nbsp;</th>'+*/
    /*'<th colspan="3" class="color_border_botom_white">Cliente</th>'+*/
    '<th class="color_border_botom_white">Cliente</th>' +
    /*'<th >&nbsp;</th>'+*/
    /*'<th colspan="8" class="color_border_botom_white">Ubicaci&oacute;n</th>'+*/
    /*'<th colspan="8" class="color_border_botom_white">Ubicaci&oacute;n</th>'+*/
    '<th colspan="4" class="color_border_botom_white">Ubicaci&oacute;n</th>' +
    "</tr>" +
    "<tr >" +
    '<th class="color_border_botom_white">Consec</th>' +
    /*'<th></th>'+*/
    '<th class="color_border_botom_white">Número</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    /*'<th class="color_border_botom_white">Tipo de empaque</th>'+*/
    /*'<th class="color_border_botom_white"></th>'+*/
    '<th class="color_border_botom_white">Garantía</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    /*'<th class="color_border_botom_white">Contenido</th>'+*/
    /*'<th></th>'+*/
    '<th class="color_border_botom_white">Número</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    /*'<th class="color_border_botom_white">Razón Social</th>'+*/
    /*'<th></th>'+*/
    '<th class="color_border_botom_white">Distrito</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    '<th class="color_border_botom_white">Regional</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    '<th class="color_border_botom_white">Plaza</th>' +
    /*'<th class="color_border_botom_white"></th>'+*/
    '<th class="color_border_botom_white">CP</th>' +
    /*'<th class="color_border_botom_white">Dirección</th>'+*/

    "</tr>" +
    "</thead>" +
    "<tbody>";

  $.each(rows, function (idx, obj) {
    e +=
      "<tr>" +
      "<td>" +
      (idx + 1) +
      "</td>" + //separador
      /*"<td></td>" +*/ "<td>" +
      (obj.vwguias_numeroguia === null ? "" : obj.vwguias_numeroguia) +
      "</td>" /*"<td>"+(obj.vwinfoenvio_nomtipoenvio === null? "":obj.vwinfoenvio_nomtipoenvio)+"</td>" +*/ + //separador //separador
      /*"<td></td>" +*/ /*"<td></td>" +*/ "<td>" +
      (obj.garantia === null ? "" : obj.garantia) +
      "</td>" /*"<td>"+(obj.vwinfoenvio_contenido === null? "":obj.vwinfoenvio_contenido)+"</td>" +*/ + //separador //separador
      /*"<td></td>" +*/ /*"<td></td>" +*/ "<td>" +
      (obj.numero_cliente === null ? "" : obj.numero_cliente) +
      "</td>" /*"<td>"+(obj.vwdestino_razonsocial === null? "":obj.vwdestino_razonsocial)+"</td>" +*/ + //separador //separador
      /*"<td></td>" +*/ /*"<td></td>" +*/ "<td>" +
      (obj.distrito === null ? "" : obj.distrito) +
      "</td>" + //separador
      /*"<td></td>" +*/ "<td>" +
      (obj.regional === null ? "" : obj.regional) +
      "</td>" + //separador
      /*"<td></td>" +*/ "<td>" +
      (obj.plaza === null ? "" : obj.plaza) +
      "</td>" + //separador
      /*"<td></td>" +*/ "<td>" +
      (obj.codigo_postal === null ? "" : obj.codigo_postal) +
      "</td>" +
      "<td>" +
      (obj.direccion === null ? "" : obj.direccion) +
      "</td>" + //separador
      "</tr>";

    /* e+="<tr>" +
            "<td>"+(idx+1)+"</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.vwguias_numeroguia === null? "":obj.vwguias_numeroguia)+"</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.vwinfoenvio_nomtipoenvio === null? "":obj.vwinfoenvio_nomtipoenvio)+"</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.vwinfoenvio_contenido === null? "":obj.vwinfoenvio_contenido)+"</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.numero_cliente === null? "":obj.numero_cliente)+"</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.distrito === null? "":obj.distrito)+"</td>" +
            "<td></td>" +//separador
            "<td>-</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.plaza === null? "":obj.plaza)+"</td>" +
            "<td></td>" +//separador
            "<td>"+(obj.codigo_postal === null? "":obj.codigo_postal)+"</td>" +
            "<td>"+(obj.direccion === null? "":obj.direccion)+"</td>" +//separador

            "</tr>";*/
  });

  e += "</tbody></table></div>";

  return e;
}

/************************************************Menu  CODIGO POSTAL***********************************/
/**
 * funcion para generar la grafica de detalle de origen anterior y destino siguiente
 */
function showDetalleCodigoPostal(valueLabel, datasource) {
  $.alert({
    title: "Detalle - " + valueLabel + " por Código Postal",
    titleClass: "custom-title-modal",
    content: generateContentCodigoPostal(),
    columnClass:
      "col-lg-12 col-md-12  col-sm-12 col-xs-rastreo-guias custom-modal-closeicon",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.9,
    onContentReady: function () {
      console.log(
        "Esta grafica CodigoPostal: modal-custom.js - showDetalleCodigoPostal"
      );
      //console.log(datasource);

      // logic to get new data
      var getData = function () {
        $.ajax({
          type: "POST",
          url: datasource.codigopos,
          dataType: "json",
          data: filtros,
          success: function (data) {
            console.log(data);

            var barChartData_tr2 = {
              labels: data.labels, //['97000', '97060', '97099', '97117', '97134', '97147'],
              datasets: data.datasets,
              tamano: data.tamano,
            };

            //Start: Code to reorder the graph
            if(barChartData_tr2.datasets.length > 0){
              barChartData_tr2 = ordering(barChartData_tr2);
              //Delete registers undefined
              var indices = [];
              var idx = barChartData_tr2.labels.indexOf(undefined);
              while (idx != -1) {
                indices.push(idx);
                idx = barChartData_tr2.labels.indexOf(undefined, idx + 1);
              }
              for (var i = 0; i < indices.length; i++) {
                if (i > 0) indices[i] = indices[i] - i;
                barChartData_tr2.labels.splice(indices[i], 1);
                for (var d = 0; d < barChartData_tr2.datasets.length; d++) {
                  barChartData_tr2.datasets[d].data.splice(indices[i], 1);
                }
              }
            }
            //End: Code to reorder the graph

            var ancho = data.tamano * 3;
            var ctx_myChart_codigo_postal = document
              .getElementById("myChart_codigo_postal")
              .getContext("2d");

            //setTimeout(function () {
            var myChmyChart_codigo_postal = new Chart(
              ctx_myChart_codigo_postal,
              {
                type: "bar",
                data: barChartData_tr2,
                options: {
                  maintainAspectRatio: false,
                  // aspectRatio: 1,
                  /*legendCallback: function(chart) {
                                 var text = [];
                                 text.push('<ul class="0-legend">');
                                 var ds = chart.data.datasets[0];
                                 var sum = ds.data.reduce(function add(a, b) { return a + b; }, 0);
                                 for (var i=0; i<ds.data.length; i++) {
                                 text.push('<li>');
                                 var perc = Math.round(100*ds.data[i]/sum,0);
                                 text.push('<span style="background-color:' + chart.data.datasets[i]?.backgroundColor + '">' + '</span>' + chart.data.datasets[i]?.label);
                                 text.push('</li>');
                                 }
                                 text.push('</ul>');
                                 return text.join("");
                                 },*/
                  onClick: function (clickEvt, activeElems) {
                    //console.log("CLICK", clickEvt, activeElems);
                    //alert("asdasddsasda");
                    //if click was on a bar, we don't care (we want clicks on labels)
                    if (activeElems && activeElems.length) return;
                    var mousePoint = Chart.helpers.getRelativePosition(
                      clickEvt,
                      myChmyChart_codigo_postal
                    );
                    var indexPosition = myChmyChart_codigo_postal.chart.scales[
                      "xAxis1"
                    ].getValueForPixel(mousePoint.x);
                    var valueDetail =
                      myChmyChart_codigo_postal.chart.scales["xAxis1"].ticks[
                        indexPosition
                      ];
                    //var valueLabel = myFCH.chart.scales["xAxis1"]._labelItems[indexPosition].label;
                    // console.log(
                    //   indexPosition,
                    //   activeElems,
                    //   myChmyChart_codigo_postal.chart.scales["xAxis1"]
                    // );
                    showProyeccionDetalleGuias2(
                      valueLabel,
                      " CP " + valueDetail,
                      datasource,
                      valueDetail
                    );
                  },

                  layout: {
                    padding: {
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 10,
                    },
                  },
                  title: {
                    display: false,
                    text: "Wisdom Application My To Do",
                  },
                  legend: {
                    onClick: null,
                    display: false,
                    position: "bottom",
                    labels: {
                      fontColor: "white",
                      fontSize: 14,
                      generateLabels: function (chart) {
                        return Chart.defaults.global.legend.labels.generateLabels
                          .apply(this, [chart])
                          .filter(function (item, i) {
                            return i <= 7;
                          });
                      },
                    },
                  },
                  tooltips: {
                    enabled: true,
                    custom: function (tooltipModel) {},
                  },
                  responsive: true,
                  scales: {
                    xAxes: [
                      {
                        id: "xAxis1",
                        type: "category",

                        stacked: true,
                        ticks: {
                          autoSkip: false,
                          maxRotation: 0,
                          minRotation: 0,
                          fontColor: "white",
                          fontSize: 15,
                          padding: 15,

                          //  beginAtZero: true,
                          /*callback: function(value, index, values) {
                                             return 'Entrega  /  Recoleccion' ;
                                             }*/
                        },
                        gridLines: {
                          color: "white",
                          drawTicks: true,
                          tickMarkLength: 90,
                          zeroLineColor: "rgba(255, 255, 255, 1)",
                        },
                      },
                    ],
                    yAxes: [
                      {
                        stacked: true,
                        scaleLabel: {
                          display: true,
                          //labelString: 'Task Count'
                        },
                        gridLines: {
                          color: "white",
                          // borderDash: [2, 5],
                        },
                        ticks: {
                          fontColor: "white",
                          fontSize: 15,
                          padding: 5,
                          beginAtZero: true,
                          userCallback: function (label, index, labels) {
                            // when the floored value is the same as the value we have a whole number
                            if (Math.floor(label) === label) {
                              return label
                                .toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                              //return label;
                            }
                          },

                          //  beginAtZero: true,
                          /*callback: function(value, index, values) {
                                             return 'Entrega  /  Recoleccion' ;
                                             }*/
                        },
                      },
                    ],
                  },
                  plugins: {
                    datalabels: {
                      // display:false,
                      anchor: "start",
                      align: "start",
                      //backgroundColor:"white",
                      //borderRadius: 5,
                      //rotation: -90,
                      color: "white",
                      font: {
                        // weight:'bold'
                      },
                      formatter: function (value, ctx_local) {
                        //console.log('ctx :', ctx_local);
                        //console.log('ctx.chart.data :', ctx_local.chart.data);
                        var chartInstance = ctx_local.chart;
                        var textSShow = null;

                        var stack0 = chartInstance.data.datasets.filter(
                          function (itm) {
                            return itm.stack === "Stack 0";
                          }
                        );

                        var stack1 = chartInstance.data.datasets.filter(
                          function (itm) {
                            return itm.stack === "Stack 1";
                          }
                        );

                        //console.log("stak0",stack0);
                        //console.log("stak1",stack1);
                        //console.log("value",value +"");

                        var datasets = null;
                        if (stack0.indexOf(ctx_local.dataset) === 0) {
                          datasets = ctx_local.chart.data.datasets.filter(
                            function (ds) {
                              var indice = 0;
                              $.each(ds._meta, function (v, a) {
                                indice = v;
                                return v;
                              });
                              return !ds._meta[indice].hidden;
                            }
                          );
                          var sum = 0;
                          stack0.map(function (dataset) {
                            sum += dataset.data[ctx_local.dataIndex];
                            //console.log("SUM",sum);
                          });
                          textSShow = "Entrega\n  " + sum;
                        } else if (stack1.indexOf(ctx_local.dataset) === 0) {
                          datasets = ctx_local.chart.data.datasets.filter(
                            function (ds) {
                              var indice = 0;
                              $.each(ds._meta, function (v, a) {
                                indice = v;
                                return v;
                              });
                              return !ds._meta[indice].hidden;
                            }
                          );
                          var sum2 = 0;
                          stack1.map(function (dataset) {
                            sum2 += dataset.data[ctx_local.dataIndex];
                            //console.log("SUM",sum2);
                          });
                          textSShow = "Recolección\n     " + sum2;
                        }
                        return textSShow;
                      },
                    },
                    /*zoom: {
                                     // Container for pan options
                                     pan: {
                                     // Boolean to enable panning
                                     enabled: true,



                                     // Panning directions. Remove the appropriate direction to disable
                                     // Eg. 'y' would only allow panning in the y direction
                                     mode: 'x'
                                     },



                                     // Container for zoom options
                                     zoom: {
                                     // Boolean to enable zooming
                                     enabled: true,



                                     // Zooming directions. Remove the appropriate direction to disable
                                     // Eg. 'y' would only allow zooming in the y direction
                                     mode: 'x',
                                     }
                                     }*/
                  },
                },
              }
            );

            var newwidth =
              $(".chartAreaWrapperCP").width() + parseInt(ancho) * 20;
            $(".chartAreaWrapperCP").width(newwidth);
            // document.getElementById('legends_cp').innerHTML = myChmyChart_codigo_postal.generateLegend();
            $("#spinner").hide();
            //});

            //myChmyChart_codigo_postal.data.labels();
            // myChmyChart_codigo_postal.data.labels = data.labels;
            //myChmyChart_codigo_postal.data.datasets = data.datasets;
            //myChmyChart_codigo_postal.update();
            //removeData(myChmyChart_codigo_postal);
            //addData(myChmyChart_codigo_postal,data.labels,data.datasets);

            function addData(chart, label, data) {
              chart.data.labels.push(label);
              chart.data.datasets.forEach(function (dataset) {
                dataset.data.push(data);
              });
              chart.update();
            }
            function removeData(chart) {
              chart.data.labels.pop();
              chart.data.datasets.forEach(function (dataset) {
                dataset.data.pop();
              });
              chart.update();
            }

            // process your data to pull out what you plan to use to update the chart
            // e.g. new label and a new data point

            // add new label and data point to chart's underlying data structures
            //myChart.data.labels.push("Post " + postId++);
            // myChart.data.datasets[0].data.push(getRandomIntInclusive(1, 25));

            // re-render the chart
            // myChart.update();
          },
        });
      };

      getData();
    },
  });
}

/**
 * funcion para generar contenido en la funcion
 * showDetalleCodigoPostal(valueLabel)
 *
 * @returns {string}
 */
function generateContentCodigoPostal() {
  var e = "";

  e +=
    '<div class="col-12 col-sm-12 col-md-12 col-lg-12 justify-content-center pl-0 pr-0">' +
    '<div style="width: 100%; overflow-x: auto;">' +
    '<div id="canvas-holder " class="chartAreaWrapperCP" style="background-color:rgb(0, 0, 0);position: relative; height:70vh; width:100vw;">' + //
    /*'<h2 id="spinner" class="text-white text-center">Cargando gráfica...</h2>'+*/

    '<canvas id="myChart_codigo_postal"  ></canvas>' +
    "</div>" +
    "</div>" +
    '<div class="text-center" id="legends_cp" ></div>' +
    generateLegends() +
    "</div>";

  return e;
}
/************************************************Menu  ORIGEN ANTERIOR Y DESTINO SIGUIENTE***********************************/
/**
 * funcion para generar la grafica de detalle de origen anterior y destino siguiente
 */
function showDetalleOrigenAnteriorDestinoSiguiente(valueLabel, datasource) {
  $.alert({
    title:
      "Detalle - " + valueLabel + " por Origen Anterior y Destino Siguiente",
    titleClass: "custom-title-modal",
    content: generateContentOrigenAnteriorDestinoSiguiente(),
    columnClass:
      "col-lg-12 col-md-12  col-sm-12 col-xs-rastreo-guias custom-modal-closeicon ",
    closeIcon: true,
    buttons: false,
    bgOpacity: 0.9,
    onContentReady: function () {
      //alert(datasource.tr1);
      var getD = function (a) {
        $.ajax({
          type: "POST",
          url: datasource.tr1,
          dataType: "json",
          data: filtros,
          success: function (data) {
            console.log(
              "Esta grafica TR1: modal-custom.js - showDetalleOrigenAnteriorDestinoSiguiente"
            );
            /*var barChartData_tr2 = {
                            labels: data.labels,
                            datasets: data.datasets

                        };*/
            var barChartData_tr2 = data;
            //Start: Code to reorder the graph
            if(barChartData_tr2.datasets.length > 0)
              var barChartData_tr2 = ordering(barChartData_tr2);
            //End: Code to reorder the graph

            var ancho = barChartData_tr2.labels.length;
            if (ancho < 6) ancho = 0;

            var ctx_myChOrigenDestino = document
              .getElementById("myChart_orige_destino")
              .getContext("2d");

            var myChOrigenDestino = new Chart(ctx_myChOrigenDestino, {
              /*onAnimationComplete: function () {
                    var sourceCanvas = this.chart.ctx.canvas;
                    var copyWidth = this.scale.xScalePaddingLeft - 5;
                    // the +5 is so that the bottommost y axis label is not clipped off
                    // we could factor this in using measureText if we wanted to be generic
                    var copyHeight = this.scale.endPoint + 5;
                    var targetCtx = document.getElementById("myChartAxis").getContext("2d");
                    targetCtx.canvas.width = copyWidth;
                    targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
                },*/

              type: "bar",
              data: barChartData_tr2,
              options: {
                maintainAspectRatio: false,
                /*legendCallback: function(chart) {
                     var text = [];
                     text.push('<ul class="0-legend">');
                     var ds = chart.data.datasets[0];
                     var sum = ds.data.reduce(function add(a, b) { return a + b; }, 0);
                     for (var i=0; i<ds.data.length; i++) {
                     text.push('<li>');
                     var perc = Math.round(100*ds.data[i]/sum,0);
                     text.push('<span style="background-color:' + chart.data.datasets[i]?.backgroundColor + '">' + '</span>' + chart.data.datasets[i]?.label);
                     text.push('</li>');
                     }
                     text.push('</ul>');
                     return text.join("");
                     },*/
                onClick: function (clickEvt, activeElems) {
                  console.log("CLICK", clickEvt, activeElems);
                  //alert("asdasddsasda");
                  //if click was on a bar, we don't care (we want clicks on labels)
                  if (activeElems && activeElems.length) return;
                  var mousePoint = Chart.helpers.getRelativePosition(
                    clickEvt,
                    myChOrigenDestino
                  );
                  var indexPosition = myChOrigenDestino.chart.scales[
                    "xAxis1"
                  ].getValueForPixel(mousePoint.x);
                  var valueDetail =
                    myChOrigenDestino.chart.scales["xAxis1"].ticks[
                      indexPosition
                    ];
                  //var valueLabel = myFCH.chart.scales["xAxis1"]._labelItems[indexPosition].label;
                  //console.log(indexPosition,activeElems,myChOrigenDestino.chart.scales["xAxis1"]);
                  showProyeccionDetalleGuiasOrigenDestino(
                    valueLabel,
                    " Origen Anterior y Destino Siguiente. " + valueDetail,
                    datasource,
                    valueDetail
                  );
                },
                layout: {
                  padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 10,
                  },
                },
                legend: {
                  onClick: null,
                  display: false,
                  position: "bottom",
                  labels: {
                    fontColor: "white",
                    fontSize: 14,
                    /*generateLabels: function(chart) {
                                return Chart.defaults.global.legend.labels.generateLabels.apply(this, [chart]).filter(function(item, i){
                                    return i <= 7;
                                });
                            }*/
                  },
                },
                tooltips: {
                  enabled: true,
                  custom: function (tooltipModel) {},
                },
                responsive: true,
                scales: {
                  xAxes: [
                    {
                      id: "xAxis1",
                      type: "category",

                      stacked: true,
                      ticks: {
                        beginAtZero: true,
                        min: 0,
                        fontColor: "white",
                        fontSize: 15,
                        padding: 15,

                        //  beginAtZero: true,
                        /*callback: function(value, index, values) {
                                 return 'Entrega  /  Recoleccion' ;
                                 }*/
                      },
                      gridLines: {
                        color: "white",
                        drawTicks: true,
                        tickMarkLength: 90,
                        zeroLineColor: "rgba(255, 255, 255, 1)",
                      },
                    },
                  ],
                  yAxes: [
                    {
                      stacked: true,
                      scaleLabel: {
                        display: true,
                        //labelString: 'Task Count'
                      },
                      gridLines: {
                        color: "white",
                        // borderDash: [2, 5],
                      },
                      ticks: {
                        fontColor: "white",
                        fontSize: 15,
                        padding: 5,
                        beginAtZero: true,
                        /*userCallback: function(label, index, labels) {
                                    // when the floored value is the same as the value we have a whole number
                                    if (Math.floor(label) === label) {
                                        return  label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        //return label;
                                    }

                                },*/
                        //  beginAtZero: true,
                        /*callback: function(value, index, values) {
                                 return 'Entrega  /  Recoleccion' ;
                                 }*/
                      },
                    },
                  ],
                },
                plugins: {
                  sort: {
                    enable: true,
                    mode: "function",
                    reference: [],
                    sortBy: "label",
                    order: "asc",
                    sortFunction: (a, b) => {
                      if (a.label < b.label) return -1;
                      if (a.label > b.label) return 1;
                      return 0;
                    },
                  },
                  datalabels: {
                    // display:false,
                    anchor: "start",
                    align: "start",
                    //backgroundColor:"white",
                    //borderRadius: 5,
                    //rotation: -90,
                    color: "white",
                    font: {
                      // weight:'bold'
                    },

                    formatter: function (value, ctx_local) {
                      //console.log('ctx :', ctx_local);
                      //console.log('ctx.chart.data :', ctx_local.chart.data);
                      var chartInstance = ctx_local.chart;
                      var textSShow = null;

                      var stack0 = chartInstance.data.datasets.filter(function (
                        itm
                      ) {
                        return itm.stack === "Stack 0";
                      });

                      var stack1 = chartInstance.data.datasets.filter(function (
                        itm
                      ) {
                        return itm.stack === "Stack 1";
                      });

                      // console.log("stak0",stack0);
                      // console.log("stak1",stack1);
                      // console.log("value",value +"");

                      var datasets = null;
                      if (stack0.indexOf(ctx_local.dataset) === 0) {
                        datasets = ctx_local.chart.data.datasets.filter(
                          function (ds) {
                            var indice = 0;
                            $.each(ds._meta, function (v, a) {
                              indice = v;
                              return v;
                            });
                            return !ds._meta[indice].hidden;
                          }
                        );
                        var sum = 0;
                        stack0.map(function (dataset) {
                          if (
                            typeof dataset.data[ctx_local.dataIndex] !==
                            "undefined"
                          ) {
                            sum += dataset.data[ctx_local.dataIndex];
                          }
                        });
                        textSShow = "Origen Ant \n  " + sum;
                      } else if (stack1.indexOf(ctx_local.dataset) === 0) {
                        datasets = ctx_local.chart.data.datasets.filter(
                          function (ds) {
                            var indice = 0;
                            $.each(ds._meta, function (v, a) {
                              indice = v;
                              return v;
                            });
                            return !ds._meta[indice].hidden;
                          }
                        );
                        var sum2 = 0;
                        stack1.map(function (dataset) {
                          if (
                            typeof dataset.data[ctx_local.dataIndex] !==
                            "undefined"
                          ) {
                            sum2 += dataset.data[ctx_local.dataIndex];
                          }
                        });
                        textSShow = "Destino Sig \n     " + sum2;
                      }
                      return textSShow;
                    },
                  },
                  /*zoom: {
                         // Container for pan options
                         pan: {
                         // Boolean to enable panning
                         enabled: true,

                         // Panning directions. Remove the appropriate direction to disable
                         // Eg. 'y' would only allow panning in the y direction
                         mode: 'x'
                         },

                         // Container for zoom options
                         zoom: {
                         // Boolean to enable zooming
                         enabled: true,

                         // Zooming directions. Remove the appropriate direction to disable
                         // Eg. 'y' would only allow zooming in the y direction
                         mode: 'xy',
                         }
                         }*/
                },
              },
            });

            var mult = 70;
            var newwidth =
              $(".chartAreaWrapper2").width() + parseInt(ancho) * mult;
            $(".chartAreaWrapper2").width(newwidth);

            console.log(
              "Cantidad de separaciones ",
              ancho,
              "multiplicado por",
              mult,
              " tamaño ",
              newwidth
            );

            //document.getElementById('legends_od').innerHTML = myChOrigenDestino.generateLegend();
            $("#spinner").hide();
          },
        });
      };
      getD();
    },
  });
}

/**
 * Function to reorder the data set of the graph of
 * high to low
 */
function ordering(arr) {
  let position = 0;
  let arrSumPos = [];
  let arrDataAux = [];

  let labels = [];
  let data = [];
  let dataset = [];

  for (let i = 0; i < arr.datasets[0].data.length; i++) {
    let sum = increase(arr.datasets, position);
    arrSumPos.push({
      sum,
      position,
    });
    position++;
  }

  arrSumPos = arrSumPos.sort((a, b) =>
    a.sum < b.sum ? 1 : b.sum < a.sum ? -1 : 0
  );

  for (let i = 0; i < arr.datasets.length; i++) {
    var lblAux = [];
    data = [];

    for (let itm = 0; itm < arrSumPos.length; itm++) {
      lblAux.push(arr.labels[arrSumPos[itm].position]);
      data.push(arr.datasets[i].data[arrSumPos[itm].position]);

      dataset[i] = {
        backgroundColor: arr.datasets[i].backgroundColor,
        idx: arr.datasets[i].idx,
        data: data,
        label: arr.datasets[i].label,
        stack: arr.datasets[i].stack,
      };
    }
    labels = lblAux;
  }

  arrDataAux = {
    tamano: arr.tamano,
    labels: labels,
    datasets: dataset,
  };

  return arrDataAux;
}
/**
 * Function to add the dataset of the graph of
 * horizontally
 */
function increase(arr, pos) {
  let sum = 0;
  arr.map(function (dataset) {
    if (typeof dataset.data[pos] !== "undefined") {
      sum += dataset.data[pos];
    }
  });
  return sum;
}

/**
 * funcion para generar contenido en la funcion
 * showDetalleOrigenAnteriorDestinoSiguiente(valueLabel)
 *
 * @returns {string}
 * TODO
 * Modificando SCROLL
 */
function generateContentOrigenAnteriorDestinoSiguiente() {
  return (
    "" +
    /*'<div class="chartWrapper">' +
         '<div class="chartAreaWrapper">' +
         '<div class="chartAreaWrapper2">' +
         '<canvas id="myChart_orige_destino"></canvas>' +
         '</div>' +
         '</div>' +
         '<canvas id="myChartAxis" height="300" width="0"></canvas>' +
         '</div>' +*/
    "" +
    "" +
    "" +
    '<div class="col-12 col-sm-12 col-md-12 col-lg-12 justify-content-center pl-0 pr-0">' +
    '<div style="width: 100%; overflow-x: auto;">' +
    '<div id="canvas-holder " class="chartAreaWrapper2" style="background-color:rgb(0,0,0);position: relative; height:70vh; width:100vw;">' + //
    '<canvas id="myChart_orige_destino"  height="140vh" ></canvas>' +
    "</div>" +
    "</div>" +
    '<div class="text-center" id="legends_cp" ></div>' +
    generateLegends() +
    "</div>"
  );
}
/*function generateContentOrigenAnteriorDestinoSiguiente() {
 return '' +
 '<div class="col-12 col-sm-12 col-md-12 col-lg-12 justify-content-center pl-0 pr-0">' +
 '<div style="width: 100%; overflow-x: auto;">'+
 '<div id="canvas-holder " style="background-color:rgb(0, 0, 0);position: relative; height:70vh; width:600vw;">'+//
 '<canvas id="myChart_orige_destino"  height="140vh" ></canvas>'+
 '</div>' +
 '</div>'+
 '<div class="text-center" id="legends_cp" ></div>'+
 generateLegends()+
 '</div>';
 }*/

/************************************************Menu  de Opciones***********************************/

/**
 * funcion para mostrar el detalle de opciones del menu
 * Chart principal
 * @param valueLabel
 */
function showMenuProyeccionEntregasD5(valueLabel, datasource) {
  $.dialog({
    theme: "dark",
    title: valueLabel,
    content: generateContentMenuOptionsD5(valueLabel),
    columnClass: "col-md-4 col-sm-4 col-xs-4 custom-modal-menu",
    onContentReady: function () {
      $(".porOrigenDestino")
        .unbind("click")
        .on("click", function (e) {
          e.preventDefault();
          showDetalleOrigenAnteriorDestinoSiguiente(valueLabel, datasource);
        });

      $(".porCodigoPostal")
        .unbind("click")
        .on("click", function (e) {
          e.preventDefault();
          showDetalleCodigoPostal(valueLabel, datasource);
        });
      $(".porRutatr2")
        .unbind("click")
        .on("click", function (e) {
          e.preventDefault();
          showDetallePlazaTr2(valueLabel, datasource);
        });
    },
  });
}

/**
 * funcion para generar el menu de
 * Proyección de Entregas D+5
 *
 * @returns {string}
 */
function generateContentMenuOptionsD5(valueLabel) {
  var e = "";

  e =
    '<div class="list-group list-group-flush bg-gray " role="tablist">' +
    '<button type="button" class="button-menu-d5 list-group-item list-group-item-action porOrigenDestino">TR1 (Origen Anterior/Destino Siguiente)</button>' +
    '<button type="button" class="button-menu-d5 list-group-item list-group-item-action porRutatr2">TR2</button>' +
    //'<button type="button" class="list-group-item list-group-item-action">Dapibus ac facilisis in</button>'+
    '<button type="button" class="button-menu-d5 list-group-item list-group-item-action porCodigoPostal">Código Postal</button>' +
    //'<button type="button" class="list-group-item list-group-item-action">'+valueLabel+' por Detalle de Guías</button>'+
    "</div>";
  return e;
}
